import { When } from "cucumber"
import util from "../utilities/util"
import loginPage from "../pageobjects/loginPage"
import addEmployerPage from "../pageobjects/addEmployerPage"
import editEmployerPage from "../pageobjects/editEmployerPage"
import adminDashboardPage from "../pageobjects/adminDashboardPage"
import changePasswordPage from "../pageobjects/changePasswordPage"
import profilePage from "../pageobjects/profilePage"
import careCoachPage from "../pageobjects/careCoachPage"
import loadEmployeeFeedPage from "../pageobjects/loadEmployeeFeedPage"
import careCoachViewDetailsPage from "../pageobjects/careCoachViewDetailsPage"
import { LocalStorage } from "node-localstorage"

When(/^I enter username as "([^"]*)"$/, function (uname) {
  let username = util.decryptedText(util.getUserDetails().username)
  if (uname === "actualusername") {
    loginPage.enterUsername(username)
    loginPage.usernameInput.getValue().should.equal(username)
  } else {
    loginPage.enterUsername(uname)
    loginPage.usernameInput.getValue().should.equal(uname)
  }
})

When(/^I enter password as "([^"]*)"$/, function (pwd) {
  let password = util.decryptedText(util.getUserDetails().password)
  if (pwd === "actualpassword") {
    loginPage.enterPassword(password)
  } else {
    loginPage.enterPassword(pwd)
  }
})

When(/^I attempt to login$/, function () {
  loginPage.clickOnSignButton()
})

When(
  /^I click on "([^"]*)" link on the admin dashboard page$/,
  function (link) {
    adminDashboardPage.clickOnMenuLink()
    if (link === "Profile") {
      adminDashboardPage.clickOnProfileLink()
    } else if (link === "Logout") {
      adminDashboardPage.clickOnLogoutLink()
    }
  }
)

When(
  /^I click on "([^"]*)" button on "([^"]*)" list page$/,
  function (string, lisPage) {
    if (lisPage === "employers") {
      adminDashboardPage.clickOnAddNewButton()
    } else if (lisPage === "care coaches") {
      careCoachPage.clickOnAddNewButton()
    }
  }
)

When(/^I enter data on input fields on add "([^"]*)" page$/, function (text) {
  let generatedName = util.generateName(6)
  let phone = util.getAnyPhoneFromList()
  if (text === "employer") {
    addEmployerPage.enterDataOnAddEmployerPage(
      generatedName,
      phone,
      "Company Email"
    )
  } else if (text === "care coach") {
    careCoachPage.enterDataOnAddCareCoachPage(generatedName, phone)
  }
})

When(/^I click on save button on add "([^"]*)" page$/, function (text) {
  if (text === "employer") {
    addEmployerPage.clickOnSaveButton()
  } else if (text === "care coach") {
    careCoachPage.clickOnSaveButton()
  }
})

When(/^I click on save button on edit "([^"]*)" page$/, function (text) {
  if (text === "employer") {
    editEmployerPage.clickOnSaveButton()
  } else if (text === "care coach") {
    careCoachPage.clickOnSaveButton()
  }
})

When(
  /^I click on edit icon of first record in "([^"]*)" list page$/,
  function (listPage) {
    global.localStorage = new LocalStorage("./scratch")
    if (listPage === "employers") {
      localStorage.setItem(
        "employerName",
        adminDashboardPage.getEmployerNameOnTable()
      )
      localStorage.setItem(
        "contactName",
        adminDashboardPage.getContactNameOnTable()
      )
      localStorage.setItem(
        "empStatus",
        adminDashboardPage.getStatusOnEmployerList()
      )
      adminDashboardPage.clickOnEditIconOfFirstRecord()
    } else if (listPage === "care coaches") {
      localStorage.setItem(
        "ccFirstName",
        careCoachPage.getFirstNameOnCareCoachTable()
      )
      localStorage.setItem(
        "ccLastName",
        careCoachPage.getLastNameOnCareCoachTable()
      )
      localStorage.setItem("ccEmail", careCoachPage.getEmailOnCareCoachTable())
      localStorage.setItem(
        "ccPhone",
        careCoachPage.getPhoneNumberOnCareCoachTable()
      )
      localStorage.setItem(
        "ccStatus",
        careCoachPage.getStatusOnCareCoachTable()
      )
      careCoachPage.clickOnEditIconOfFirstRecord()
    }
  }
)

When(/^I update data on input fields on edit "([^"]*)" page$/, function (text) {
  let generatedName = util.generateName(6)
  if (text === "employer") {
    editEmployerPage.updateDataOnEditEmployerPage(generatedName, "Employee ID")
  } else if (text === "care coach") {
    careCoachPage.updateDataOnEditCareCoachPage(generatedName)
  }
})

When(/^I enter "([^"]*)" on change password tab$/, function (text) {
  let password = util.decryptedText(util.getUserDetails().password)
  let temp_password = util.decryptedText(util.getUserDetails().temp_password)
  if (text === "Current Password") {
    changePasswordPage.enterCurrentPassword(password)
  } else if (text === "New Password") {
    changePasswordPage.enterNewPassword(temp_password)
  }
})

When(/^I enter incorrect "([^"]*)" on change password tab$/, function (text) {
  //here, we providing username as current password,to test incorrect current password testing
  let password = util.decryptedText(util.getUserDetails().username)
  if (text === "Current Password") {
    changePasswordPage.enterCurrentPassword(password)
  }
})

When(/^I confirm "([^"]*)" on change password tab$/, function (retypePassword) {
  let temp_password = util.decryptedText(util.getUserDetails().temp_password)
  changePasswordPage.enterReEnterNewPassword(temp_password)
})

When(/^I click on update button on "([^"]*)" page$/, function (text) {
  if (text === "change password") {
    changePasswordPage.clickOnUpdateButton()
  } else if (text === "profile") {
    profilePage.clickOnUpdateButton()
  }
})

When(/^I update name and phone on profile page$/, function () {
  profilePage.updateDataOnProfilePage()
})

When(
  /^I click on status toggle button on edit "([^"]*)" page$/,
  function (pageText) {
    if (pageText === "employer") {
      editEmployerPage.clickOnStatusToggleButton()
    } else if (pageText === "care coach") {
      careCoachPage.clickOnStatusToggleButton()
    }
  }
)

When(
  /^I click on "([^"]*)" button on model on edit "([^"]*)" page$/,
  function (buttonTxt, pageText) {
    if (pageText === "employer") {
      if (buttonTxt === "Continue") {
        editEmployerPage.clickOnContinueButtonOnModel()
      } else {
        editEmployerPage.clickOnCancelButtonOnModel()
      }
    } else if (pageText === "care coach") {
      if (buttonTxt === "Continue") {
        careCoachPage.clickOnContinueButtonOnModel()
      } else {
        careCoachPage.clickOnCancelButtonOnModel()
      }
    }
  }
)

When(/^I click on "([^"]*)" link from sidebar$/, function (leftMenu) {
  if (leftMenu === "Care Coaches") {
    adminDashboardPage.clickOnCareCoachesSideBarMenu()
  }
})

When(
  /^I click on upload icon of first record and select the "([^"]*)" file on employer list page$/,
  function (fileType) {
    loadEmployeeFeedPage.uploadCSVFile(fileType)
  }
)

When(
  /^I click on refresh icon of first record on employer list page$/,
  function () {
    loadEmployeeFeedPage.clickOnReloadIconOfFirstRecord()
  }
)

When(
  /^I click on view icon of first record in care coaches list page$/,
  function () {
    global.localStorage = new LocalStorage("./scratch")
    localStorage.setItem(
      "ccFirstNameOnTable",
      careCoachPage.getFirstNameOnCareCoachTable()
    )
    localStorage.setItem(
      "ccLastNameOnTable",
      careCoachPage.getLastNameOnCareCoachTable()
    )
    localStorage.setItem(
      "ccEmailOnTable",
      careCoachPage.getEmailOnCareCoachTable()
    )
    localStorage.setItem(
      "ccPhoneOnTable",
      careCoachPage.getPhoneNumberOnCareCoachTable()
    )
    localStorage.setItem(
      "ccStatusOnTable",
      careCoachPage.getStatusOnCareCoachList()
    )
    careCoachPage.clickOnViewIconOfFirstRecord()
  }
)
When(
  /^I click on "([^"]*)" button on view care coach details page$/,
  function (text) {
    if (text === "Edit") {
      careCoachViewDetailsPage.clickOnEditButton()
    } else if (text === "Preview Profile") {
      careCoachViewDetailsPage.clickOnProfilePreviewButton()
    }
  }
)

When(/^I click on close button on Preview Profile model$/, function () {
  careCoachViewDetailsPage.clickOnCloseButtonOnModel()
})
