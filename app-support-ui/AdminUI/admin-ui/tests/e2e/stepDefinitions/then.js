/* eslint-disable no-unused-expressions */
import { Then } from "cucumber"
import util from "../utilities/util"
import loginPage from "../pageobjects/loginPage"
import loginErrorPage from "../pageobjects/loginErrorPage"
import forbiddenPage from "../pageobjects/forbiddenPage"
import adminDashboardPage from "../pageobjects/adminDashboardPage"
import addEmployerPage from "../pageobjects/addEmployerPage"
import editEmployerPage from "../pageobjects/editEmployerPage"
import changePasswordPage from "../pageobjects/changePasswordPage"
import profilePage from "../pageobjects/profilePage"
import careCoachPage from "../pageobjects/careCoachPage"
import loadEmployeeFeedPage from "../pageobjects/loadEmployeeFeedPage"
import careCoachViewDetailsPage from "../pageobjects/careCoachViewDetailsPage"

Then(/^I should see cognito login form fields$/, function () {
  loginPage.validateUIFields().should.be.true
})

Then(/^I should see login error message as "([^"]*)"$/, function (message) {
  loginErrorPage.getErrorMessage().should.equal(message)
  browser.pause(3000)
})

Then(/^I should see admin dashboard page$/, function () {
  adminDashboardPage.getEmployersHeadeing().should.be.true
  adminDashboardPage.getEmployerTable().should.be.true
})

Then(/^I should see "([^"]*)" page$/, function (text) {
  if (text === "Forbidden") {
    forbiddenPage.getForbiddenLabel().should.equal(text)
  } else if (text === "Add New Employer") {
    addEmployerPage.getEmployersHeadeing().should.equal(text)
  } else if (text === "Edit Employer") {
    editEmployerPage.getEmployersHeadeing().should.equal(text)
  } else if (text === "Profile") {
    profilePage.getProfileHeadeing().should.equal(text)
  } else if (text === "Add New Care Coach" || text === "Edit Care Coach") {
    careCoachPage.getCareCoachPageHeading().should.equal(text)
  } else if (text === "Care Coaches") {
    profilePage.getCareCoachListHeading().should.equal(text)
  }
})

Then(/^I should see forbidden message as "([^"]*)"$/, function (message) {
  forbiddenPage.getForbiddenMessage().should.equal(message)
})

Then(
  /^I should be successfully logged out and land on the login page$/,
  function () {
    loginPage.validateUIFields().should.be.true
  }
)

Then(
  /^I should see message as "([^"]*)" on "([^"]*)" list page$/,
  function (message, listPage) {
    if (listPage === "employers") {
      adminDashboardPage.changesSavedMessage().should.equal(message)
    } else if (listPage === "care coaches") {
      careCoachPage.changesSavedMessage().should.equal(message)
    }
  }
)

Then(
  /^I should see "([^"]*)" data on "([^"]*)" list page$/,
  function (text, listPage) {
    if (listPage === "employers") {
      adminDashboardPage
        .getEmployerNameOnTable()
        .should.equal(localStorage.getItem("employerName"))
      adminDashboardPage
        .getContactNameOnTable()
        .should.equal(localStorage.getItem("contactName"))
      adminDashboardPage
        .getStatusOnEmployerList()
        .should.equal(localStorage.getItem("employerStatus"))
    } else if (listPage === "care coaches") {
      careCoachPage
        .getFirstNameOnCareCoachTable()
        .should.equal(localStorage.getItem("firstName"))
      careCoachPage
        .getLastNameOnCareCoachTable()
        .should.equal(localStorage.getItem("lastName"))
      careCoachPage
        .getEmailOnCareCoachTable()
        .should.equal(localStorage.getItem("email"))
      careCoachPage
        .getPhoneNumberOnCareCoachTable()
        .should.equal(localStorage.getItem("phone"))
      careCoachPage
        .getStatusOnCareCoachTable()
        .should.equal(localStorage.getItem("status"))
    }
    localStorage._deleteLocation()
  }
)

Then(
  /^I should see "([^"]*)" tab on profile page and click on it$/,
  function (message) {
    changePasswordPage.getChangePasswordTab().should.equal(message)
    changePasswordPage.clickOnChangePasswordTab()
  }
)

Then(
  /^I should see "([^"]*)" message at the bottom on "([^"]*)" page$/,
  function (message, pageName) {
    if (pageName === "profile") {
      profilePage.getSuccessMessgae().should.equal(message)
    } else if (pageName === "change password") {
      changePasswordPage.getSuccessMessgae().should.equal(message)
    }
    browser.pause(3000)
  }
)

Then(/^swap passwords in config file$/, function () {
  let username = util.getUserDetails().username
  let password = util.getUserDetails().password
  let temp_password = util.getUserDetails().temp_password
  changePasswordPage.swapPasswords(password, temp_password, username)
})

Then(
  /^I should see "([^"]*)" fields on the Profile page$/,
  function (profileUI) {
    profilePage.validateUIFieldsOnProfilePage().should.be.true
  }
)

Then(
  /^"([^"]*)" field should be prepopulated and set as "([^"]*)" on the profile page$/,
  function (email, header) {
    profilePage.validateEmailAddressDisabled().should.be.true
  }
)

Then(
  /^I should see error message "([^"]*)" for incorrect current password$/,
  function (msg) {
    changePasswordPage.getErrorMessage().should.equal(msg)
    browser.pause(3000)
  }
)

Then(
  /^I should see "([^"]*)" status toggle button on edit "([^"]*)" page$/,
  function (statusTxt, pageText) {
    if (pageText === "employer") {
      editEmployerPage.getStatusTextOnEditEmployerPage().should.equal(statusTxt)
    } else if (pageText === "care coach") {
      careCoachPage.getStatusTextOnEditCareCoachPage().should.equal(statusTxt)
    }
  }
)

Then(
  /^I should see "([^"]*)" model with "([^"]*)" and "([^"]*)" button on edit "([^"]*)" page$/,
  function (modelHeader, cancelTxt, continueTxt, pageText) {
    if (pageText === "employer") {
      editEmployerPage
        .getDeActivateAccountHeadingOnModel()
        .should.equal(modelHeader)
      editEmployerPage.getCancelButtonOnModel().should.equal(cancelTxt)
      editEmployerPage.getContinueButtonOnModel().should.equal(continueTxt)
    } else if (pageText === "care coach") {
      careCoachPage
        .getDeActivateCareCoachHeadingOnModel()
        .should.equal(modelHeader)
      careCoachPage.getCancelButtonOnModel().should.equal(cancelTxt)
      careCoachPage.getContinueButtonOnModel().should.equal(continueTxt)
    }
  }
)

Then(
  /^I should see "([^"]*)" status on "([^"]*)" list page$/,
  function (statusOnList, listPage) {
    if (listPage === "employers") {
      adminDashboardPage.getStatusOnEmployerList().should.equal(statusOnList)
    } else if (listPage === "care coaches") {
      careCoachPage.getStatusOnCareCoachList().should.equal(statusOnList)
    }
    browser.pause(2000)
  }
)

Then(/^I should see "([^"]*)" list page$/, function (text) {
  if (text === "Care Coaches") {
    careCoachPage.getCareCoachListHeading().should.equal(text)
    careCoachPage.getCareCoachTable().should.be.true
  }
})

Then(
  /^I should see "([^"]*)" status in upload status column$/,
  function (columnTxt) {
    loadEmployeeFeedPage.getUploadStatusOnTable().should.equal(columnTxt)
    browser.pause(1000)
  }
)

Then(
  /^I should not see refresh icon of first record on employer list page$/,
  function () {
    loadEmployeeFeedPage.validateReloadIconShouldNotVisible().should.be.true
  }
)

Then(
  /^I should see download failed icon of first record on employer list page$/,
  function () {
    loadEmployeeFeedPage.validateDownloadFailedIconShouldVisible().should.be
      .true
    browser.pause(2000)
  }
)

Then(/^I should see view care coach details page$/, function () {
  careCoachViewDetailsPage
    .getCareCoachViewPageSubHeading()
    .should.equal("View Care Coach Details")
})

Then(
  /^I should see care coach details on view care coach details page$/,
  function () {
    careCoachViewDetailsPage.verifyCareCoachViewDetailsWithCareCoachesListPage()
      .should.be.true
  }
)

Then(
  /^I should see Edit and Preview Profile button on view care coach details page$/,
  function () {
    careCoachViewDetailsPage.verifyEditAndProfilePreviewButton().should.be.true
  }
)

Then(
  /^I should verify the view care coach data with prepopulated input fields on edit care coach page$/,
  function () {
    careCoachPage.verifyCareCoachViewDetailsWithEditCareCoachPage().should.be
      .true
  }
)

Then(/^I should see Care Coach Profile model and other details$/, function () {
  careCoachViewDetailsPage.verifyCareCoachProfileDetailsOnModel().should.be.true
})

Then(
  /^I should see assigned caregivers list and reassigned care coach on view care coach details page$/,
  function () {
    careCoachViewDetailsPage.verifyAssignedCaregiversTableAndReAssignedCareCoach()
      .should.be.true
  }
)
