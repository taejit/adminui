import { Given } from "cucumber"
import loginPage from "../pageobjects/loginPage"

Given("I am on the cognito login page", function () {
  loginPage.open() // navigating to login page
})
