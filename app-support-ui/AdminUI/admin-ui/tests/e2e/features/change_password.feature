
Feature: Lantern Admin Reset Password

    As an Admin
    I want an option to reset password
    So that I can change password anytime at wish

    Background: User is logged in and is on Admin Dashboard Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login

    #Scenario 1.1
    Scenario: Scenario 1.1: Admin is able to reset password successfully from Menu section

        Then I should see admin dashboard page
        When I click on "Profile" link on the admin dashboard page
        Then I should see "Change Password" tab on profile page and click on it
        When I enter "Current Password" on change password tab
        And I enter "New Password" on change password tab
        And I confirm "Retype New Password" on change password tab
        And I click on update button on "change password" page
        Then I should see "Password changed successfully." message at the bottom on "change password" page

    #Scenario 1.2
    Scenario: Scenario 1.2: Admin is able to swap current password to check incorrect username & password
        Then I should see login error message as "Incorrect username or password."
        And swap passwords in config file

    #Scenario 1.3
    Scenario: Scenario 1.3: Admin is able to again reset password successfully from Menu section

        Then I should see admin dashboard page
        When I click on "Profile" link on the admin dashboard page
        Then I should see "Change Password" tab on profile page and click on it
        When I enter "Current Password" on change password tab
        And I enter "New Password" on change password tab
        And I confirm "Retype New Password" on change password tab
        And I click on update button on "change password" page
        Then I should see "Password changed successfully." message at the bottom on "change password" page
        And swap passwords in config file

    #Scenario 2.0
    Scenario: Admin failed to reset password due to wrong current password
        Then I should see admin dashboard page
        When I click on "Profile" link on the admin dashboard page
        Then I should see "Change Password" tab on profile page and click on it
        When I enter incorrect "Current Password" on change password tab
        And I enter "New Password" on change password tab
        And I confirm "Retype New Password" on change password tab
        And I click on update button on "change password" page
        Then I should see error message "Unable to change your password. Please try again." for incorrect current password
