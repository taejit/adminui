Feature: Lantern Admin Portal - Employer On Boarding
    As an admin
    I want to View Employers List, Add, Update, Activate and De-Activate Employer for Lantern Application

    Background: User is logged in and is on Admin Dashboard Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see admin dashboard page

    Scenario: Add new employer with random data
        When I click on "Add New" button on "employers" list page
        Then I should see "Add New Employer" page
        When I enter data on input fields on add "employer" page
        And I click on save button on add "employer" page
        Then I should see message as "Changes Saved" on "employers" list page
        And I should see "saved" data on "employers" list page
        And I should see "Active" status on "employers" list page

    Scenario: Update existing employer with random data
        When I click on edit icon of first record in "employers" list page
        Then I should see "Edit Employer" page
        When I update data on input fields on edit "employer" page
        And I click on save button on edit "employer" page
        Then I should see message as "Changes Saved" on "employers" list page
        And I should see "updated" data on "employers" list page
        And I should see "Active" status on "employers" list page
    @smoketest
    Scenario: De-Activate existing employer
        When I click on edit icon of first record in "employers" list page
        Then I should see "Edit Employer" page
        And I should see "Active" status toggle button on edit "employer" page
        When I click on status toggle button on edit "employer" page
        Then I should see "Deactivate Account" model with "Cancel" and "Continue" button on edit "employer" page
        When I click on "Cancel" button on model on edit "employer" page
        Then I should see "Active" status toggle button on edit "employer" page
        When I click on status toggle button on edit "employer" page
        Then I should see "Deactivate Account" model with "Cancel" and "Continue" button on edit "employer" page
        When I click on "Continue" button on model on edit "employer" page
        Then I should see "Inactive" status toggle button on edit "employer" page
        When I click on save button on edit "employer" page
        Then I should see message as "Changes Saved" on "employers" list page
        And I should see "updated" data on "employers" list page
        And I should see "Inactive" status on "employers" list page
    @smoketest
    Scenario: Activate existing employer
        When I click on edit icon of first record in "employers" list page
        Then I should see "Edit Employer" page
        And I should see "Inactive" status toggle button on edit "employer" page
        When I click on status toggle button on edit "employer" page
        Then I should see "Active" status toggle button on edit "employer" page
        When I click on save button on edit "employer" page
        Then I should see message as "Changes Saved" on "employers" list page
        And I should see "updated" data on "employers" list page
        And I should see "Active" status on "employers" list page
