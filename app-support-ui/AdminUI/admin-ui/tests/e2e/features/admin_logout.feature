Feature: Lantern Admin Portal Logout
    As a Logged in Admin User of the Lantern Application
    I want to Logout successfully

    Background: User is logged in and is on Admin Dashboard Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see admin dashboard page
    @smoketest
    Scenario: Successful Logout
        When I click on "Logout" link on the admin dashboard page
        Then I should be successfully logged out and land on the login page