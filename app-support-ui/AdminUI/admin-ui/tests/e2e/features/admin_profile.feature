Feature: View/Edit Admin Profile
    As an Admin User of the application
    I want to view and update my profile

    Background: Admin is logged in and is on Dashboard Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see admin dashboard page
    @smoketest
    Scenario: Successful View and Edit Profile
        When I click on "Profile" link on the admin dashboard page
        Then I should see "Profile" page
        And I should see "name,email and phone" fields on the Profile page
        And "Email Address" field should be prepopulated and set as "readonly" on the profile page
        When I update name and phone on profile page
        And I click on update button on "profile" page
        Then I should see "Profile updated successfully." message at the bottom on "profile" page


