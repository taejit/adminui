Feature: Lantern Admin Portal - Care Coach Management
    As an admin
    I want to View Care Coaches List, Add, Update,Activate and De-Activate,Detail View of Care Coach for Lantern Application

    Background: User is logged in and is on Admin Dashboard Page
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see admin dashboard page
        When I click on "Care Coaches" link from sidebar
        Then I should see "Care Coaches" list page

    Scenario: Add new care coach with random data
        When I click on "Add New" button on "care coaches" list page
        Then I should see "Add New Care Coach" page
        When I enter data on input fields on add "care coach" page
        And I click on save button on add "care coach" page
        Then I should see message as "Changes Saved" on "care coaches" list page
        And I should see "saved" data on "care coaches" list page
        And I should see "Active" status on "care coaches" list page

    Scenario: Update existing care coach with random data
        When I click on edit icon of first record in "care coaches" list page
        Then I should see "Edit Care Coach" page
        When I update data on input fields on edit "care coach" page
        And I click on save button on edit "care coach" page
        Then I should see message as "Changes Saved" on "care coaches" list page
        And I should see "updated" data on "care coaches" list page
        And I should see "Active" status on "care coaches" list page

    Scenario: De-Activate existing care coach
        When I click on edit icon of first record in "care coaches" list page
        Then I should see "Edit Care Coach" page
        And I should see "Active" status toggle button on edit "care coach" page
        When I click on status toggle button on edit "care coach" page
        Then I should see "Deactivate Care Coach" model with "Cancel" and "Continue" button on edit "care coach" page
        When I click on "Cancel" button on model on edit "care coach" page
        Then I should see "Active" status toggle button on edit "care coach" page
        When I click on status toggle button on edit "care coach" page
        Then I should see "Deactivate Care Coach" model with "Cancel" and "Continue" button on edit "care coach" page
        When I click on "Continue" button on model on edit "care coach" page
        Then I should see "Inactive" status toggle button on edit "care coach" page
        When I click on save button on edit "care coach" page
        Then I should see message as "Changes Saved" on "care coaches" list page
        And I should see "updated" data on "care coaches" list page
        And I should see "Inactive" status on "care coaches" list page

    Scenario: Activate existing care coach
        When I click on edit icon of first record in "care coaches" list page
        Then I should see "Edit Care Coach" page
        And I should see "Inactive" status toggle button on edit "care coach" page
        When I click on status toggle button on edit "care coach" page
        Then I should see "Active" status toggle button on edit "care coach" page
        When I click on save button on edit "care coach" page
        Then I should see message as "Changes Saved" on "care coaches" list page
        And I should see "updated" data on "care coaches" list page
        And I should see "Active" status on "care coaches" list page
    @smoketest
    Scenario: Show Detail View of Care Coach page
        When I click on view icon of first record in care coaches list page
        Then I should see view care coach details page
        And I should see care coach details on view care coach details page
        And I should see Edit and Preview Profile button on view care coach details page
    @smoketest
    Scenario: Show Edit Care Coach Page from Detail View of Care Coach page
        When I click on view icon of first record in care coaches list page
        Then I should see care coach details on view care coach details page
        When I click on "Edit" button on view care coach details page
        Then I should see "Edit Care Coach" page
        And I should verify the view care coach data with prepopulated input fields on edit care coach page
    @smoketest
    Scenario: Show Care Coach Profile Model from Detail View of Care Coach page
        When I click on view icon of first record in care coaches list page
        Then I should see care coach details on view care coach details page
        When I click on "Preview Profile" button on view care coach details page
        Then I should see Care Coach Profile model and other details
    @smoketest
    Scenario: Show Care Coach Profile Model from Detail View of Care Coach page and close the profile model
        When I click on view icon of first record in care coaches list page
        Then I should see care coach details on view care coach details page
        When I click on "Preview Profile" button on view care coach details page
        Then I should see Care Coach Profile model and other details
        When I click on close button on Preview Profile model
        Then I should see view care coach details page
    @smoketest
    Scenario: Show Reassign Caregiver Model from Detail View of Care Coach page
        When I click on view icon of first record in care coaches list page
        Then I should see care coach details on view care coach details page
        And I should see assigned caregivers list and reassigned care coach on view care coach details page


