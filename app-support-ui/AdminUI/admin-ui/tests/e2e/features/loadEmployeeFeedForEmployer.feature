Feature: Lantern Admin Portal - Load Employee Feed For a Given Employer
    As an admin
    I want to load employee feed for a given employer for Lantern Application

    Background: User is logged in and is on Admin Dashboard Page so that he can load csv data file for an employer
        Given I am on the cognito login page
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see admin dashboard page

    Scenario: Upload valid csv data file and check the upload status as success
        When I click on upload icon of first record and select the "valid" file on employer list page
        Then I should see "In Progress" status in upload status column
        When I click on refresh icon of first record on employer list page
        Then I should not see refresh icon of first record on employer list page
        And I should see "Success" status in upload status column

    Scenario: Upload invalid csv data file and check the upload status as failed
        When I click on upload icon of first record and select the "invalid" file on employer list page
        Then I should see "In Progress" status in upload status column
        When I click on refresh icon of first record on employer list page
        Then I should not see refresh icon of first record on employer list page
        And I should see download failed icon of first record on employer list page
        And I should see "Failed" status in upload status column
