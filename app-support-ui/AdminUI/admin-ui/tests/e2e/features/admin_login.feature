Feature: Lantern Admin Portal Login
    As an admin
    I want to visit the login page of cognito for Lantern Application
    So that I can Login into my account

    Background:

        Given I am on the cognito login page

    Scenario: Visit the Login page
        Then I should see cognito login form fields
    @smoketest
    Scenario: Attempt to login with wrong credentials
        When I enter username as "wrong@email.com"
        And I enter password as "wrongPassword"
        And I attempt to login
        Then I should see login error message as "Incorrect username or password."
    @smoketest
    Scenario: User is not part of admin group and try to attempt to login with his credentials
        When I enter username as "harshal@mailinator.com"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see "Forbidden" page
        And  I should see forbidden message as "You do not have permissions to access this portal."
    @smoketest
    Scenario: Attempt to login with correct credentials
        When I enter username as "actualusername"
        And I enter password as "actualpassword"
        And I attempt to login
        Then I should see admin dashboard page

