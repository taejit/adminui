/* eslint-disable no-unused-expressions */
import Page from "./page"
import { LocalStorage } from "node-localstorage"
class careCoachViewDetailsPage extends Page {
  /**
   * define elements
   */
  //Care Coach View Section,need to check
  get subHeadingOnViewCareCoachPage() {
    return $(
      "//div[contains(@class,'subheader')]//li[contains(@class,'active')]"
    )
  }

  get calendlyUsernameLabel() {
    return $("//h6[@id='cccalendlyuname']")
  }

  get editButton() {
    return $("//button[text()='Edit']")
  }

  get previewProfileButton() {
    return $("//button[text()='Preview Profile']")
  }

  get ccNameOnViewCareCoachDetails() {
    return $("//div[@id='ccCard']/h5")
  }

  get emailText() {
    return $("//div[@id='ccCard']/h6[1]")
  }

  get phoneText() {
    return $("//div[@id='ccCard']/h6[@id='ccPhone']")
  }

  get statusText() {
    return $("//div[@id='ccCard']/h6/span")
  }

  //Caregivers Table Section,need to check

  get noCareGiversMessage() {
    return $("//div[@class='no-caregivers-data']")
  }

  get firstNameOnTable() {
    return $("//table[contains(@class,'table')]//tr[@class][1]//td[1]")
  }
  get lastNameOnTable() {
    return $("//table[contains(@class,'table')]//tr[@class][1]//td[2]")
  }

  get emailOnTable() {
    return $("//table[contains(@class,'table')]//tr[@class][1]//td[3]")
  }

  get employerOnTable() {
    return $("//table[contains(@class,'table')]//tr[@class][1]//td[4]")
  }
  get refreshIconOfFirstRecord() {
    return $(
      "//table[contains(@class,'table')]//tr[@class][1]//td//*[name()='svg']"
    )
  }

  //Re-Assign Caregiver Model,need to check

  get headingOnReAssignedCaregiverModel() {
    return $(
      "//tr[@class='p-0'][2]//div[contains(@class,'fade modal show')]//h5"
    )
  }

  get cancelBtnOnModel() {
    return $("//tr[@class='p-0'][2]//button[text()='Cancel']")
  }

  get careGiverNameTextOnModel() {
    return $("//tr[@class='p-0'][2]//label[@id='ccFirstName'][1]")
  }

  get careCoachNameTextOnModel() {
    return $("//tr[@class='p-0'][2]//label[@id='ccFirstName'][2]")
  }

  //Care Coach Profile Model

  get headingOnCareCoachProfileModel() {
    return $("//div[contains(@class,'.my-modal-window')]//h5")
  }

  get careCoachProfileMessage() {
    return $("//h3[@id='messageOnModel']")
  }

  get closeBtnOnModel() {
    return $("//button[text()='Close']")
  }

  get careCoachNameTextOnProfileModel() {
    return $(
      "//h6[text()='Name']/ancestor::label/following-sibling::p[@id='ccName']"
    )
  }
  get pronounsOnProfileModel() {
    return $(
      "//h6[text()='Pronouns']/ancestor::label/following-sibling::p[@id='ccPronouns']"
    )
  }
  get locationOnProfileModel() {
    return $(
      "//h6[text()='Location']/ancestor::label/following-sibling::p[@id='ccLocation']"
    )
  }
  get startDateOnProfileModel() {
    return $(
      "//h6[text()='Start Date / Years of Experience']/ancestor::label/following-sibling::p[@id='ccJobStartDate']"
    )
  }

  get credentialsOnProfileModel() {
    return $(
      "//h6[text()='Credentials']/ancestor::label/following-sibling::p[@id='ccSpecialities']"
    )
  }

  get aboutStatementOnProfileModel() {
    return $(
      "//h6[text()='About Statement']/ancestor::label/following-sibling::p[@id='ccAbout']"
    )
  }

  get profilePhotoOnProfileModel() {
    return $(
      "//h6[text()='Photo']/ancestor::label/following-sibling::div[@class='media']/img"
    )
  }

  waitForCareCoachDetailViewPageToLoad() {
    if (!this.subHeadingOnViewCareCoachPage.isDisplayed()) {
      this.subHeadingOnViewCareCoachPage.waitForDisplayed(3000)
    }
  }

  getCareCoachViewPageSubHeading() {
    this.waitForCareCoachDetailViewPageToLoad()
    return this.subHeadingOnViewCareCoachPage.getText()
  }

  waitForCareCoachProfileModelToLoad() {
    if (!this.headingOnCareCoachProfileModel.isDisplayed()) {
      this.headingOnCareCoachProfileModel.waitForDisplayed(3000)
    }
  }

  getHeadingOnCareCoachProfileModel() {
    this.waitForCareCoachProfileModelToLoad()
    return this.headingOnCareCoachProfileModel.getText()
  }

  waitForReAssignedCaregiverModelToLoad() {
    if (!this.headingOnReAssignedCaregiverModel.isDisplayed()) {
      this.headingOnReAssignedCaregiverModel.waitForDisplayed(3000)
    }
  }

  getHeadingOnReAssignedCaregiverModel() {
    this.waitForReAssignedCaregiverModelToLoad()
    return this.headingOnReAssignedCaregiverModel.getText()
  }

  verifyCareCoachViewDetailsWithCareCoachesListPage() {
    try {
      let name =
        localStorage.getItem("ccFirstNameOnTable") +
        " " +
        localStorage.getItem("ccLastNameOnTable")
      name.should.equal(this.ccNameOnViewCareCoachDetails.getText())

      let ccEmail = this.emailText.getText()
      ccEmail = ccEmail.substring(ccEmail.indexOf(" ") + 1)

      let ccPhone = this.phoneText.getText()
      ccPhone = ccPhone.substring(ccPhone.indexOf(" ") + 1)

      localStorage.getItem("ccEmailOnTable").should.equal(ccEmail)
      localStorage.getItem("ccPhoneOnTable").should.equal(ccPhone)
      localStorage
        .getItem("ccStatusOnTable")
        .should.equal(this.statusText.getText())
      this.calendlyUsernameLabel.isDisplayed().should.be.true
      localStorage._deleteLocation()
      return true
    } catch (err) {
      return false
    }
  }

  verifyEditAndProfilePreviewButton() {
    try {
      if (
        this.editButton.isDisplayed() &&
        this.previewProfileButton.isDisplayed()
      ) {
        return true
      } else {
        return false
      }
    } catch (err) {
      return false
    }
  }

  clickOnEditButton() {
    global.localStorage = new LocalStorage("./scratch")
    var ccName = this.ccNameOnViewCareCoachDetails.getText()
    let ccEmail = this.emailText.getText()
    ccEmail = ccEmail.substring(ccEmail.indexOf(" ") + 1)
    let ccPhone = this.phoneText.getText()
    ccPhone = ccPhone.substring(ccPhone.indexOf(" ") + 1)

    localStorage.setItem(
      "ccFirstName",
      ccName.substring(0, ccName.indexOf(" "))
    )
    localStorage.setItem(
      "ccLastName",
      ccName.substring(ccName.indexOf(" ") + 1)
    )
    localStorage.setItem("ccEmail", ccEmail)
    localStorage.setItem("ccPhone", ccPhone)
    localStorage.setItem("ccStatus", this.statusText.getText())
    this.editButton.click()
    browser.pause(3000)
  }

  clickOnProfilePreviewButton() {
    global.localStorage = new LocalStorage("./scratch")
    localStorage.setItem("ccName", this.ccNameOnViewCareCoachDetails.getText())
    this.previewProfileButton.click()
  }

  verifyCareCoachProfileDetailsOnModel() {
    try {
      this.getHeadingOnCareCoachProfileModel().should.equal(
        "Care Coach Profile"
      )
      try {
        this.careCoachProfileMessage
          .getText()
          .should.equal("This Care Coach has not filled out their profile yet.")
      } catch (err) {
        this.careCoachNameTextOnProfileModel
          .getText()
          .should.equal(localStorage.getItem("ccName"))
        this.pronounsOnProfileModel.isDisplayed().should.be.true
        this.locationOnProfileModel.isDisplayed().should.be.true
        this.startDateOnProfileModel.isDisplayed().should.be.true
        this.credentialsOnProfileModel.isDisplayed().should.be.true
        this.aboutStatementOnProfileModel.isDisplayed().should.be.true
        this.profilePhotoOnProfileModel.isDisplayed().should.be.true
        this.closeBtnOnModel.isDisplayed().should.be.true
        localStorage._deleteLocation()
      }
      return true
    } catch (err) {
      return false
    }
  }

  clickOnCloseButtonOnModel() {
    this.closeBtnOnModel.click()
    browser.pause(2000)
  }

  verifyAssignedCaregiversTableAndReAssignedCareCoach() {
    try {
      try {
        this.firstNameOnTable.isDisplayed().should.be.true
        this.lastNameOnTable.isDisplayed().should.be.true
        this.emailOnTable.isDisplayed().should.be.true
        this.employerOnTable.isDisplayed().should.be.true
        this.refreshIconOfFirstRecord.isDisplayed().should.be.true
        var firstName = this.firstNameOnTable.getText()
        var lastName = this.lastNameOnTable.getText()
        let careGiverName = firstName + " " + lastName
        var careCoachName = this.ccNameOnViewCareCoachDetails.getText()
        this.refreshIconOfFirstRecord.click()

        this.getHeadingOnReAssignedCaregiverModel().should.equal(
          "Reassign Caregiver"
        )
        careGiverName.should.equal(this.careGiverNameTextOnModel.getText())
        careCoachName.should.equal(this.careCoachNameTextOnModel.getText())
      } catch (err) {
        this.noCareGiversMessage
          .getText()
          .should.equal("There are no Caregivers assigned to this Care Coach.")
      }
      return true
    } catch (err) {
      return false
    }
  }
}

export default new careCoachViewDetailsPage()
