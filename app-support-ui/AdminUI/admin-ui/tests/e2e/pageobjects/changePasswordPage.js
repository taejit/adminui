import Page from "./page"
const fs = require("fs")
const filePath = "tests/e2e/config/user.conf.json"

class ChangePasswordPage extends Page {
  /**
   * define elements
   */

  get changePasswordTab() {
    return $("//li/a[text()='Change Password']")
  }

  get currentPasswordInput() {
    return $("//input[@name='currentPassword']")
  }

  get newPasswordInput() {
    return $("//input[@name='newPassword']")
  }

  get reEnterNewPasswordInput() {
    return $("//input[@name='retypePassword']")
  }

  get updateButton() {
    return $("//button[text()='Update' and contains(@class,'update-password')]")
  }

  get errorMessageBanner() {
    return $("//div[@class='text-danger']")
  }

  get successMessageBanner() {
    return $("//div[@class='text-success']")
  }
  /**
   * page specific methods
   */
  getChangePasswordTab() {
    return this.changePasswordTab.getText()
  }

  clickOnChangePasswordTab() {
    this.changePasswordTab.click()
    browser.pause(3000)
  }

  enterCurrentPassword(currentPassword) {
    this.currentPasswordInput.clearValue()
    this.currentPasswordInput.setValue(currentPassword)
    browser.pause(1000)
  }

  enterNewPassword(newPassword) {
    this.newPasswordInput.clearValue()
    this.newPasswordInput.setValue(newPassword)
    browser.pause(1000)
  }

  enterReEnterNewPassword(reEnterNewPassword) {
    this.reEnterNewPasswordInput.clearValue()
    this.reEnterNewPasswordInput.setValue(reEnterNewPassword)
    browser.pause(1000)
  }

  clickOnUpdateButton() {
    this.updateButton.click()
    browser.pause(1000)
  }

  getErrorMessage() {
    return this.errorMessageBanner.getText()
  }

  getSuccessMessgae() {
    return this.successMessageBanner.getText()
  }

  swapPasswords(pwd, tmp_pwd, username) {
    let usrObj = { username: username, password: tmp_pwd, temp_password: pwd }

    fs.writeFileSync(filePath, JSON.stringify(usrObj), "utf-8", (err) => {
      if (err) {
        console.error("Error in writing data in config file", err)
        return
      }
      console.log("swapping :", usrObj)
      console.log("wrote to file successfully")
    })
  }
}

export default new ChangePasswordPage()
