import Page from "./page"
import { LocalStorage } from "node-localstorage"
import util from "../utilities/util"

class EditEmployerPage extends Page {
  /**
   * define elements
   */

  get editEmployerNameInput() {
    return $("//input[@name='editEmpName']")
  }
  get editContactNameInput() {
    return $("//input[@name='contactName']")
  }
  get editEmailAddressInput() {
    return $("//input[@name='editEmail']")
  }
  get editPhoneInput() {
    return $("//input[@name='phone']")
  }
  get editUniqueEmployeeIdentifierDropDown() {
    return $("//select[@name='editUniqueIdentifier']")
  }
  get saveButton() {
    return $("//button[text()='Save']")
  }
  get employerHeader() {
    return $("//h4")
  }

  get statusToggleButton() {
    return $("//input[@name='isActive']/following-sibling::span")
  }

  get statusText() {
    return $("//span[contains(@class,'status-text')]")
  }

  get headerTextOnModel() {
    return $("//h5[@class='modal-title']")
  }

  get cancelBtnOnModel() {
    return $("//button[.='Cancel']")
  }

  get continueBtnOnModel() {
    return $("//button[.='Continue']")
  }

  waitForEmployerPageToLoad() {
    if (!this.employerHeader.isDisplayed()) {
      this.employerHeader.waitForDisplayed(5000)
    }
  }

  getEmployersHeadeing() {
    this.waitForEmployerPageToLoad()
    return this.employerHeader.getText()
  }

  updateDataOnEditEmployerPage(name, uniquIdent) {
    //verify the first row records with the edit care coach fields
    this.editEmployerNameInput
      .getValue()
      .should.equal(localStorage.getItem("employerName"))
    this.editContactNameInput
      .getValue()
      .should.equal(localStorage.getItem("contactName"))
    this.statusText.getText().should.equal(localStorage.getItem("empStatus"))
    localStorage._deleteLocation()

    //updating new records on edit employer page
    util.clearValue(this.editEmployerNameInput)
    this.editEmployerNameInput.setValue("employer_" + name)
    util.clearValue(this.editContactNameInput)
    this.editContactNameInput.setValue("contact_" + name)
    util.clearValue(this.editEmailAddressInput)
    this.editEmailAddressInput.setValue("employer_" + name + "@mailinator.com")
    let currentPhone = this.editPhoneInput.getValue()
    let phone = util.getAnyPhoneFromListExceptExistingPhone(currentPhone)
    util.clearValue(this.editPhoneInput)
    this.editPhoneInput.setValue(phone)
    browser.pause(1000)
    this.editUniqueEmployeeIdentifierDropDown.selectByVisibleText(uniquIdent)
    browser.pause(1000)
  }

  clickOnSaveButton() {
    global.localStorage = new LocalStorage("./scratch")
    localStorage.setItem("employerName", this.editEmployerNameInput.getValue())
    localStorage.setItem("contactName", this.editContactNameInput.getValue())
    localStorage.setItem("employerStatus", this.statusText.getText())
    this.saveButton.click()
    browser.pause(3000)
  }

  waitForDeActivatAccountModelToLoad() {
    if (!this.headerTextOnModel.isDisplayed()) {
      this.headerTextOnModel.waitForDisplayed(2000)
    }
  }

  getDeActivateAccountHeadingOnModel() {
    this.waitForDeActivatAccountModelToLoad()
    return this.headerTextOnModel.getText()
  }

  getCancelButtonOnModel() {
    return this.cancelBtnOnModel.getText()
  }

  getContinueButtonOnModel() {
    return this.continueBtnOnModel.getText()
  }

  clickOnCancelButtonOnModel() {
    this.cancelBtnOnModel.click()
    browser.pause(2000)
  }

  clickOnContinueButtonOnModel() {
    this.continueBtnOnModel.click()
    browser.pause(2000)
  }

  getStatusTextOnEditEmployerPage() {
    return this.statusText.getText()
  }

  clickOnStatusToggleButton() {
    this.statusToggleButton.click()
  }
}

export default new EditEmployerPage()
