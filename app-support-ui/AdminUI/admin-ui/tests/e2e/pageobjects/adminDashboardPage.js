import Page from "./page"
class AdminDashboardPage extends Page {
  /**
   * define elements
   */

  get dashboardEmployersHeading() {
    return $("//h1[@class='title']")
  }
  get menuLink() {
    return $("//div[text()='Menu']")
  }
  get logoutLink() {
    return $("//a[@class='dropdown-item' and text()='Log Out']")
  }
  get profileLink() {
    return $("//a[@class='dropdown-item' and text()='Profile']")
  }
  get addNewButton() {
    return $("//button[text()='Add New']")
  }
  get ChangesSaved() {
    return $(
      "//div[contains(@class,'alert-success') and text()='Changes Saved']"
    )
  }
  get editIconOfFirstRecord() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]//td/*[name()='svg'][1]"
    )
  }
  get employerNameOnTable() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]/td[@class='emp-name'][1]"
    )
  }
  get contactNameOnTable() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]/td[@class='emp-contact'][1]"
    )
  }

  get statusOnTable() {
    return $("//table[@class='table']//tr[not(@class)][1]/td[3]//span")
  }

  get employerTable() {
    return $("//h1[text()='Employers']/following-sibling::table")
  }

  get careCoachSideBarMenu() {
    return $("//a[text()='Care Coaches']")
  }

  waitForDashboardPageToLoad() {
    if (!this.dashboardEmployersHeading.isDisplayed()) {
      this.dashboardEmployersHeading.waitForDisplayed(5000)
    }
  }

  getEmployersHeadeing() {
    browser.pause(5000)
    this.waitForDashboardPageToLoad()
    return this.dashboardEmployersHeading.isDisplayed()
  }

  getEmployerNameOnTable() {
    return this.employerNameOnTable.getText()
  }

  getContactNameOnTable() {
    return this.contactNameOnTable.getText()
  }

  getStatusOnEmployerList() {
    return this.statusOnTable.getText()
  }

  changesSavedMessage() {
    return this.ChangesSaved.getText()
  }

  clickOnMenuLink() {
    this.menuLink.click()
    browser.pause(2000)
  }

  clickOnProfileLink() {
    this.profileLink.click()
    browser.pause(2000)
  }

  clickOnLogoutLink() {
    this.logoutLink.click()
    browser.pause(5000)
  }

  clickOnAddNewButton() {
    this.addNewButton.click()
    browser.pause(3000)
  }

  clickOnEditIconOfFirstRecord() {
    this.editIconOfFirstRecord.click()
    browser.pause(3000)
  }

  getEmployerTable() {
    browser.pause(5000)
    return this.employerTable.isDisplayed()
  }

  clickOnCareCoachesSideBarMenu() {
    this.careCoachSideBarMenu.click()
    browser.pause(1000)
  }
}

export default new AdminDashboardPage()
