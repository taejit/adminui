import Page from "./page"
import { LocalStorage } from "node-localstorage"
let lsEmployerName, lsContactName
class AddEmployerPage extends Page {
  /**
   * define elements
   */

  get employerNameInput() {
    return $("//input[@name='employerName']")
  }
  get contactNameInput() {
    return $("//input[@name='contactName']")
  }
  get emailAddressInput() {
    return $("//input[@name='email']")
  }
  get phoneInput() {
    return $("//input[@name='phone']")
  }
  get UniqueEmployeeIdentifierDropDown() {
    return $("//select[@name='uniqueIdentifier']")
  }
  get saveButton() {
    return $("//button[text()='Save']")
  }
  get employerHeader() {
    return $("//h4")
  }

  waitForEmployerPageToLoad() {
    if (!this.employerHeader.isDisplayed()) {
      this.employerHeader.waitForDisplayed(5000)
    }
  }

  getEmployersHeadeing() {
    this.waitForEmployerPageToLoad()
    return this.employerHeader.getText()
  }

  enterDataOnAddEmployerPage(name, phone, uniquIdent) {
    this.employerNameInput.setValue("employer_" + name)
    browser.pause(1000)
    this.contactNameInput.setValue("contact_" + name)
    browser.pause(1000)
    this.emailAddressInput.setValue("employer_" + name + "@mailinator.com")
    browser.pause(1000)
    this.phoneInput.setValue(phone)
    browser.pause(1000)
    this.UniqueEmployeeIdentifierDropDown.selectByVisibleText(uniquIdent)
    browser.pause(1000)
    lsEmployerName = "employer_" + name
    lsContactName = "contact_" + name
  }

  clickOnSaveButton() {
    this.saveButton.click()
    global.localStorage = new LocalStorage("./scratch")
    localStorage.setItem("employerName", lsEmployerName)
    localStorage.setItem("contactName", lsContactName)
    localStorage.setItem("employerStatus", "Active")
    browser.pause(3000)
  }
}

export default new AddEmployerPage()
