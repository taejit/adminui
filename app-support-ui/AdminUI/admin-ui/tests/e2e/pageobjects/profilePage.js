import Page from "./page"
import util from "../utilities/util"

class ProfilePage extends Page {
  /**
   * define elements
   */

  get profileHeader() {
    return $("//h4")
  }

  get nameInput() {
    return $("//input[@name='name']")
  }

  get phoneInput() {
    return $("//input[@name='phoneNumber']")
  }

  get emailAddressDisabled() {
    return $("//input[@name='emailAddress']")
  }

  get updateButton() {
    return $("//button[text()='Update' and contains(@class,'update-about')]")
  }

  get aboutTab() {
    return $("//li/a[text()='About]")
  }

  get successMessageBanner() {
    return $("//div[@class='text-success']")
  }

  waitForProfileageToLoad() {
    if (!this.profileHeader.isDisplayed()) {
      this.profileHeader.waitForDisplayed(3000)
    }
  }

  getProfileHeadeing() {
    this.waitForProfileageToLoad()
    return this.profileHeader.getText()
  }

  getStateOfAboutTab() {
    this.aboutTab.getAttribute("class")
  }

  clickOnUpdateButton() {
    this.updateButton.click()
    browser.pause(3000)
  }

  getSuccessMessgae() {
    return this.successMessageBanner.getText()
  }

  updateDataOnProfilePage() {
    util.clearValue(this.nameInput)
    this.nameInput.setValue(util.generateName(6))
    let currentPhone = this.phoneInput.getValue()
    let phone = util.getAnyPhoneFromListExceptExistingPhone(currentPhone)
    util.clearValue(this.phoneInput)
    this.phoneInput.setValue(phone)
  }

  validateUIFieldsOnProfilePage() {
    try {
      if (
        this.nameInput.getValue() !== "" &&
        this.emailAddressDisabled.getValue() !== "" &&
        this.phoneInput.getValue() !== ""
      ) {
        return true
      } else {
        return false
      }
    } catch (err) {
      return false
    }
  }

  validateEmailAddressDisabled() {
    if (this.emailAddressDisabled.isEnabled()) {
      return false
    } else {
      return true
    }
  }
}

export default new ProfilePage()
