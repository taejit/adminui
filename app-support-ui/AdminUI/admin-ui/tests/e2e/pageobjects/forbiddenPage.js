import Page from "./page"

class ForbiddenPage extends Page {
  /**
   * define elements
   */

  get forbiddenLabel() {
    return $("//h2")
  }
  get forbiddenMessageLabel() {
    return $("//h6[1]")
  }
  get goToLoginButton() {
    return $("//button[.='Go to Login']")
  }

  waitForForbiddenPageToLoad() {
    if (!this.forbiddenLabel.isDisplayed()) {
      this.forbiddenLabel.waitForDisplayed(5000)
    }
  }
  getForbiddenLabel() {
    this.waitForForbiddenPageToLoad()
    return this.forbiddenLabel.getText()
  }

  getForbiddenMessage() {
    return this.forbiddenMessageLabel.getText()
  }

  clickOnGoToLoginButton() {
    this.goToLoginButton.click()
    browser.pause(5000)
  }
}

export default new ForbiddenPage()
