import Page from "./page"
import { LocalStorage } from "node-localstorage"
import util from "../utilities/util"

class CareCoachPage extends Page {
  /**
   * define elements
   */
  get careCoachesHeadingOnList() {
    return $("//h4[@class='title']")
  }

  get careCoachHeadingOnPage() {
    return $("//h4[@id='care-coach-header']")
  }

  get addNewButton() {
    return $("//button[text()='Add New']")
  }

  get firstNameInput() {
    return $("//input[@id='ccFirstName']")
  }

  get lastNameInput() {
    return $("//input[@id='ccLastName']")
  }

  get emailInput() {
    return $("//input[@id='ccEmail']")
  }

  get phoneInput() {
    return $("//input[@id='ccPhone']")
  }
  get calendlyUsernameInput() {
    return $("//input[@id='cccalendlyUsername']")
  }

  get statusToggleButton() {
    return $("//input[@name='isActive']/following-sibling::span")
  }

  get statusText() {
    return $("//span[contains(@class,'status-text')]")
  }

  get headerTextOnModel() {
    return $("//h5[@class='modal-title']")
  }

  get cancelBtnOnModel() {
    return $("//button[.='Cancel']")
  }

  get continueBtnOnModel() {
    return $("//button[.='Continue']")
  }

  get saveButton() {
    return $("//button[@id='saveBtn']")
  }

  get ChangesSaved() {
    return $(
      "//div[contains(@class,'alert-success') and text()='Changes Saved']"
    )
  }
  get editIconOfFirstRecord() {
    return $(
      "//table[contains(@class,'table')]//tr[@class][1]//td[not(@class)]/*[name()='svg' and contains(@class,'edit-carecoach')]"
    )
  }
  get viewIconOfFirstRecord() {
    return $(
      "//table[contains(@class,'table')]//tr[@class][1]//td[not(@class)]/*[name()='svg' and contains(@class,'view-care-coach')]"
    )
  }

  get firstNameOnTable() {
    return $(
      "//table[contains(@class,'table')]//tr[@class][1]//td[@class='firstName']"
    )
  }
  get lastNameOnTable() {
    return $(
      "//table[contains(@class,'table')]//tr[@class][1]//td[@class='lastName']"
    )
  }

  get emailOnTable() {
    return $(
      "//table[contains(@class,'table')]//tr[@class][1]//td[@class='email']"
    )
  }

  get phoneNumberOnTable() {
    return $(
      "//table[contains(@class,'table')]//tr[@class][1]//td[@class='phoneNumber']"
    )
  }
  get statusOnTable() {
    return $(
      "//table[contains(@class,'table')]//tr[@class][1]//td[@class='isActive']/span"
    )
  }

  get careCoachTable() {
    return $("//h4[@class='title']/ancestor::div[@class='card']//table")
  }

  waitForCareCaochListPageToLoad() {
    if (!this.careCoachesHeadingOnList.isDisplayed()) {
      browser.pause(3000)
      this.careCoachesHeadingOnList.waitForDisplayed(3000)
    }
  }

  getCareCoachListHeading() {
    this.waitForCareCaochListPageToLoad()
    return this.careCoachesHeadingOnList.getText()
  }

  getFirstNameOnCareCoachTable() {
    return this.firstNameOnTable.getText()
  }

  getLastNameOnCareCoachTable() {
    return this.lastNameOnTable.getText()
  }

  getEmailOnCareCoachTable() {
    return this.emailOnTable.getText()
  }

  getPhoneNumberOnCareCoachTable() {
    return this.phoneNumberOnTable.getText()
  }

  getStatusOnCareCoachTable() {
    return this.statusOnTable.getText()
  }

  changesSavedMessage() {
    return this.ChangesSaved.getText()
  }

  clickOnAddNewButton() {
    this.addNewButton.click()
    browser.pause(3000)
  }

  clickOnEditIconOfFirstRecord() {
    this.editIconOfFirstRecord.click()
    browser.pause(3000)
  }

  clickOnViewIconOfFirstRecord() {
    this.viewIconOfFirstRecord.click()
    browser.pause(3000)
  }

  getCareCoachTable() {
    return this.careCoachTable.isDisplayed()
  }

  getStatusOnCareCoachList() {
    return this.statusOnTable.getText()
  }

  waitForCareCoachPageToLoad() {
    if (!this.careCoachHeadingOnPage.isDisplayed()) {
      this.careCoachHeadingOnPage.waitForDisplayed(5000)
    }
  }

  getCareCoachPageHeading() {
    this.waitForCareCoachPageToLoad()
    return this.careCoachHeadingOnPage.getText()
  }

  enterDataOnAddCareCoachPage(name, phone) {
    this.firstNameInput.setValue("fn_" + name)
    browser.pause(1000)
    this.lastNameInput.setValue("ln_" + name)
    browser.pause(1000)
    this.emailInput.setValue(name + "@mailinator.com")
    browser.pause(1000)
    this.phoneInput.setValue(phone)
    browser.pause(1000)
    this.calendlyUsernameInput.setValue("lantern-integration")
    browser.pause(1000)
  }

  updateDataOnEditCareCoachPage(name) {
    //verify the first row records with the edit care coach fields
    this.firstNameInput
      .getValue()
      .should.equal(localStorage.getItem("ccFirstName"))
    this.lastNameInput
      .getValue()
      .should.equal(localStorage.getItem("ccLastName"))
    this.emailInput.getValue().should.equal(localStorage.getItem("ccEmail"))
    this.phoneInput.getValue().should.equal(localStorage.getItem("ccPhone"))
    this.statusText.getText().should.equal(localStorage.getItem("ccStatus"))
    localStorage._deleteLocation()

    //updating new records on edit care coach page
    util.clearValue(this.firstNameInput)
    this.firstNameInput.setValue(name + "_fn")
    util.clearValue(this.lastNameInput)
    this.lastNameInput.setValue(name + "_ln")
    util.clearValue(this.emailInput)
    this.emailInput.setValue("edit_" + name + "@mailinator.com")
    let currentPhone = this.phoneInput.getValue()
    let phone = util.getAnyPhoneFromListExceptExistingPhone(currentPhone)
    util.clearValue(this.phoneInput)
    this.phoneInput.setValue(phone)
    browser.pause(1000)
  }

  clickOnSaveButton() {
    global.localStorage = new LocalStorage("./scratch")
    localStorage.setItem("firstName", this.firstNameInput.getValue())
    localStorage.setItem("lastName", this.lastNameInput.getValue())
    localStorage.setItem("email", this.emailInput.getValue())
    localStorage.setItem("phone", this.phoneInput.getValue())
    try {
      localStorage.setItem("status", this.statusText.getText())
    } catch (err) {
      localStorage.setItem("status", "Active")
    }
    this.saveButton.click()
    browser.pause(3000)
  }

  waitForDeActivatCareCoachModelToLoad() {
    if (!this.headerTextOnModel.isDisplayed()) {
      this.headerTextOnModel.waitForDisplayed(2000)
    }
  }

  getDeActivateCareCoachHeadingOnModel() {
    this.waitForDeActivatCareCoachModelToLoad()
    return this.headerTextOnModel.getText()
  }

  getCancelButtonOnModel() {
    return this.cancelBtnOnModel.getText()
  }

  getContinueButtonOnModel() {
    return this.continueBtnOnModel.getText()
  }

  clickOnCancelButtonOnModel() {
    this.cancelBtnOnModel.click()
    browser.pause(2000)
  }

  clickOnContinueButtonOnModel() {
    this.continueBtnOnModel.click()
    browser.pause(2000)
  }

  getStatusTextOnEditCareCoachPage() {
    return this.statusText.getText()
  }

  clickOnStatusToggleButton() {
    this.statusToggleButton.click()
  }

  verifyCareCoachViewDetailsWithEditCareCoachPage() {
    try {
      this.firstNameInput
        .getValue()
        .should.equal(localStorage.getItem("ccFirstName"))
      this.lastNameInput
        .getValue()
        .should.equal(localStorage.getItem("ccLastName"))
      this.emailInput.getValue().should.equal(localStorage.getItem("ccEmail"))
      this.phoneInput.getValue().should.equal(localStorage.getItem("ccPhone"))
      this.statusText.getText().should.equal(localStorage.getItem("ccStatus"))
      localStorage._deleteLocation()
      return true
    } catch (err) {
      return false
    }
  }
}

export default new CareCoachPage()
