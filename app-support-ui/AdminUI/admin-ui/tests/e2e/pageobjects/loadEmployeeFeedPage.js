import Page from "./page"
const path = require("path")
var parentDir = path.dirname(__dirname)

class LoadEmployeeFeedPage extends Page {
  /**
   * define elements
   */

  get fileUpload() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]//td/label[@class='fileLabel']//input[@id='file']"
    )
  }
  get uploadIconOfFirstRecord() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]//td/label[@class='fileLabel']"
    )
  }

  get uploadStatusOnTable() {
    return $("//table[@class='table']//tr[not(@class)][1]/td[4]//span")
  }

  get downloadFailedIconOfFirstRecord() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]//td/button[@class='button-failed']"
    )
  }

  get reloadIconOfFirstRecord() {
    return $(
      "//table[@class='table']//tr[not(@class)][1]//td/button[@class='button-reload']"
    )
  }

  getUploadStatusOnTable() {
    return this.uploadStatusOnTable.getText()
  }

  clickOnUploadIconOfFirstRecord() {
    this.uploadIconOfFirstRecord.click()
    browser.pause(1000)
  }

  clickOnReloadIconOfFirstRecord() {
    let ctr = 0
    do {
      this.reloadIconOfFirstRecord.click()
      browser.pause(1000)
      ctr++
    } while (
      !(
        this.uploadStatusOnTable.getText() === "Success" ||
        this.uploadStatusOnTable.getText() === "Failed"
      ) &&
      ctr < 10
    )
  }

  validateReloadIconShouldNotVisible() {
    if (this.reloadIconOfFirstRecord.isDisplayed()) {
      return false
    } else {
      return true
    }
  }

  validateDownloadFailedIconShouldVisible() {
    if (this.downloadFailedIconOfFirstRecord.isDisplayed()) {
      return true
    } else {
      return false
    }
  }

  uploadCSVFile(fileName) {
    var filePath = path.join(
      parentDir + "/utilities",
      fileName + "-employees.csv"
    )
    browser.pause(1000)
    browser.execute((el) => (el.style.display = "block"), this.fileUpload)
    this.fileUpload.waitForDisplayed()
    this.fileUpload.setValue(filePath)
    browser.pause(1000)

    browser.execute((el) => (el.style.display = "none"), this.fileUpload)
    browser.pause(5000)
  }
}

export default new LoadEmployeeFeedPage()
