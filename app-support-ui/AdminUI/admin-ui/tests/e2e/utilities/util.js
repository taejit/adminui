/* eslint-disable no-undef */
const CryptoJS = require("crypto-js")

const encryptedBase64Key = "bXVzdGJlMTZieXRlc2tleQ=="
var parsedBase64Key = CryptoJS.enc.Base64.parse(encryptedBase64Key)
const filePath = "tests/e2e/config/user.conf.json"
const fs = require("fs")

const phone_array = [
  5042010052,
  2029674478,
  2029674530,
  4047772394,
  2027953213,
  2014222730,
  2014222656,
  2243238312,
]

class Util {
  generateName(length) {
    var result = ""
    var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    var charactersLength = characters.length
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
  }

  generateNumber(length) {
    return Math.floor(
      Math.pow(10, length - 1) + Math.random() * 9 * Math.pow(10, length - 1)
    )
  }

  checkValueExistInColumn(columnList, textToBeVarified) {
    var result = false
    for (let i = 0; i < columnList.length; i++) {
      if (columnList[i].getText().equal(textToBeVarified)) {
        result = true
        break
      }
    }
    return result
  }

  clearValue(inputElement) {
    if (inputElement.getValue().indexOf(" ") !== -1) {
      for (let i = 0; i < 5; i++) {
        inputElement.doubleClick()
        browser.pause(1000)
        inputElement.keys("\uE003")
      }
    } //this if block to clear phone input field
    else if (
      inputElement.getValue().indexOf("@") !== -1 ||
      inputElement.getValue().indexOf("(") !== -1
    ) {
      for (let i = 0; i < 5; i++) {
        inputElement.doubleClick()
        browser.pause(1000)
        inputElement.keys("\uE003")
      }
    } //this if block to clear phone input field
    else {
      inputElement.doubleClick()
      browser.pause(1000)
      inputElement.keys("\uE003") //backspace
    } //this to clear any input field
  }

  getUserDetails() {
    const data = fs.readFileSync(filePath, { encoding: "utf8", flag: "r" })
    return JSON.parse(data)
  }

  getAnyPhoneFromList() {
    return phone_array[Math.floor(Math.random() * phone_array.length)]
  }

  getAnyPhoneFromListExceptExistingPhone(phone) {
    var phn = phone_array[Math.floor(Math.random() * phone_array.length)]
    if (phn === phone) {
      return getAnyPhoneFromListExceptExistingPhone(phone)
    }
    return phn
  }

  checkDataNotEquals(fields, value) {
    if (fields.getText().equal(value)) {
      return false
    }
    return true
  }

  selectValueFromDropDown(field, value) {
    try {
      field.selectByIndex(value)
    } catch (err) {
      field.selectByIndex(0)
    }
  }

  encryptedData(plaintText) {
    return CryptoJS.AES.encrypt(plaintText, parsedBase64Key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7,
    })
  }

  decryptedText(encryptedCipherText) {
    var decryptedData = CryptoJS.AES.decrypt(
      encryptedCipherText,
      parsedBase64Key,
      {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7,
      }
    )
    return decryptedData.toString(CryptoJS.enc.Utf8)
  }
}

export default new Util()
