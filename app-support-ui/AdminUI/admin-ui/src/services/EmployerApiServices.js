import axios from "axios"
import userManager from "../Auth/OidcAuth"

async function _getAuthHeader() {
  let user = await userManager.getUser()
  let token = user ? user.id_token : ""
  return {
    Authorization: `Bearer ${token}`,
  }
}

const employerApiService = {
  createEmployer: async (requestObj) => {
    try {
      const response = await axios({
        method: "post",
        url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/employers`,
        data: requestObj,
        headers: await _getAuthHeader(),
      })
      return response
    } catch (error) {
      return error
    }
  },

  getEmployer: async (uuid) => {
    try {
      const response = await axios({
        method: "get",
        url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/employers/${uuid}`,
        headers: await _getAuthHeader(),
      })
      return response
    } catch (error) {
      return error
    }
  },

  updateEmployer: async (requestObj, uuid) => {
    try {
      const response = await axios({
        method: "PUT",
        url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/employers/${uuid}`,
        data: requestObj,
        headers: await _getAuthHeader(),
      })
      return response
    } catch (error) {
      return error
    }
  },

  getEmployers: async () => {
    const employersResponse = await axios({
      method: "get",
      url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/employers`,
      headers: await _getAuthHeader(),
    })
    return employersResponse
  },

  getUploadStatusFromApi: async (uuid) => {
    const uploadstate = await axios({
      method: "get",
      url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/employers/${uuid}/employee-uploads/latest`,
      headers: await _getAuthHeader(),
    })
    return uploadstate
  },

  getPresignedUrl: async (requestObj, uuid) => {
    const response = await axios({
      method: "POST",
      url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/employers/${uuid}/employee-uploads`,
      data: requestObj,
      headers: await _getAuthHeader(),
    })
    return response
  },

  uploadFileThroughPresignedUrl: async (url, requestObj) => {
    const response = await axios({
      method: "PUT",
      url,
      data: requestObj,
      headers: {
        "Content-Type": "text/csv",
      },
    })
    return response
  },
}

export default employerApiService
