import axios from "axios"
import CareCoachAPIService from "./CareCoachAPIService"
import userManager from "../Auth/OidcAuth"

jest.mock("axios")
jest.mock("../Auth/OidcAuth")

const MOCK_ID_TOKEN = "MOCK_ID_TOKEN"

const MOCK_USER = {
  id_token: MOCK_ID_TOKEN,
}
const MOCK_USER_PROMISE = Promise.resolve(MOCK_USER)

describe("CareCoachAPIService", () => {
  beforeEach(() => {})
  afterEach(() => {
    jest.restoreAllMocks()
  })
  it("should put data if status code equals 201", async () => {
    userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)

    const requestObj = {
      firstName: "John",
      lastName: "Doe",
      nickName: "Joe",
      email: "example@lantern.care",
      phone: "6021234567",
      location: "Gilbert, Arizona",
      pronouns: "He/him",
      isActive: true,
    }
    const uuid = "daad0c7c-0ab2-4bea-8d96-2512cff733e0"
    jest.spyOn(axios, "default").mockResolvedValue({
      status: 202,
    })

    const actual = await CareCoachAPIService.syncCareCoach(requestObj, uuid)
    expect(actual).toEqual({
      status: 202,
    })
    expect(axios).toHaveBeenCalled()
  })
  it("should put data if status code equals 201", async () => {
    userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)

    const requestObj = {
      id: 6,
      uuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
      firstName: "John",
      lastName: "Doe",
      nickName: "Joe",
      email: "example@lantern.care",
      phone: "6021234567",
      pronouns: "He/him",
      location: "Gilbert, Arizona",
      jobTitle: null,
      jobStartDate: null,
      specialities: null,
      about: null,
      isActive: true,
      createdAt: "2021-01-28T08:15:29.276Z",
      updatedAt: "2021-01-28T08:15:29.270Z",
      caregivers: [
        {
          id: 2,
          uuid: "99481c44-f73b-4a33-9b91-5dba46edd90a",
          carecoachUuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
          fullName: "test name 2",
          firstName: "test 2",
          employerName: null,
          lastName: "name",
          nickName: null,
          email: "test@test.com",
          phone: "1234567890",
          pronouns: null,
          isActive: true,
          createdAt: "2021-01-29T08:15:29.270Z",
          updatedAt: "2021-01-29T08:15:29.270Z",
        },
        {
          id: 1,
          uuid: "25b1fbbb-9ea5-49d2-9840-344b6b45af2a",
          carecoachUuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
          fullName: "test name",
          firstName: "test",
          employerName: null,
          lastName: "name",
          nickName: null,
          email: "test@test.com",
          phone: "1234567890",
          pronouns: null,
          isActive: true,
          createdAt: "2021-01-29T08:15:29.270Z",
          updatedAt: "2021-01-29T08:15:29.270Z",
        },
      ],
      profileImage:
        "https://carecoach-profile-images-abhinav.s3.amazonaws.com/730b261c-d7ce-40bf-b978-3f21e3326ec9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAQ4WO7KOQOSVYKENI%2F20210205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210205T071903Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEDAaCXVzLWVhc3QtMSJIMEYCIQCSAqecgbkdabb6k%2BwMe7atbj%2BFIZjBPAhp%2BKRxnFsOnwIhAItFRYcOPCij14tO8hnrAiIgpeocW72HlMcMn9Gvtc6uKpUDCCgQABoMMDYxNjM3MzUwMzA0Igy7EwsZLeVIaZH9g8Yq8gJPRAQz4iFogaJ9yA2dz%2BFsH7ef%2BXhaqVqvghTi6Hah6QW0w5AZUNOvP%2BNbCT%2BKEAB71htBW9SRX5RQGKiA2AiGZkfdhv2ylpA9LU0JNRAdIbaa9Jeewo%2BHKeCJzhQY1uOvJrqHvdv5E5O9GX%2FVvw69KdA0ZL95KcjIjH8v0XqJOqPFIuGLXQH80HjZuKoAfESVzdpo9a1rmnDVmfamYK%2FYswYhCDFsbar%2Bu4pn%2BFwTkvegHw1ZA5mVE2LaGj8UidMsxSN7g4RCGNYUjdGJr33Xb%2BkOWO6l1DDi6SK5eNHPlrYwk2dbleWh4YccUSJ68jeYAjjXssSOxtCOy1WOyowzDiYiiL8tz%2B%2FiK%2BF8SjbZq57wWU7FFnfaj%2FAMxMbcsCYvw8ennUq3Whm1%2FH5jo%2FBmF%2B1cY73BFeYekv6So21anXnFfP1DFWhEueJwMNhG37FLDgjh1kPRLGgvD603fPSX0okECkpDLa8zPKCUXdcTc6ALMOHc84AGOqUBolCb4X0CJaq4AtGEvfnXzsSXM0coSO5%2F7%2F0bkWos57UvpnymwZcRVt0BuYaEFutCsDmQcruQ7apUW54Q9EmXV470vpAm%2B%2BQ96X0VOuD62W2LOPaOyRlYOgmlFyjnRQg287xv524%2BYTiXls4JflwFslh5CGWtBCY7ZAA%2F3XcVcEZ25oe%2FaDbpjyJqaGZSOy5TTAPS141%2B0dvCIMKTQ0rF1JD%2FIbpL&X-Amz-Signature=ecd49ea929eca64cf30ac3946255d717f480e06c589d162ace10509fdc493e0e&X-Amz-SignedHeaders=host",
    }
    const uuid = "daad0c7c-0ab2-4bea-8d96-2512cff733e0"
    jest.spyOn(axios, "default").mockResolvedValue({
      status: 202,
    })

    const actual = await CareCoachAPIService.getCareCoach(uuid)
    expect(actual).toEqual({
      status: 202,
    })
    expect(axios).toHaveBeenCalled()
  })
  it("should put data if status code equals 201", async () => {
    userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)

    const requestObj = {
      id: 6,
      uuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
      firstName: "John",
      lastName: "Doe",
      nickName: "Joe",
      email: "example@lantern.care",
      phone: "6021234567",
      pronouns: "He/him",
      location: "Gilbert, Arizona",
      jobTitle: null,
      jobStartDate: null,
      specialities: null,
      about: null,
      isActive: true,
      createdAt: "2021-01-28T08:15:29.276Z",
      updatedAt: "2021-01-28T08:15:29.270Z",
      caregivers: [
        {
          id: 2,
          uuid: "99481c44-f73b-4a33-9b91-5dba46edd90a",
          carecoachUuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
          fullName: "test name 2",
          firstName: "test 2",
          employerName: null,
          lastName: "name",
          nickName: null,
          email: "test@test.com",
          phone: "1234567890",
          pronouns: null,
          isActive: true,
          createdAt: "2021-01-29T08:15:29.270Z",
          updatedAt: "2021-01-29T08:15:29.270Z",
        },
        {
          id: 1,
          uuid: "25b1fbbb-9ea5-49d2-9840-344b6b45af2a",
          carecoachUuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
          fullName: "test name",
          firstName: "test",
          employerName: null,
          lastName: "name",
          nickName: null,
          email: "test@test.com",
          phone: "1234567890",
          pronouns: null,
          isActive: true,
          createdAt: "2021-01-29T08:15:29.270Z",
          updatedAt: "2021-01-29T08:15:29.270Z",
        },
      ],
      profileImage:
        "https://carecoach-profile-images-abhinav.s3.amazonaws.com/730b261c-d7ce-40bf-b978-3f21e3326ec9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAQ4WO7KOQOSVYKENI%2F20210205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210205T071903Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEDAaCXVzLWVhc3QtMSJIMEYCIQCSAqecgbkdabb6k%2BwMe7atbj%2BFIZjBPAhp%2BKRxnFsOnwIhAItFRYcOPCij14tO8hnrAiIgpeocW72HlMcMn9Gvtc6uKpUDCCgQABoMMDYxNjM3MzUwMzA0Igy7EwsZLeVIaZH9g8Yq8gJPRAQz4iFogaJ9yA2dz%2BFsH7ef%2BXhaqVqvghTi6Hah6QW0w5AZUNOvP%2BNbCT%2BKEAB71htBW9SRX5RQGKiA2AiGZkfdhv2ylpA9LU0JNRAdIbaa9Jeewo%2BHKeCJzhQY1uOvJrqHvdv5E5O9GX%2FVvw69KdA0ZL95KcjIjH8v0XqJOqPFIuGLXQH80HjZuKoAfESVzdpo9a1rmnDVmfamYK%2FYswYhCDFsbar%2Bu4pn%2BFwTkvegHw1ZA5mVE2LaGj8UidMsxSN7g4RCGNYUjdGJr33Xb%2BkOWO6l1DDi6SK5eNHPlrYwk2dbleWh4YccUSJ68jeYAjjXssSOxtCOy1WOyowzDiYiiL8tz%2B%2FiK%2BF8SjbZq57wWU7FFnfaj%2FAMxMbcsCYvw8ennUq3Whm1%2FH5jo%2FBmF%2B1cY73BFeYekv6So21anXnFfP1DFWhEueJwMNhG37FLDgjh1kPRLGgvD603fPSX0okECkpDLa8zPKCUXdcTc6ALMOHc84AGOqUBolCb4X0CJaq4AtGEvfnXzsSXM0coSO5%2F7%2F0bkWos57UvpnymwZcRVt0BuYaEFutCsDmQcruQ7apUW54Q9EmXV470vpAm%2B%2BQ96X0VOuD62W2LOPaOyRlYOgmlFyjnRQg287xv524%2BYTiXls4JflwFslh5CGWtBCY7ZAA%2F3XcVcEZ25oe%2FaDbpjyJqaGZSOy5TTAPS141%2B0dvCIMKTQ0rF1JD%2FIbpL&X-Amz-Signature=ecd49ea929eca64cf30ac3946255d717f480e06c589d162ace10509fdc493e0e&X-Amz-SignedHeaders=host",
    }
    const uuid = "daad0c7c-0ab2-4bea-8d96-2512cff733e0"
    jest.spyOn(axios, "default").mockResolvedValue({
      status: 202,
    })

    const actual = await CareCoachAPIService.getCareCoachList()
    expect(actual).toEqual({
      status: 202,
    })
    expect(axios).toHaveBeenCalled()
  })

  it("should assign carecoach if status code equals 201", async () => {
    userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)

    const requestObj = {
      id: 6,
      uuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
      firstName: "John",
      lastName: "Doe",
      nickName: "Joe",
      email: "example@lantern.care",
      phone: "6021234567",
      pronouns: "He/him",
      location: "Gilbert, Arizona",
      jobTitle: null,
      jobStartDate: null,
      specialities: null,
      about: null,
      isActive: true,
      createdAt: "2021-01-28T08:15:29.276Z",
      updatedAt: "2021-01-28T08:15:29.270Z",
      caregivers: [
        {
          id: 2,
          uuid: "99481c44-f73b-4a33-9b91-5dba46edd90a",
          carecoachUuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
          fullName: "test name 2",
          firstName: "test 2",
          employerName: null,
          lastName: "name",
          nickName: null,
          email: "test@test.com",
          phone: "1234567890",
          pronouns: null,
          isActive: true,
          createdAt: "2021-01-29T08:15:29.270Z",
          updatedAt: "2021-01-29T08:15:29.270Z",
        },
        {
          id: 1,
          uuid: "25b1fbbb-9ea5-49d2-9840-344b6b45af2a",
          carecoachUuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
          fullName: "test name",
          firstName: "test",
          employerName: null,
          lastName: "name",
          nickName: null,
          email: "test@test.com",
          phone: "1234567890",
          pronouns: null,
          isActive: true,
          createdAt: "2021-01-29T08:15:29.270Z",
          updatedAt: "2021-01-29T08:15:29.270Z",
        },
      ],
      profileImage:
        "https://carecoach-profile-images-abhinav.s3.amazonaws.com/730b261c-d7ce-40bf-b978-3f21e3326ec9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAQ4WO7KOQOSVYKENI%2F20210205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210205T071903Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEDAaCXVzLWVhc3QtMSJIMEYCIQCSAqecgbkdabb6k%2BwMe7atbj%2BFIZjBPAhp%2BKRxnFsOnwIhAItFRYcOPCij14tO8hnrAiIgpeocW72HlMcMn9Gvtc6uKpUDCCgQABoMMDYxNjM3MzUwMzA0Igy7EwsZLeVIaZH9g8Yq8gJPRAQz4iFogaJ9yA2dz%2BFsH7ef%2BXhaqVqvghTi6Hah6QW0w5AZUNOvP%2BNbCT%2BKEAB71htBW9SRX5RQGKiA2AiGZkfdhv2ylpA9LU0JNRAdIbaa9Jeewo%2BHKeCJzhQY1uOvJrqHvdv5E5O9GX%2FVvw69KdA0ZL95KcjIjH8v0XqJOqPFIuGLXQH80HjZuKoAfESVzdpo9a1rmnDVmfamYK%2FYswYhCDFsbar%2Bu4pn%2BFwTkvegHw1ZA5mVE2LaGj8UidMsxSN7g4RCGNYUjdGJr33Xb%2BkOWO6l1DDi6SK5eNHPlrYwk2dbleWh4YccUSJ68jeYAjjXssSOxtCOy1WOyowzDiYiiL8tz%2B%2FiK%2BF8SjbZq57wWU7FFnfaj%2FAMxMbcsCYvw8ennUq3Whm1%2FH5jo%2FBmF%2B1cY73BFeYekv6So21anXnFfP1DFWhEueJwMNhG37FLDgjh1kPRLGgvD603fPSX0okECkpDLa8zPKCUXdcTc6ALMOHc84AGOqUBolCb4X0CJaq4AtGEvfnXzsSXM0coSO5%2F7%2F0bkWos57UvpnymwZcRVt0BuYaEFutCsDmQcruQ7apUW54Q9EmXV470vpAm%2B%2BQ96X0VOuD62W2LOPaOyRlYOgmlFyjnRQg287xv524%2BYTiXls4JflwFslh5CGWtBCY7ZAA%2F3XcVcEZ25oe%2FaDbpjyJqaGZSOy5TTAPS141%2B0dvCIMKTQ0rF1JD%2FIbpL&X-Amz-Signature=ecd49ea929eca64cf30ac3946255d717f480e06c589d162ace10509fdc493e0e&X-Amz-SignedHeaders=host",
    }
    const uuid = "daad0c7c-0ab2-4bea-8d96-2512cff733e0"
    jest.spyOn(axios, "default").mockResolvedValue({
      status: 202,
    })

    const actual = await CareCoachAPIService.assignCareCoach()
    expect(actual).toEqual({
      status: 202,
    })
    expect(axios).toHaveBeenCalled()
  })
})
