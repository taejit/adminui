import axios from "axios"
import userManager from "../Auth/OidcAuth"

async function _getAuthHeader() {
  let user = await userManager.getUser()
  let token = user ? user.id_token : ""
  return {
    Authorization: `Bearer ${token}`,
  }
}

const careCoachApiService = {
  syncCareCoach: async (requestObj, uuid) => {
    try {
      return await axios({
        method: "put",
        url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/carecoaches/${uuid}/profile`,
        data: requestObj,
        headers: await _getAuthHeader(),
      })
    } catch (error) {
      return error
    }
  },
  getCareCoach: async (uuid) => {
    try {
      return await axios({
        method: "get",
        url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/carecoaches/${uuid}/`,
        headers: await _getAuthHeader(),
      })
    } catch (error) {
      return error
    }
  },
  getCareCoachList: async () => {
    try {
      return await axios({
        method: "get",
        url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/carecoaches/`,
        headers: await _getAuthHeader(),
      })
    } catch (error) {
      return error
    }
  },

  assignCareCoach: async (caregiverUuid, carecoachUuid) => {
    try {
      return await axios({
        method: "PATCH",
        url: `${process.env.REACT_APP_EMPLOYER_API_URL}/api/v1/caregivers/${caregiverUuid}/carecoaches/${carecoachUuid}`,
        headers: await _getAuthHeader(),
      })
    } catch (error) {
      return error
    }
  },
}

export default careCoachApiService
