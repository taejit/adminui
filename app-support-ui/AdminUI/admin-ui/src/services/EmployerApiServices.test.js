import axios from "axios"
import EmployerApiServices from "./EmployerApiServices"
import { settings } from "../__mock__/Employer.mock"
import userManager from "../Auth/OidcAuth"

jest.mock("axios")
jest.mock("../Auth/OidcAuth")

const MOCK_ID_TOKEN = "MOCK_ID_TOKEN"

const MOCK_USER = {
  id_token: MOCK_ID_TOKEN,
}
const MOCK_USER_PROMISE = Promise.resolve(MOCK_USER)

const EXPECTED_AUTH_HEADERS = {
  Authorization: "Bearer MOCK_ID_TOKEN",
}

describe("createEmployer", () => {
  beforeEach(() => {})
  afterEach(() => {
    jest.restoreAllMocks()
  })
  it("should return data if status code equals 201", async () => {
    userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
    const mRes = {
      status: 201,
      data: {
        employer_id: "123456",
        uuid: "daad0c7c-0ab2-4bea-8d96-2512cff733e0",
      },
    }

    const requestObj = {
      name: "employerName",
      contact: {
        name: "contactName",
        phone: "1234567890",
        email: "test@gmail.com",
      },
      identifierType: "Email",
    }

    jest.spyOn(axios, "default").mockResolvedValue({
      employer_id: "123456",
      uuid: "daad0c7c-0ab2-4bea-8d96-2512cff733e0",
    })

    const actual = await EmployerApiServices.createEmployer(requestObj)
    expect(actual).toEqual({
      employer_id: "123456",
      uuid: "daad0c7c-0ab2-4bea-8d96-2512cff733e0",
    })
  })

  it("should return data if status code equals 400", async () => {
    userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
    const mRes = {
      status: 400,
    }

    const requestObj = {
      name: "employerName",
      contact: {
        phone: "1234567890",
        email: "test@gmail.com",
      },
      identifierType: "Email",
    }

    axios.mockResolvedValueOnce(mRes)
    const actual = await EmployerApiServices.createEmployer(requestObj)
    expect(actual.status).toEqual(400)
  })

  describe("get Employer details based on uuid while loading Edit page", () => {
    afterEach(() => {
      jest.restoreAllMocks()
    })

    it("should return data if status code equals 200", async () => {
      userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
      const mRes = {
        contact: {
          name: "Anand",
          phone: "1234567891",
          email: "bramsai@intraedge.com",
        },
        employerId: "ng8blI",
        identifierType: "employerId",
        isActive: false,
        name: "asasas",
        uuid: "9c378a39-7db6-490d-9e5d-4b9e4ee2cbee",
      }
      const uuid = "9c378a39-7db6-490d-9e5d-4b9e4ee2cbee"

      jest.spyOn(axios, "default").mockResolvedValue(mRes)
      const actual = await EmployerApiServices.getEmployer(uuid)
      expect(actual).toEqual({
        contact: {
          name: "Anand",
          phone: "1234567891",
          email: "bramsai@intraedge.com",
        },
        employerId: "ng8blI",
        identifierType: "employerId",
        isActive: false,
        name: "asasas",
        uuid: "9c378a39-7db6-490d-9e5d-4b9e4ee2cbee",
      })
    })

    it("should return data if status code equals 400", async () => {
      userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
      const mRes = {
        status: 400,
      }

      const requestObj = {
        name: "employerName",
        contact: {
          phone: "1234567890",
          email: "test@gmail.com",
        },
        identifierType: "Email",
      }

      axios.mockResolvedValueOnce(mRes)
      const uuid = "9c378a39-7db6-490d-9e5d-4b9e4ee2cbee"
      const actual = await EmployerApiServices.getEmployer(requestObj)
      expect(actual.status).toEqual(400)
    })

    it("getEmployers should return data if status code is 200", async () => {
      userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
      const mRes = [
        {
          contact: {
            name: "Anand",
            phone: "1234567891",
            email: "bramsai@intraedge.com",
          },
          employerId: "ng8blI",
          identifierType: "employerId",
          isActive: false,
          name: "asasas",
          uuid: "9c378a39-7db6-490d-9e5d-4b9e4ee2cbee",
        },
      ]

      jest.spyOn(axios, "default").mockResolvedValue(mRes)
      const actual = await EmployerApiServices.getEmployers()
      expect(actual).toEqual([
        {
          contact: {
            name: "Anand",
            phone: "1234567891",
            email: "bramsai@intraedge.com",
          },
          employerId: "ng8blI",
          identifierType: "employerId",
          isActive: false,
          name: "asasas",
          uuid: "9c378a39-7db6-490d-9e5d-4b9e4ee2cbee",
        },
      ])
    })

    it("getUploadStatusFromApi test", async () => {
      userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
      const mRes = {
        uploadStatus: "STARTED",
        status: 200,
      }
      const uuid = "abcde12-ghsdty"
      jest.spyOn(axios, "default").mockResolvedValue(mRes)
      const actual = await EmployerApiServices.getUploadStatusFromApi(uuid)
      expect(actual).toEqual(mRes)
    })

    it("getPresignedUrl test", async () => {
      userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
      const mRes = {
        objectKey: "asbc-23bshsd-23j",
        presignedUrl: "someurl",
      }
      jest.spyOn(axios, "default").mockResolvedValue(mRes)
      const actual = await EmployerApiServices.getPresignedUrl(
        {},
        "abc12-ghs34"
      )
      expect(actual).toEqual(mRes)
    })
    it("uploadFileThroughPresignedUrl test", async () => {
      userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
      const mRes = {
        objectKey: "asbc-23bshsd-23j",
        presignedUrl: "someurl",
      }
      jest.spyOn(axios, "default").mockResolvedValue(mRes)
      const actual = await EmployerApiServices.uploadFileThroughPresignedUrl(
        "http://url",
        {}
      )
      expect(actual).toEqual(mRes)
    })
  })

  describe("submit form in Edit page", () => {
    afterEach(() => {
      jest.restoreAllMocks()
    })

    it("should return data if status code equals 200", async () => {
      userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
      const mRes = {
        status: 200,
        data: {
          employer_id: "123456",
          uuid: "daad0c7c-0ab2-4bea-8d96-2512cff733e0",
        },
      }

      const requestObj = {
        name: "employerName",
        contact: {
          name: "contactName",
          phone: "1234567890",
          email: "test@gmail.com",
        },
        identifierType: "Email",
      }

      jest.spyOn(axios, "default").mockResolvedValue({
        employer_id: "123456",
        uuid: "daad0c7c-0ab2-4bea-8d96-2512cff733e0",
      })
      const uuid = "daad0c7c-0ab2-4bea-8d96-2512cff733e0"
      const actual = await EmployerApiServices.updateEmployer(requestObj, uuid)
      expect(actual).toEqual({
        employer_id: "123456",
        uuid: "daad0c7c-0ab2-4bea-8d96-2512cff733e0",
      })
    })

    it("should return data if status code equals 400", async () => {
      userManager.getUser.mockReturnValue(MOCK_USER_PROMISE)
      const mRes = {
        status: 400,
      }

      const requestObj = {
        name: "employerName",
        contact: {
          phone: "1234567890",
          email: "test@gmail.com",
        },
        identifierType: "Email",
      }

      axios.mockResolvedValueOnce(mRes)
      const actual = await EmployerApiServices.updateEmployer(requestObj)
      expect(actual.status).toEqual(400)
    })
  })
})
