import React, { Suspense } from "react"
import { shallow } from "enzyme/build"
import App from "./App"
jest.mock("./components/errors/Page404", () =>
  require("./components/errors/Page404")
)
jest.mock("./components/errors/Page500", () =>
  require("./components/errors/Page500")
)
describe("App component test", () => {
  it("should show the component", async () => {
    const component = await shallow(
      <Suspense fallback={<div>loading</div>}>
        <App />
      </Suspense>
    )
    expect(component.find(App).length).toEqual(1)
  })
})
