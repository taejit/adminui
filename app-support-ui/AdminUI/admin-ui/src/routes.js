import React from "react"

const Employers = React.lazy(() => import("./components/employers/Employers"))
const EditEmployer = React.lazy(() =>
  import("./components/employers/EditEmployer")
)
const AddEmployer = React.lazy(() =>
  import("./components/employers/AddEmployer")
)
const CareCoaches = React.lazy(() =>
  import("./components/carecoaches/CareCoaches")
)
const ForgotPassword = React.lazy(() =>
  import("./components/forgotpassword/ForgotPassword")
)
const EmployerData = React.lazy(() =>
  import("./components/employers/EmployerData")
)

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/employers", name: "Employers", component: Employers, exact: true },
  {
    path: "/forgot-password",
    name: "Reset Password",
    component: ForgotPassword,
    exact: true,
  },
  {
    path: "/employers/add",
    name: "Add New Employer",
    component: AddEmployer,
    exact: true,
  },
  {
    path: "/employers/edit/:id",
    name: "Edit Employer",
    component: EditEmployer,
  },
  {
    path: "/employers/view/:id",
    name: "Employer",
    component: EmployerData,
    dynamicParam: "name",
  },
  {
    path: "/carecoaches",
    name: "Care Coaches",
    component: CareCoaches,
    exact: true,
  },
  {
    path: "/carecoaches/add",
    name: "Add New Care Coach",
    component: React.lazy(() =>
      import("./components/carecoaches/AddEditCareCoach")
    ),
  },
  {
    path: "/carecoaches/edit/:id",
    name: "Edit Care Coach",
    component: React.lazy(() =>
      import("./components/carecoaches/AddEditCareCoach")
    ),
  },
  {
    path: "/carecoaches/view/:id",
    name: "View Care Coach Details",
    component: React.lazy(() =>
      import("./components/carecoaches/ViewCareCoach")
    ),
  },
  {
    path: "/profile",
    exact: true,
    name: "Profile",
    component: React.lazy(() => import("./components/profile")),
  },
]

export default routes
