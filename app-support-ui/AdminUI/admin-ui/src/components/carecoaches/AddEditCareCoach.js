import React, { useEffect, useState } from "react"
import _ from "underscore"
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CRow,
  CAlert,
  CSwitch,
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
  CModalFooter,
} from "@coreui/react"
import { useFormik } from "formik"
import * as Yup from "yup"
import "../employers/employer.css"
import { v4 as uuidv4 } from "uuid"
import AWS from "aws-sdk"
import instance from "../../Auth/GetAWSCredentials"
import FormatPhoneNumber from "src/utils/FormatPhoneNumber"
import careCoachApiService from "src/services/CareCoachAPIService"

const AddEditCareCoach = ({ history, match }) => {
  const { id } = match.params
  const isAddMode = !id
  const [error, setError] = useState("")
  const [phone, setPhone] = useState("")
  const [showModal, setModal] = useState(false)
  const [loader, setLoader] = useState(false)

  const [selectedCareCoach, setCareCoach] = useState({})
  const [inputField, setInputField] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone_number: "",
    calendlyUsername: "",
  })

  const congnitoServiceInstance = async () => {
    AWS.config.region = `us-east-1`
    AWS.config.credentials = await instance.getAWSCredentials()
    return new AWS.CognitoIdentityServiceProvider()
  }

  useEffect(() => {
    async function getUserData() {
      setLoader(true)
      const getUserParams = {
        UserPoolId: `${window._env_.pool_id}`,
        Username: id,
      }
      const cognitoIdentityServiceProvider = await congnitoServiceInstance()
      const userData = await cognitoIdentityServiceProvider
        .adminGetUser(getUserParams)
        .promise()
      setLoader(false)
      if (!userData) {
        setError("Unable to get carecoach details")
        return
      }
      const transformedUserData = {}
      userData.UserAttributes.map(
        (userAttribute) =>
          (transformedUserData[userAttribute.Name] = userAttribute.Value)
      )
      transformedUserData.isActive = userData.Enabled
      setCareCoach(transformedUserData)
      let newState = {
        firstName: transformedUserData.given_name,
        lastName: transformedUserData.family_name,
        email: transformedUserData.email,
        phone_number: transformedUserData.phone_number.replace(/^\+[0-9]/, ""),
        status: userData.Enabled,
        uuid: transformedUserData["custom:uuid"],
        calendlyUsername: transformedUserData["custom:calendly_username"],
        cognitoId: transformedUserData.sub,
      }
      setInputField(newState)
      let phoneNumber = newState.phone_number
      if (phoneNumber) {
        setPhone(FormatPhoneNumber(phoneNumber))
      }
    }
    !isAddMode && getUserData()
  }, [id, isAddMode])

  const normalizeInput = (value) => {
    let inputfield = inputField
    inputfield.phone = value
    setInputField(inputfield)
    formik.setFieldValue("phone", inputfield.phone, true)
    let result = FormatPhoneNumber(value)
    setPhone(result)
  }

  // to mask the phone number as (555)555-5555
  const phoneRegExp = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/

  const formik = useFormik({
    initialValues: {
      firstName: inputField.firstName || "",
      lastName: inputField.lastName || "",
      email: inputField.email || "",
      phone: inputField.phone_number || "",
      calendlyUsername: inputField.calendlyUsername || "",
      isActive: inputField.status || "",
      cognitoId: inputField.cognitoId || "",
    },
    enableReinitialize: true,
    validationSchema: Yup.object({
      firstName: Yup.string().required("Required"),
      lastName: Yup.string().required("Required"),
      email: Yup.string().email("Email is invalid").required("Required"),
      phone: Yup.string()
        .matches(phoneRegExp, "Phone number is not valid")
        .required("Required"),
      calendlyUsername: Yup.string()
        .required("Required")
        .max(50, "Should not exceed 50 characters"),
    }),
    onSubmit: async (values) => {
      const data = {
        firstname: values.firstName,
        lastname: values.lastName,
        email: values.email,
        phone_number: values.phone.replace(/[^\d]/g, ""),
        calendlyUsername: values.calendlyUsername,
        status: !isAddMode ? values.isActive : "",
        // for Add carecaoch there will not be sub
        cognitoId: values.cognitoId || "",
      }
      return isAddMode ? addCareCoach(data) : updateCareCoach(data)
    },
  })

  //To save data into Cognito with adminCreateUser()
  const addCareCoach = async (requestData) => {
    const userName = requestData.email
    const uuid = uuidv4()
    var userCreationParams = {
      UserPoolId: `${window._env_.pool_id}`,
      Username: userName,
      DesiredDeliveryMediums: ["EMAIL"],
      ForceAliasCreation: false,
      UserAttributes: [
        {
          Name: "name",
          Value: requestData.firstname + " " + requestData.lastname,
        },
        {
          Name: "given_name",
          Value: requestData.firstname,
        },
        {
          Name: "family_name",
          Value: requestData.lastname,
        },
        {
          Name: "nickname",
          Value: requestData.firstname + " " + requestData.lastname,
        },
        {
          Name: "email",
          Value: requestData.email,
        },
        {
          Name: "phone_number",
          Value: `+1${requestData.phone_number.replace(/[- )(]/g, "")}`,
        },
        {
          Name: "email_verified",
          Value: "true",
        },
        {
          Name: "phone_number_verified",
          Value: "true",
        },
        {
          Name: "custom:uuid",
          Value: uuid,
        },
        {
          Name: "custom:location",
          Value: "",
        },
        {
          Name: "custom:pronoun",
          Value: "",
        },
        {
          Name: "custom:pronoun_other",
          Value: "",
        },
        {
          Name: "custom:job_start_date",
          Value: "",
        },
        {
          Name: "custom:bio",
          Value: "",
        },
        {
          Name: "custom:credentials",
          Value: "",
        },
        {
          Name: "custom:calendly_username",
          Value: requestData.calendlyUsername,
        },
      ],
    }
    const cognitoIdentityServiceProvider = await congnitoServiceInstance()
    let adminCreateUserData
    try {
      adminCreateUserData = await cognitoIdentityServiceProvider
        .adminCreateUser(userCreationParams)
        .promise()
    } catch (error) {
      if (error.name === "UsernameExistsException") {
        setError(error.message)
      } else {
        console.error("Unable to add CareCoach.", error)
        setError("Unable to add CareCoach. Please try again")
      }
      return
    }
    var params = {
      GroupName: "carecoach",
      UserPoolId: `${window._env_.pool_id}`,
      Username: adminCreateUserData.User.Username,
    }
    const addUsertoGroupResponse = await cognitoIdentityServiceProvider
      .adminAddUserToGroup(params)
      .promise()
    if (!addUsertoGroupResponse) {
      setError("Unable to add carecoach to group")
      return
    }
    const careCoachProfile = {
      firstName: requestData.firstname,
      lastName: requestData.lastname,
      nickName: `${requestData.firstname} ${requestData.lastname}`,
      email: requestData.email,
      phone: requestData.phone_number,
      location: selectedCareCoach["custom:location"] || "",
      pronouns:
        selectedCareCoach["custom:pronoun"] === "other"
          ? selectedCareCoach["custom:pronoun_other"] || ""
          : selectedCareCoach["custom:pronoun"] || "",
      isActive: requestData.status || false,
      jobStartDate: selectedCareCoach["custom:job_start_date"] || "",
      specialities: selectedCareCoach["custom:credentials"] || "",
      about: selectedCareCoach["custom:bio"] || "",
      calendlyUsername: requestData.calendlyUsername,
      cognitoId: getCognitoId(adminCreateUserData),
    }
    await syncCareCoachData(careCoachProfile, uuid)
    history.push({
      pathname: "/carecoaches",
      state: "Changes Saved",
    })
  }

  const getCognitoId = (user) => {
    if (user) {
      const found = _.findWhere(user.User.Attributes, { Name: "sub" })
      if (found) {
        return found.Value
      }
    }
    return ""
  }

  //To update the data in Cognito with
  const updateCareCoach = async (updateData) => {
    // will use adminUpdateUserAttributes() to update the data into cognito
    //params object contains fields to be updated and also required while updating
    var updateParams = {
      UserAttributes: [
        {
          Name: "name",
          Value: updateData.firstname + " " + updateData.lastname,
        },
        {
          Name: "given_name",
          Value: updateData.firstname,
        },
        {
          Name: "family_name",
          Value: updateData.lastname,
        },
        {
          Name: "nickname",
          Value: updateData.firstname + " " + updateData.lastname,
        },
        {
          Name: "email",
          Value: updateData.email,
        },
        {
          Name: "phone_number",
          Value: `+1${updateData.phone_number.replace(/[- )(]/g, "")}`,
        },
        {
          Name: "custom:location",
          Value: selectedCareCoach["custom:location"] || "",
        },
        {
          Name: "custom:pronoun",
          Value: selectedCareCoach["custom:pronoun"] || "",
        },
        {
          Name: "custom:pronoun_other",
          Value: selectedCareCoach["custom:pronoun_other"] || "",
        },
        {
          Name: "custom:job_start_date",
          Value: selectedCareCoach["custom:job_start_date"] || "",
        },
        {
          Name: "custom:bio",
          Value: selectedCareCoach["custom:bio"] || "",
        },
        {
          Name: "custom:credentials",
          Value: selectedCareCoach["custom:credentials"] || "",
        },
        {
          Name: "custom:calendly_username",
          Value: updateData.calendlyUsername,
        },
      ],
      UserPoolId: `${window._env_.pool_id}`,
      Username: id,
    }
    const cognitoIdentityServiceProvider = await congnitoServiceInstance()
    try {
      await cognitoIdentityServiceProvider
        .adminUpdateUserAttributes(updateParams)
        .promise()
    } catch (error) {
      if (error.name === "AliasExistsException") {
        setError(error.message)
      } else {
        console.error("Unable to edit CareCoach.", error)
        setError("Unable to update Carecoach")
      }
      return
    }

    let enableDisableParams = {
      UserPoolId: `${window._env_.pool_id}`,
      Username: id,
    }

    let enableDisableResponse
    if (updateData.status) {
      enableDisableResponse = await cognitoIdentityServiceProvider
        .adminEnableUser(enableDisableParams)
        .promise()
    } else {
      enableDisableResponse = await cognitoIdentityServiceProvider
        .adminDisableUser(enableDisableParams)
        .promise()
    }

    if (!enableDisableResponse) {
      setError("Unable to update the user status")
      return
    }

    const careCoachProfile = {
      firstName: updateData.firstname,
      lastName: updateData.lastname,
      nickName: `${updateData.firstname} ${updateData.lastname}`,
      email: updateData.email,
      phone: updateData.phone_number,
      location: selectedCareCoach["custom:location"] || "",
      pronouns:
        selectedCareCoach["custom:pronoun"] === "other"
          ? selectedCareCoach["custom:pronoun_other"] || ""
          : selectedCareCoach["custom:pronoun"] || "",
      isActive: updateData.status || false,
      jobStartDate: selectedCareCoach["custom:job_start_date"] || "",
      specialities: selectedCareCoach["custom:credentials"] || "",
      about: selectedCareCoach["custom:bio"] || "",
      calendlyUsername: updateData.calendlyUsername,
      cognitoId: updateData.cognitoId,
    }
    await syncCareCoachData(careCoachProfile, selectedCareCoach["custom:uuid"])
    history.push({
      pathname: "/carecoaches",
      state: "Changes Saved",
    })
  }

  const syncCareCoachData = async (careCoach, careCoachId) => {
    try {
      await careCoachApiService.syncCareCoach(careCoach, careCoachId)
    } catch (err) {
      setError("Unable to put message in the queue successfully")
    }
  }

  const changeStatus = (value) => {
    !value && setModal(true)
    formik.setFieldValue("isActive", value)
  }

  return (
    <>
      {loader ? (
        <div
          className="spinner-border text-primary spinner-alignment"
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      ) : (
        <>
          <CModal
            id="ccModal"
            show={showModal}
            onClose={() => {
              setModal(!showModal)
              formik.setFieldValue("isActive", true)
            }}
            color="danger"
          >
            <CModalHeader closeButton>
              <CModalTitle>Deactivate Care Coach</CModalTitle>
            </CModalHeader>
            <CModalBody>
              <div className="sm-12">
                <p>
                  Are you sure you want to deactivate this Care Coach? They will
                  no longer have access to their website portal and all the
                  Caregivers that are assigned to them will be reassigned to
                  someone else.
                </p>
              </div>
            </CModalBody>
            <CModalFooter>
              <CButton
                id="ccCancel"
                color="secondary"
                onClick={() => {
                  setModal(false)
                  formik.setFieldValue("isActive", true)
                }}
              >
                Cancel
              </CButton>{" "}
              <CButton
                id="ccContinue"
                color="danger"
                onClick={() => {
                  formik.setFieldValue("isActive", false)
                  setModal(!showModal)
                }}
              >
                Continue
              </CButton>
            </CModalFooter>
          </CModal>
          <CCard>
            <CCardHeader className="emp-header">
              <h4 id="care-coach-header" className="card-title mb-0">
                {isAddMode ? "Add New Care Coach" : "Edit Care Coach"}
              </h4>
            </CCardHeader>
            <CCardBody>
              {error && (
                <CRow>
                  <CCol>
                    <div className="mt-2">
                      <CAlert id="ccError" color="danger" closeButton>
                        {error}
                      </CAlert>
                    </div>
                  </CCol>
                </CRow>
              )}
              <CForm id="AddEditForm" onSubmit={formik.handleSubmit}>
                <CRow>
                  <CCol sm="6">
                    <CFormGroup>
                      <CLabel
                        htmlFor="firstname"
                        className="label-font required"
                      >
                        First Name
                      </CLabel>
                      <CInput
                        type="text"
                        name="firstName"
                        id="ccFirstName"
                        {...formik.getFieldProps("firstName")}
                      />
                      {formik.touched.firstName && formik.errors.firstName && (
                        <div className="text-danger">
                          {formik.errors.firstName}
                        </div>
                      )}
                    </CFormGroup>
                  </CCol>
                  <CCol sm="6">
                    <CFormGroup>
                      <CLabel className="label-font required">Last Name</CLabel>
                      <CInput
                        type="text"
                        name="lastName"
                        id="ccLastName"
                        {...formik.getFieldProps("lastName")}
                      />
                      {formik.touched.lastName && formik.errors.lastName && (
                        <div className="text-danger">
                          {formik.errors.lastName}
                        </div>
                      )}
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol sm="6">
                    <CFormGroup>
                      <CLabel className="label-font required">
                        Email Address
                      </CLabel>
                      <CInput
                        type="text"
                        name="email"
                        id="ccEmail"
                        {...formik.getFieldProps("email")}
                      />
                      {formik.touched.email && formik.errors.email && (
                        <div className="text-danger">{formik.errors.email}</div>
                      )}
                    </CFormGroup>
                  </CCol>
                  <CCol sm="6">
                    <CFormGroup>
                      <CLabel className="label-font required">
                        Phone Number
                      </CLabel>
                      <CInput
                        type="text"
                        name="phone"
                        id="ccPhone"
                        value={phone ? phone : ""}
                        onChange={(e) => normalizeInput(e.target.value)}
                      />
                      {formik.touched.phone && formik.errors.phone && (
                        <div className="text-danger">{formik.errors.phone}</div>
                      )}
                    </CFormGroup>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol sm="6">
                    <CFormGroup>
                      <CLabel className="label-font required">
                        Calendly Username
                      </CLabel>
                      <CInput
                        type="text"
                        name="calendlyUsername"
                        id="cccalendlyUsername"
                        {...formik.getFieldProps("calendlyUsername")}
                      />
                      {formik.touched.calendlyUsername &&
                        formik.errors.calendlyUsername && (
                          <div className="text-danger">
                            {formik.errors.calendlyUsername}
                          </div>
                        )}
                    </CFormGroup>
                  </CCol>
                </CRow>
                {!isAddMode ? (
                  <>
                    <CRow>
                      <CCol sm="6">
                        <CFormGroup>
                          <CLabel className="label-font">
                            <b>Status</b>
                          </CLabel>
                        </CFormGroup>
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol sm="6" md="2" className="mb-4">
                        <CSwitch
                          className={"mx-1"}
                          shape={"pill"}
                          color={"primary"}
                          name="isActive"
                          id="ccIsActive"
                          checked={formik.values.isActive}
                          onChange={(e) => changeStatus(e.target.checked)}
                        />
                        <span className="text-secondary ml-4 status-text">
                          {formik.values.isActive ? "Active" : "Inactive"}
                        </span>
                        {formik.touched.isActive && formik.errors.isActive && (
                          <div className="text-danger">
                            {formik.errors.isActive}
                          </div>
                        )}
                      </CCol>
                    </CRow>
                  </>
                ) : (
                  ""
                )}
                <CRow>
                  <CCol sm="12" md="2">
                    <CButton
                      type="submit"
                      id="saveBtn"
                      className="save-edit-button"
                      block
                      shape="pill"
                      color="primary"
                      disabled={!formik.dirty}
                    >
                      Save
                    </CButton>
                  </CCol>
                </CRow>
              </CForm>
            </CCardBody>
          </CCard>
        </>
      )}
    </>
  )
}

export default AddEditCareCoach
