import CareCoaches from "./CareCoaches"
import React from "react"
import { shallow } from "enzyme"
import { createMemoryHistory } from "history"
import { Router } from "react-router-dom"
import AWS from "aws-sdk"
import { fireEvent, render, waitFor } from "@testing-library/react"

jest.mock("../../Auth/GetAWSCredentials", () => {
  return {
    getAWSCredentials: jest.fn().mockResolvedValue({}),
  }
})
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./employers",
    state: "Changes Saved",
  }),
}))
describe("CareCoaches Component", () => {
  beforeEach(() => {
    jest.spyOn(AWS, "CognitoIdentityServiceProvider").mockImplementation(() => {
      return {
        listUsersInGroup: () => {
          return {
            promise: () => {
              return Promise.resolve({
                Users: [
                  {
                    Enabled: true,
                    Attributes: [
                      {
                        Name: "email",
                        Value: "testName@email.com",
                      },
                      {
                        Name: "name",
                        Value: "test name",
                      },
                      {
                        Name: "phone_number",
                        Value: "+17234992839",
                      },
                    ],
                  },
                  {
                    Enabled: false,
                    Attributes: [
                      {
                        Name: "email",
                        Value: "testName@email.com",
                      },
                      {
                        Name: "name",
                        Value: "test name",
                      },
                      {
                        Name: "phone_number",
                        Value: "+17234992839",
                      },
                    ],
                  },
                ],
              })
            },
          }
        },
      }
    })
  })

  afterEach(() => {
    jest.resetAllMocks()
  })

  it("mounts without crashing", () => {
    const wrapper = shallow(<CareCoaches />)
    wrapper.unmount()
  })

  it("Add care coach button test", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <Router history={history}>
          <CareCoaches />
        </Router>
      )
    })
    const { container } = wrapper
    const addBtn = container.querySelector(".add-carecoach")
    expect(addBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(addBtn)
    })
    expect(history.push).toHaveBeenCalled()
  })

  it("Edit care coach button test", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <Router history={history}>
          <CareCoaches />
        </Router>
      )
    })
    const { container } = wrapper
    const editBtn = container.querySelector(".edit-carecoach")
    expect(editBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(editBtn)
    })
    expect(history.push).toHaveBeenCalled()
  })
})
