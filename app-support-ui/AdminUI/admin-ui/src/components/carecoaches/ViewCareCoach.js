import React, { useEffect, useState } from "react"
import careCoachApiService from "src/services/CareCoachAPIService"
import Icon from "@coreui/icons-react"
import "./viewcarecoach.css"

import {
  CCard,
  CCardBody,
  CCol,
  CButton,
  CSelect,
  CRow,
  CDataTable,
  CBadge,
  CTooltip,
  CContainer,
  CModal,
  CModalHeader,
  CModalFooter,
  CModalBody,
  CFormGroup,
  CLabel,
  CCardText,
  CMedia,
  CMediaBody,
} from "@coreui/react"
import { useHistory } from "react-router-dom"
import FormatPhoneNumber from "src/utils/FormatPhoneNumber"

import { toast } from "react-toastify"
import "./carecoach.css"
import differenceInYears from "date-fns/differenceInYears"
import format from "date-fns/format"

const ViewCareCoach = (props, id) => {
  const [inputField, setInputField] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone_number: "",
    calendlyUsername: "",
    isActive: "",
    jobStartDate: "",
  })

  const [selectedCaregiver, setSelectedCaregiver] = useState([])
  const [careGivers, setCareGivers] = useState([])
  const [careCoaches, setCareCoaches] = useState([])
  const [showModal, setModal] = useState(false)
  const [showCarecoachAssignModal, setCarecoachAssignModal] = useState(false)
  const [carecoachUuid, setCarecoachUuid] = useState("")
  const cognitoUuid = window.sessionStorage.getItem("CCCognitoUuid")
  const getCarecoachData = async (uuid) => {
    try {
      const response = await careCoachApiService.getCareCoach(uuid)
      setInputField(response.data)
      setCareGivers(response.data.caregivers)
    } catch (err) {
      toast.error("Unable to fetch the employers. Please try again", {
        autoClose: 3000,
      })
      return
    }
  }
  const getCoachList = async () => {
    const carecoachResponse = await careCoachApiService.getCareCoachList()
    setCareCoaches(carecoachResponse.data)
  }

  useEffect(() => {
    getCoachList()
  }, [])
  useEffect(() => {
    getCarecoachData(props.match.params.id)
  }, [props.match.params.id])

  const history = useHistory()
  const editClick = () => history.push(`/carecoaches/edit/${cognitoUuid}`)
  const assignCarecoach = async () => {
    const resp = await careCoachApiService.assignCareCoach(
      selectedCaregiver.uuid,
      carecoachUuid
    )
    if (resp.status === 200) {
      setCarecoachAssignModal(false)
      getCarecoachData(props.match.params.id)
    }
  }
  const previewModal = () => {
    setModal(!showModal)
  }
  const fields = [
    { key: "firstName", label: "FirstName", _style: { fontSize: "12px" } },
    { key: "lastName", label: "LastName", _style: { fontSize: "12px" } },
    { key: "email", label: "Email", _style: { fontSize: "12px" } },
    { key: "employerName", label: "Employer", _style: { fontSize: "12px" } },
    { key: "edit", label: "Actions", _style: { fontSize: "12px" } },
  ]

  const openCaregiver = (index) => {
    setSelectedCaregiver(careGivers[index])
    setCarecoachAssignModal(true)
    // set new carecoach uuid for first entry upon opening the modal
    // because on change of drop down will not happen for this first record
    setCarecoachUuid(careCoaches[0].uuid)
  }

  const closeCaregiverModel = () => {
    setSelectedCaregiver(null)
    setCarecoachAssignModal(false)
  }

  function experienceInYears(startDate) {
    let currentDate = format(new Date(), "yyyy, MM, dd")
    if (startDate) {
      let expInYears = differenceInYears(
        new Date(currentDate),
        new Date(startDate)
      )

      // January 4th 2019
      let formattedDate = format(new Date(startDate), "MMMM do, yyyy")
      return formattedDate + " / " + expInYears + " years of experience"
    } else {
      return "N/A"
    }
  }

  return (
    <>
      <CCard data-testid="list_editor">
        <CContainer fluid>
          <CRow>
            <CCol sm="8">
              <CCard>
                <CCardBody id="ccCard">
                  <h5> {inputField.firstName + " " + inputField.lastName}</h5>
                  <h6>Email: {inputField.email} </h6>
                  <h6 id="ccPhone" className="phoneNumber">
                    Phone: {FormatPhoneNumber(inputField.phone)}
                  </h6>
                  <h6 id="cccalendlyuname">
                    Calendly Username: {inputField.calendlyUsername}
                  </h6>
                  <h6>
                    Status:&nbsp;
                    <CBadge shape="pill" color="primary">
                      {inputField.isActive ? "Active" : "Inactive"}
                    </CBadge>
                  </h6>
                </CCardBody>
              </CCard>
            </CCol>
            <CCol sm="2">
              <CCard>
                <CButton
                  id="btn-edit"
                  placeholder="btn-edit"
                  data-testid="btn-edit"
                  className="m-2  edit-carecoach button float-right"
                  shape="pill"
                  color="primary"
                  onClick={editClick}
                >
                  Edit
                </CButton>
              </CCard>
            </CCol>
            <CCol sm="2">
              <CCard>
                <CButton
                  id="button"
                  className="m-2 add-carecoach button float-right"
                  color="primary"
                  onClick={previewModal}
                >
                  Preview Profile
                </CButton>

                {!inputField.length > 0 ? (
                  <CModal
                    className=".my-modal-window"
                    show={showModal}
                    onClose={previewModal}
                  >
                    <CModalHeader closeButton>
                      <h5 className="ml-3">Care Coach Profile</h5>
                    </CModalHeader>
                    <CModalBody>
                      <CFormGroup className="modal-scrollbar">
                        <CLabel className="mb-0">
                          <h6 className="textclass">Name</h6>
                        </CLabel>
                        <CCardText className="mt-0" id="ccName">
                          {inputField.firstName + " " + inputField.lastName}
                        </CCardText>
                        <CLabel className="mb-0">
                          <h6 className="textclass">Pronouns</h6>
                        </CLabel>
                        <CCardText id="ccPronouns">
                          {inputField.pronouns}{" "}
                        </CCardText>
                        <CLabel className="mb-0">
                          <h6 className="textclass">Location</h6>
                        </CLabel>
                        <CCardText id="ccLocation">
                          {inputField.location}{" "}
                        </CCardText>
                        <CLabel className="mb-0">
                          <h6 className="textclass">
                            Start Date / Years of Experience
                          </h6>
                        </CLabel>
                        <CCardText id="ccJobStartDate">
                          {experienceInYears(inputField.jobStartDate)}{" "}
                        </CCardText>
                        <CLabel className="mb-0">
                          <h6 className="textclass">Credentials</h6>
                        </CLabel>
                        <CCardText id="ccSpecialities">
                          {inputField.specialities}{" "}
                        </CCardText>
                        <CLabel className="mb-0">
                          <h6 className="textclass">About Statement</h6>
                        </CLabel>
                        <CCardText id="ccAbout" className="overflow-auto">
                          {inputField.about}
                        </CCardText>
                        <CLabel className="mb-0">
                          <h6 className="textclass">Photo</h6>
                        </CLabel>
                        <CMedia>
                          <img
                            width="110"
                            height="107"
                            src={inputField.profileImage}
                            alt="profile-img"
                          ></img>
                          {/* <img src="" height="50" /> */}
                          <CMediaBody></CMediaBody>
                        </CMedia>
                      </CFormGroup>
                    </CModalBody>
                    <CModalFooter>
                      <CButton color="primary" onClick={previewModal}>
                        Close
                      </CButton>
                    </CModalFooter>
                  </CModal>
                ) : (
                  "No Data"
                )}
              </CCard>
            </CCol>
          </CRow>
        </CContainer>
        <CCardBody>
          <CRow>
            <CCol>
              <h4 id="traffic" className="title">
                Assigned Caregivers
              </h4>
            </CCol>
          </CRow>
          <br />

          <CDataTable
            className="dataTables_wrapper"
            noItemsViewSlot={
              <div className="no-caregivers-data">
                There are no Caregivers assigned to this Care Coach.
              </div>
            }
            items={careGivers}
            fields={fields}
            hover
            sorter
            dynamic={true}
            scopedSlots={{
              phoneNumber: (item) => (
                <td className="phoneNumber">
                  {FormatPhoneNumber(item.phoneNumber)}
                </td>
              ),
              edit: (item, index) => {
                return (
                  <td className="py-2">
                    <CTooltip content="Reassign Caregiver to a different Care Coarch">
                      <Icon
                        data-testid="buttonmodal"
                        data-toggle="tooltip"
                        data-placement="Top"
                        title="Reassign Caregiver to a different Care Coarch"
                        name="cil-reload"
                        className="svg pop-up mr-3"
                        size="xl"
                        onClick={() => {
                          openCaregiver(index)
                        }}
                      />
                    </CTooltip>
                  </td>
                )
              },
              details: (item, index) => {
                return (
                  <CModal
                    className="modal"
                    show={showCarecoachAssignModal}
                    onClose={closeCaregiverModel}
                  >
                    <CModalHeader className="mr-2" closeButton>
                      <h5>Reassign Caregiver</h5>
                    </CModalHeader>
                    <CModalBody>
                      <CFormGroup>
                        <CLabel className="mt-2">
                          <h6 className="textclass">Caregiver:</h6>
                        </CLabel>
                        <CLabel id="ccFirstName">
                          {selectedCaregiver && selectedCaregiver.fullName}
                        </CLabel>
                        <br />
                        <CLabel className="mb-3">
                          <h6 className="textclass">Current Care Coach:</h6>
                        </CLabel>
                        <CLabel id="ccFirstName">
                          {" "}
                          {inputField.firstName + " " + inputField.lastName}
                        </CLabel>
                      </CFormGroup>
                      <CFormGroup>
                        <CLabel>
                          <h6 className="textclass">Reassign Caregiver to:</h6>
                        </CLabel>
                        <CSelect
                          custom
                          onChange={(e) => setCarecoachUuid(e.target.value)}
                        >
                          {careCoaches.map((list) => (
                            <option value={list.uuid}>
                              {list.firstName + " " + list.lastName}
                            </option>
                          ))}
                        </CSelect>
                      </CFormGroup>
                    </CModalBody>
                    <CModalFooter>
                      <CButton
                        color="primary"
                        onClick={() => closeCaregiverModel()}
                      >
                        Cancel
                      </CButton>{" "}
                      <CButton
                        disabled={careCoaches.length === 0}
                        className="mr-2"
                        color="primary"
                        type="submit"
                        id="ccSaveBtn"
                        onClick={() => assignCarecoach()}
                      >
                        Save
                      </CButton>
                    </CModalFooter>
                  </CModal>
                )
              },
            }}
          />
        </CCardBody>
      </CCard>
    </>
  )
}

export default ViewCareCoach
