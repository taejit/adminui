import React, { useEffect, useState } from "react"
import {
  CCard,
  CCardBody,
  CCol,
  CButton,
  CRow,
  CDataTable,
  CBadge,
  CTooltip,
  CAlert,
} from "@coreui/react"
import CIcon from "@coreui/icons-react"
import { useHistory, useLocation } from "react-router-dom"
import AWS from "aws-sdk"
import FormatPhoneNumber from "src/utils/FormatPhoneNumber"
import instance from "../../Auth/GetAWSCredentials"
import "./carecoach.css"

const CareCoaches = () => {
  const [careCoaches, setCareCoaches] = useState([])
  const [errorMessage, setErrorMessage] = useState("")
  const [isLoading, setLoader] = useState(true)
  const location = useLocation()

  const congnitoServiceInstance = async () => {
    AWS.config.region = `${window._env_.region}`
    AWS.config.credentials = await instance.getAWSCredentials()
    return new AWS.CognitoIdentityServiceProvider()
  }

  useEffect(() => {
    congnitoServiceInstance().then((cognitoidentityserviceprovider) => {
      let careCoachListparams = {
        UserPoolId: `${window._env_.pool_id}`,
        GroupName: "carecoach",
        Limit: 60,
      }
      setLoader(true)
      cognitoidentityserviceprovider
        .listUsersInGroup(careCoachListparams)
        .promise()
        .then((careCoachesResponse) => {
          const careCoachesList = []
          careCoachesResponse.Users = careCoachesResponse.Users.sort(
            (user1, user2) =>
              user2.UserLastModifiedDate - user1.UserLastModifiedDate
          )
          careCoachesResponse.Users.forEach((user) => {
            const transformedUserData = {}
            user.Attributes.map(
              (userAttribute) =>
                (transformedUserData[userAttribute.Name] = userAttribute.Value)
            )
            transformedUserData.firstName = transformedUserData.given_name || ""
            transformedUserData.lastName = transformedUserData.family_name || ""
            transformedUserData.isActive = user.Enabled
            transformedUserData.phoneNumber = transformedUserData.phone_number.replace(
              /^\+[0-9]/,
              ""
            )
            transformedUserData.uuid = user.Username
            careCoachesList.push(transformedUserData)
          })
          setLoader(false)
          setCareCoaches(careCoachesList)
        })
        .catch((err) => {
          setLoader(false)
          setErrorMessage("Unable to load care coaches.")
        })
    })
  }, [])

  const history = useHistory()
  const addclick = () => history.push("/carecoaches/add")
  const fields = [
    { key: "firstName", label: "First Name", _classes: "firstName" },
    { key: "lastName", label: "Last Name", _classes: "lastName" },
    { key: "email", label: "Email", _classes: "email" },
    { key: "phoneNumber", label: "Phone", _classes: "phoneNumber" },
    { key: "isActive", label: "Status", _classes: "status" },
    { key: "edit", label: "Action" },
  ]

  return (
    <>
      {isLoading ? (
        <div className="loader-div">
          <div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      ) : (
        <CCard>
          <CCardBody>
            <CRow>
              <CCol>
                <h4 id="traffic" className="title">
                  Care Coaches
                </h4>
              </CCol>
              <CCol sm="2">
                <CButton
                  className="m-2 float-right add-carecoach"
                  shape="pill"
                  color="primary"
                  onClick={addclick}
                >
                  Add New
                </CButton>
              </CCol>
            </CRow>
            <br />
            <>
              {(errorMessage || location.state) && (
                <CRow>
                  <CCol>
                    <div className="mt-2">
                      {errorMessage ? (
                        <CAlert>{errorMessage}</CAlert>
                      ) : (
                        <div className="alert alert-success" role="alert">
                          {location.state}
                        </div>
                      )}
                    </div>
                  </CCol>
                </CRow>
              )}
            </>
            <CDataTable
              className="dataTables_wrapper"
              items={careCoaches}
              fields={fields}
              hover
              sorter
              dynamic={true}
              scopedSlots={{
                phoneNumber: (item) => (
                  <td className="phoneNumber">
                    {FormatPhoneNumber(item.phoneNumber)}
                  </td>
                ),
                isActive: (item) => (
                  <td className="isActive">
                    <CBadge
                      color={
                        item.isActive ? "badge-primary" : "badge-secondary"
                      }
                    >
                      {item.isActive ? "Active" : "Inactive"}
                    </CBadge>
                  </td>
                ),
                edit: (item) => (
                  <>
                    <td>
                      <CTooltip content="View Care Coach">
                        <CIcon
                          size="xl"
                          name={"cilDescription"}
                          className="mx-0 view-care-coach"
                          onClick={() => {
                            window.sessionStorage.setItem(
                              "CCCognitoUuid",
                              item.uuid
                            )
                            history.push({
                              pathname: `/carecoaches/view/${item["custom:uuid"]}`,
                            })
                          }}
                        ></CIcon>
                      </CTooltip>
                      <CTooltip content="Edit Care Coach">
                        <CIcon
                          size="xl"
                          name={"cilPencil"}
                          className="ml-4 edit-carecoach"
                          onClick={() =>
                            history.push(`/carecoaches/edit/${item.uuid}`)
                          }
                        ></CIcon>
                      </CTooltip>
                    </td>
                  </>
                ),
              }}
            />
          </CCardBody>
        </CCard>
      )}
    </>
  )
}

export default CareCoaches
