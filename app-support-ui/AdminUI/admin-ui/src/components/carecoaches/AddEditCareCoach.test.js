import React from "react"
import AddEditCareCoach from "./AddEditCareCoach"
import { shallow, mount } from "enzyme"
import renderer from "react-test-renderer"
import { waitFor, fireEvent, render, cleanup } from "@testing-library/react"
import AWS from "aws-sdk"
import { createMemoryHistory } from "history"

jest.mock("../../Auth/GetAWSCredentials", () => {
  return {
    getAWSCredentials: jest.fn().mockResolvedValue({}),
  }
})

describe("checks component is loading or not", () => {
  beforeEach(() => {
    jest.spyOn(AWS, "CognitoIdentityServiceProvider").mockImplementation(() => {
      return {
        adminGetUser: () => {
          return {
            promise: () => {
              return Promise.resolve({
                UserAttributes: [
                  {
                    Name: "email",
                    Value: "testName@email.com",
                  },
                  {
                    Name: "name",
                    Value: "test name",
                  },
                  {
                    Name: "phone_number",
                    Value: "+17234992839",
                  },
                ],
              })
            },
          }
        },
        adminCreateUser: () => {
          return {
            promise: () => {
              return Promise.resolve({ User: { Username: "test" } })
            },
          }
        },
        adminAddUserToGroup: () => {
          return {
            promise: () => {
              return Promise.resolve("added user to group")
            },
          }
        },
        adminEnableUser: () => {
          return {
            promise: () => {
              return Promise.resolve("Unable to update the user status")
            },
          }
        },
        adminDisableUser: () => {
          return {
            promise: () => {
              return Promise.resolve("Unable to update the user status")
            },
          }
        },
        adminUpdateUserAttributes: () => {
          return {
            promise: () => {
              return Promise.resolve({})
            },
          }
        },
      }
    })
  })
  afterEach(() => {
    jest.resetAllMocks()
    cleanup()
  })
  it("mounts without crashing", () => {
    const wrapper = shallow(<AddEditCareCoach match={{ params: {} }} />)
    wrapper.unmount()
  })

  it("Expect to have add new care coach heading if uuid doesn't exist", () => {
    const wrapper = mount(<AddEditCareCoach match={{ params: {} }} />)
    expect(wrapper.find("#care-coach-header").text()).toEqual(
      "Add New Care Coach"
    )
  })

  it("Expect to have edit heading if uuid exists", () => {
    const wrapper = shallow(
      <AddEditCareCoach match={{ params: { id: "12345" } }} />
    )
    expect(wrapper.find("#care-coach-header").text()).toEqual("Edit Care Coach")
  })

  it("Expects to create a user after form submit", async () => {
    const tree = renderer.create(<AddEditCareCoach match={{ params: {} }} />)

    const wrapperFirstName = tree.root.findByProps({ id: "ccFirstName" }).props
    const wrapperLastName = tree.root.findByProps({ id: "ccLastName" }).props
    const wrapperEmail = tree.root.findByProps({ id: "ccEmail" }).props
    const wrapperPhone = tree.root.findByProps({ id: "ccPhone" }).props
    const wrapperCalendlyUsername = tree.root.findByProps({
      id: "cccalendlyUsername",
    }).props
    const wrapperSubmit = tree.root.findByProps({ id: "AddEditForm" }).props

    await renderer.act(async () => {
      await wrapperFirstName.onChange({
        target: { name: "firstName", value: "testname" },
      })
      await wrapperLastName.onChange({
        target: { name: "lastName", value: "testLastName" },
      })
      await wrapperEmail.onChange({
        target: { name: "email", value: "email@email.com" },
      })
      await wrapperPhone.onChange({
        target: { name: "phone", value: "1234567890" },
      })
      await wrapperCalendlyUsername.onChange({
        target: { name: "phone", value: "1234567890" },
      })
      await wrapperSubmit.onSubmit()
    })
  })

  it("Expects to update a active user after form submit", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <AddEditCareCoach
          history={history}
          match={{
            params: {
              id: "uuid",
            },
          }}
        />
      )
    })
    const { container } = wrapper

    const wrapperFirstName = container.querySelector("#ccFirstName")
    const wrapperLastName = container.querySelector("#ccLastName")
    const wrapperEmail = container.querySelector("#ccEmail")
    const wrapperPhone = container.querySelector("#ccPhone")
    const wrapperCalendlyUsername = container.querySelector(
      "#cccalendlyUsername"
    )
    const wrapperisActive = container.querySelector("#ccIsActive")

    const wrapperSubmitBtn = container.querySelector("#saveBtn")

    await waitFor(async () => {
      fireEvent.change(wrapperFirstName, {
        target: {
          value: "firstName",
          name: "firstName",
        },
      })
      fireEvent.change(wrapperLastName, {
        target: {
          value: "lastName",
          name: "lastName",
        },
      })
      fireEvent.change(wrapperEmail, {
        target: {
          value: "email@test.com",
          name: "email",
        },
      })
      fireEvent.change(wrapperPhone, {
        target: {
          value: "9999999999",
          name: "phone",
        },
      })
      fireEvent.change(wrapperCalendlyUsername, {
        target: {
          value: "meetanand",
          name: "calendlyUsername",
        },
      })
      fireEvent.change(wrapperisActive, {
        target: {
          value: true,
          name: "isActive",
        },
      })
    })
    await waitFor(async () => {
      fireEvent.click(wrapperSubmitBtn)
    })
    expect(history.push).toHaveBeenCalled()
  })

  it("Expects to update a inactive user after form submit", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <AddEditCareCoach
          history={history}
          match={{
            params: {
              id: "uuid",
            },
          }}
        />
      )
    })
    const { container } = wrapper

    const wrapperFirstName = container.querySelector("#ccFirstName")
    const wrapperLastName = container.querySelector("#ccLastName")
    const wrapperEmail = container.querySelector("#ccEmail")
    const wrapperPhone = container.querySelector("#ccPhone")
    const wrapperCalendlyUsername = container.querySelector(
      "#cccalendlyUsername"
    )
    const wrapperisActive = container.querySelector("#ccIsActive")

    const wrapperSubmitBtn = container.querySelector("#saveBtn")

    await waitFor(async () => {
      fireEvent.change(wrapperFirstName, {
        target: {
          value: "firstName",
          name: "firstName",
        },
      })
      fireEvent.change(wrapperLastName, {
        target: {
          value: "lastName",
          name: "lastName",
        },
      })
      fireEvent.change(wrapperEmail, {
        target: {
          value: "email@test.com",
          name: "email",
        },
      })
      fireEvent.change(wrapperPhone, {
        target: {
          value: "9999999999",
          name: "phone",
        },
      })
      fireEvent.change(wrapperCalendlyUsername, {
        target: {
          value: "meetanand",
          name: "calendlyUsername",
        },
      })
      fireEvent.change(wrapperisActive, {
        target: {
          value: true,
          name: "isActive",
        },
      })
    })
    await waitFor(async () => {
      fireEvent.click(wrapperSubmitBtn)
    })
    expect(history.push).toHaveBeenCalled()
  })

  it("Expects to render component if uuid exists", async () => {
    const tree = renderer.create(
      <AddEditCareCoach match={{ params: { id: "test-uuid" } }} />
    )

    const wrapperFirstName = tree.root.findByProps({ id: "ccFirstName" }).props
    const wrapperLastName = tree.root.findByProps({ id: "ccLastName" }).props
    const wrapperEmail = tree.root.findByProps({ id: "ccEmail" }).props
    const wrapperPhone = tree.root.findByProps({ id: "ccPhone" }).props
    const wrapperCalendlyUsername = tree.root.findByProps({
      id: "cccalendlyUsername",
    }).props
    const wrapperSubmit = tree.root.findByProps({ id: "AddEditForm" }).props

    await renderer.act(async () => {
      await wrapperFirstName.onChange({
        target: { name: "firstName", value: "testname" },
      })
      await wrapperLastName.onChange({
        target: { name: "lastName", value: "testLastName" },
      })
      await wrapperEmail.onChange({
        target: { name: "email", value: "email@email.com" },
      })
      await wrapperPhone.onChange({
        target: { name: "phone", value: "1234567890" },
      })
      await wrapperCalendlyUsername.onChange({
        target: { name: "calendlyUsername", value: "meetanand" },
      })
      await wrapperSubmit.onSubmit()
    })
  })

  it("call cognitoIdentityServiceProvider in onchange of switch toggle", async () => {
    const setState = jest.fn()
    const useStateSpy = jest.spyOn(React, "useState")
    const mockOject = {
      firstName: "testName",
      lastName: "testName",
      email: "testName@email.com",
      phone: "993020233",
    }
    useStateSpy.mockImplementation(() => [mockOject, setState])
    let wrapper
    await waitFor(() => {
      wrapper = render(<AddEditCareCoach match={{ params: { id: "uuid" } }} />)
    })

    const { container } = wrapper
    const checkboxBtn = container.querySelector("#ccIsActive")
    const renderContinue = container.querySelector("#ccContinue")
    const renderCancel = container.querySelector("#ccCancel")

    //checking for true
    await waitFor(() => {
      fireEvent.click(checkboxBtn)
    })
    expect(checkboxBtn.checked).toBe(true)

    //checking for false
    await waitFor(() => {
      fireEvent.click(checkboxBtn)
      fireEvent.click(renderContinue)
    })
    expect(checkboxBtn.checked).toBe(false)
  })

  it("Call normalizeInput on onchange of phone input", () => {
    const wrapper = shallow(
      <AddEditCareCoach match={{ params: { id: "123456789" } }} />
    )
    wrapper.find("#ccPhone").simulate("change", {
      target: { value: "1234567890" },
    })
    expect(wrapper.find("#ccPhone").props().value).toBe("(123) 456-7890")
  })
})
