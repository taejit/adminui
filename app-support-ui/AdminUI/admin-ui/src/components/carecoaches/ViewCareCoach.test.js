import React from "react"
import ViewCareCoach from "./ViewCareCoach"
import { shallow, mount, configure } from "enzyme"
import renderer from "react-test-renderer"
import {
  waitFor,
  fireEvent,
  getAllByTestId,
  render,
  cleanup,
  screen,
} from "@testing-library/react"
import { createMemoryHistory } from "history"
import careCoachApiService from "../../services/CareCoachAPIService"
import { Router } from "react-router-dom"
import Adapter from "enzyme-adapter-react-16"

configure({ adapter: new Adapter() })

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./viewcarecoach",
    state: "Changes Saved",
  }),
}))

describe("when user click the button state value should changed", () => {
  const job = jest.fn()

  let wrapper
  beforeEach(() => {
    wrapper = mount(
      <ViewCareCoach match={{ params: { id: "123456" } }} job={job} />
    )
  })

  it("user click button", () => {
    const setModal = jest.fn()
    wrapper.find("#button").at(1).simulate("click")
    expect(wrapper.find(setModal)).toHaveLength(0)
    // expect(setModal).toHaveLength({ showModal: true })
  })
  it("displays initial to-dos", () => {
    const { getByTestId } = render(
      <ViewCareCoach match={{ params: { id: "123456" } }} />
    )
    const todos = getByTestId("list_editor")
    expect(todos.children.length).toBe(2)
  })
})

describe("Test Button component", () => {
  it("Click", () => {
    const history = createMemoryHistory()
    history.push = jest.fn()
    const { container } = render(
      <Router history={history}>
        <ViewCareCoach match={{ params: { id: "123456" } }} />
        {""}
      </Router>
    )
    const CIcon = getAllByTestId(container, "btn-edit")
    fireEvent.click(CIcon[0])
  })
})
describe("checks component is loading or not", () => {
  beforeEach(() => {
    jest.spyOn(careCoachApiService, "getCareCoach").mockImplementation()
    jest
      .spyOn(careCoachApiService, "getCareCoachList")
      .mockImplementation(() => {
        return {
          promise: () => {
            return Promise.resolve({
              data: [
                {
                  id: 6,
                  uuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
                  firstName: "John",
                  lastName: "Dow",
                  nickName: "Joe",
                  email: "example@lantern.care",
                  phone: "6021234567",
                  pronouns: "He/him",
                  location: "Gilbert, Arizona",
                  jobTitle: null,
                  jobStartDate: "2021-03-09",
                  specialities: null,
                  about: null,
                  isActive: true,
                  createdAt: "2021-01-28T08:15:29.276Z",
                  updatedAt: "2021-01-28T08:15:29.270Z",
                  caregivers: [
                    {
                      id: 2,
                      uuid: "99481c44-f73b-4a33-9b91-5dba46edd90a",
                      carecoachUuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
                      fullName: "test name 2",
                      firstName: "test 2",
                      employerName: null,
                      lastName: "name",
                      nickName: null,
                      email: "test@test.com",
                      phone: "1234567890",
                      pronouns: null,
                      isActive: true,
                      createdAt: "2021-01-29T08:15:29.270Z",
                      updatedAt: "2021-01-29T08:15:29.270Z",
                    },
                    {
                      id: 1,
                      uuid: "25b1fbbb-9ea5-49d2-9840-344b6b45af2a",
                      carecoachUuid: "730b261c-d7ce-40bf-b978-3f21e3326ec9",
                      fullName: "test name",
                      firstName: "test",
                      employerName: null,
                      lastName: "name",
                      nickName: null,
                      email: "test@test.com",
                      phone: "1234567890",
                      pronouns: null,
                      isActive: true,
                      createdAt: "2021-01-29T08:15:29.270Z",
                      updatedAt: "2021-01-29T08:15:29.270Z",
                    },
                  ],
                  profileImage:
                    "https://carecoach-profile-images-abhinav.s3.amazonaws.com/730b261c-d7ce-40bf-b978-3f21e3326ec9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAQ4WO7KOQOSVYKENI%2F20210205%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210205T071903Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEDAaCXVzLWVhc3QtMSJIMEYCIQCSAqecgbkdabb6k%2BwMe7atbj%2BFIZjBPAhp%2BKRxnFsOnwIhAItFRYcOPCij14tO8hnrAiIgpeocW72HlMcMn9Gvtc6uKpUDCCgQABoMMDYxNjM3MzUwMzA0Igy7EwsZLeVIaZH9g8Yq8gJPRAQz4iFogaJ9yA2dz%2BFsH7ef%2BXhaqVqvghTi6Hah6QW0w5AZUNOvP%2BNbCT%2BKEAB71htBW9SRX5RQGKiA2AiGZkfdhv2ylpA9LU0JNRAdIbaa9Jeewo%2BHKeCJzhQY1uOvJrqHvdv5E5O9GX%2FVvw69KdA0ZL95KcjIjH8v0XqJOqPFIuGLXQH80HjZuKoAfESVzdpo9a1rmnDVmfamYK%2FYswYhCDFsbar%2Bu4pn%2BFwTkvegHw1ZA5mVE2LaGj8UidMsxSN7g4RCGNYUjdGJr33Xb%2BkOWO6l1DDi6SK5eNHPlrYwk2dbleWh4YccUSJ68jeYAjjXssSOxtCOy1WOyowzDiYiiL8tz%2B%2FiK%2BF8SjbZq57wWU7FFnfaj%2FAMxMbcsCYvw8ennUq3Whm1%2FH5jo%2FBmF%2B1cY73BFeYekv6So21anXnFfP1DFWhEueJwMNhG37FLDgjh1kPRLGgvD603fPSX0okECkpDLa8zPKCUXdcTc6ALMOHc84AGOqUBolCb4X0CJaq4AtGEvfnXzsSXM0coSO5%2F7%2F0bkWos57UvpnymwZcRVt0BuYaEFutCsDmQcruQ7apUW54Q9EmXV470vpAm%2B%2BQ96X0VOuD62W2LOPaOyRlYOgmlFyjnRQg287xv524%2BYTiXls4JflwFslh5CGWtBCY7ZAA%2F3XcVcEZ25oe%2FaDbpjyJqaGZSOy5TTAPS141%2B0dvCIMKTQ0rF1JD%2FIbpL&X-Amz-Signature=ecd49ea929eca64cf30ac3946255d717f480e06c589d162ace10509fdc493e0e&X-Amz-SignedHeaders=host",
                },
              ],
            })
          },
        }
      })
  })

  afterEach(() => {
    jest.resetAllMocks()
    cleanup()
  })

  it("mounts without crashing", () => {
    const wrapper = shallow(
      <ViewCareCoach match={{ params: { id: "123456" } }} />
    )
    wrapper.unmount()
  })

  it("Expects to show a user ", async () => {
    const tree = renderer.create(<ViewCareCoach match={{ params: {} }} />)
    const wrapperSpecialities = tree.root.findByProps({ id: "ccSpecialities" })
      .props
    const wrapperPhone = tree.root.findByProps({ id: "ccPhone" }).props
    const wrapperSubmit = tree.root.findByProps({ id: "ccAbout" }).props
  })

  it("Expects to render component if uuid exists", async () => {
    const tree = renderer.create(
      <ViewCareCoach match={{ params: { id: "test-uuid" } }} />
    )

    const wrapperSpecialities = tree.root.findByProps({ id: "ccSpecialities" })
      .props
    const wrapperPhone = tree.root.findByProps({ id: "ccPhone" }).props
    const wrapperSubmit = tree.root.findByProps({ id: "ccAbout" }).props
  })
  it("call getCareCoach ", async () => {
    const setState = jest.fn()
    const useStateSpy = jest.spyOn(React, "useState")
    const mockOject = {
      firstName: "testName",
      lastName: "testName",
      email: "testName@email.com",
      phone: "993020233",
    }
    useStateSpy.mockImplementation(() => [mockOject, setState])
    let wrapper
    await (() => {
      wrapper = render(<ViewCareCoach match={{ params: { id: "uuid" } }} />)
    })
  })
  it("renders Modal component given the props", () => {
    const closeFn = jest.fn()
    const container = shallow(
      <ViewCareCoach match={{ params: { id: "uuid" } }} closeFn={closeFn}>
        Care Coach Profile
      </ViewCareCoach>
    )
    const overlay = container.find(".modal-overlay")
    expect(overlay).toHaveLength(0)
    const modal = overlay.find(".modal")
    modal.unmount()
  })

  it("call Api in onSubmit", async () => {
    const historyMock = { push: jest.fn() }
    // pass historyMock as prop to component as you are accessing in `props.history` => shallow(<AddEmployer history={historyMock} />)
    const pushSpy = jest.spyOn(historyMock, "push")
    careCoachApiService.assignCareCoach = await jest
      .fn()
      .mockImplementation(() => Promise.resolve({ status: 200 }))
    // then perform the onSubmit
    expect(pushSpy).toBeTruthy()
  })
})
