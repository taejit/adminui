import React from "react"
import { shallow } from "enzyme/build"
import { fireEvent, render, waitFor } from "@testing-library/react"
import ForgotPassword from "./ForgotPassword"
import AWS from "aws-sdk"

jest.mock("../../Auth/GetAWSCredentials", () => {
  return {
    getAWSCredentials: jest.fn().mockResolvedValue({}),
  }
})

describe("Reset Password test", () => {
  it("Reset Password Screen Load", async () => {
    let wrapper
    await waitFor(() => {
      wrapper = render(<ForgotPassword />)
    })
    const { queryByText } = wrapper
    const submitButton = queryByText("Submit")
    expect(submitButton).toBeDefined()
    await waitFor(() => {
      fireEvent.click(submitButton)
    })
    const errorCheck = queryByText("Enter a Valid Email")
    expect(errorCheck).toBeDefined()
  })

  it("Reset Password Failed Test", async () => {
    jest.spyOn(AWS, "CognitoIdentityServiceProvider").mockImplementation(() => {
      return {
        listUsers: () => {
          return {
            promise: () => {
              return Promise.resolve({
                Users: [],
              })
            },
          }
        },
      }
    })

    let wrapper
    await waitFor(() => {
      wrapper = shallow(<ForgotPassword />)
    })

    wrapper.find("#emailAddress").simulate("change", {
      target: { value: "asdfhkjsh@gmail.com" },
    })

    wrapper.find("#button-submit").simulate("click")

    expect(wrapper.find("#data-error")).toBeDefined()
  })

  it("Reset Password API Failed Test", async () => {
    jest.spyOn(AWS, "CognitoIdentityServiceProvider").mockImplementation(() => {
      return {
        listUsers: () => {
          return {
            promise: () => {
              return Promise.reject({})
            },
          }
        },
      }
    })

    let wrapper
    await waitFor(() => {
      wrapper = shallow(<ForgotPassword />)
    })

    wrapper.find("#emailAddress").simulate("change", {
      target: { value: "asdfhkjsh@gmail.com" },
    })

    wrapper.find("#button-submit").simulate("click")
  })

  it("setUserpassword API Failed Test", async () => {
    jest.spyOn(AWS, "CognitoIdentityServiceProvider").mockImplementation(() => {
      return {
        listUsers: () => {
          return {
            promise: () => {
              return Promise.resolve({
                Users: [
                  {
                    Attributes: [
                      {
                        "custom:uuid": "c6295320-204c-49af-b9ee-450da02bc359",
                        sub: "a499a02a-da88-47bb-956e-df3e5bdb29cf",
                        "custom:job_start_date": "2021-03-10",
                        email_verified: "true",
                        phone_number_verified: "true",
                        given_name: "AEJIT",
                        "custom:location": "Arizona City AZ",
                        "custom:bio": "lallalalalal",
                        "custom:pronoun": "He, Him",
                        name: "AEJIT TRIPATHI",
                        nickname: "AEJIT TRIPATHI",
                        phone_number: "+1860",
                        family_name: "TRIPATHI",
                        email: "aejit-carecoach@mailinator.com",
                        "custom:credentials": "RN",
                      },
                    ],
                    Enabled: true,
                    UserCreateDate: 1614776842.815,
                    UserLastModifiedDate: 1619760883.114,
                    Username: "a499a02a-da88-47bb-956e-df3e5bdb29cf",
                    UserStatus: "FORCE_CHANGE_PASSWORD",
                  },
                ],
              })
            },
          }
        },
        adminSetUserPassword: () => {
          return {
            promise: () => {
              return Promise.reject({})
            },
          }
        },
      }
    })

    let wrapper
    await waitFor(() => {
      wrapper = shallow(<ForgotPassword />)
    })

    wrapper.find("#emailAddress").simulate("change", {
      target: { value: "leo-coach@mailinator.com" },
    })

    wrapper.find("#button-submit").simulate("click")
  })

  it("Reset Password Success Test", async () => {
    jest.spyOn(AWS, "CognitoIdentityServiceProvider").mockImplementation(() => {
      return {
        listUsers: () => {
          return {
            promise: () => {
              return Promise.resolve({
                Users: [
                  {
                    Attributes: [
                      {
                        "custom:uuid": "c6295320-204c-49af-b9ee-450da02bc359",
                        sub: "a499a02a-da88-47bb-956e-df3e5bdb29cf",
                        "custom:job_start_date": "2021-03-10",
                        email_verified: "true",
                        phone_number_verified: "true",
                        given_name: "AEJIT",
                        "custom:location": "Arizona City AZ",
                        "custom:bio": "lallalalalal",
                        "custom:pronoun": "He, Him",
                        name: "AEJIT TRIPATHI",
                        nickname: "AEJIT TRIPATHI",
                        phone_number: "+1860",
                        family_name: "TRIPATHI",
                        email: "aejit-carecoach@mailinator.com",
                        "custom:credentials": "RN",
                      },
                    ],
                    Enabled: true,
                    UserCreateDate: 1614776842.815,
                    UserLastModifiedDate: 1619760883.114,
                    Username: "a499a02a-da88-47bb-956e-df3e5bdb29cf",
                    UserStatus: "FORCE_CHANGE_PASSWORD",
                  },
                ],
              })
            },
          }
        },
        adminSetUserPassword: () => {
          return {
            promise: () => {
              return Promise.resolve({})
            },
          }
        },
      }
    })

    let wrapper
    await waitFor(() => {
      wrapper = shallow(<ForgotPassword />)
    })

    wrapper.find("#emailAddress").simulate("change", {
      target: { value: "leo-coach@mailinator.com" },
    })

    wrapper.find("#button-submit").simulate("click")

    expect(wrapper.find("#data-success")).toBeDefined()
  })
})
