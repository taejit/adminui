import React, { useState } from "react"
import instance from "../../Auth/GetAWSCredentials"
import AWS from "aws-sdk"
import "./forgotPass.css"
import { CCardHeader, CCard, CCardBody } from "@coreui/react"

const ForgotPassword = () => {
  const [email, setEmail] = useState("")
  const [error, setError] = useState("")
  const [success, setSuccess] = useState("")

  const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
  }

  const handleSubmit = () => {
    if (validateEmail(email)) {
      setError("")
      userExist(email)
    } else {
      setSuccess("")
      setError("Enter a Valid Email")
    }
  }

  const handleOnChange = (val) => {
    setEmail(val)
  }

  const congnitoServiceInstance = async () => {
    AWS.config.region = `${window._env_.region}`
    AWS.config.credentials = await instance.getAWSCredentials()
    return new AWS.CognitoIdentityServiceProvider()
  }

  function generatePassword(passwordLength) {
    var numberChars = "0123456789"
    var upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    var lowerChars = "abcdefghijklmnopqrstuvwxyz"
    var specialChars = "!@#$%^&*()_+="
    var allChars = numberChars + upperChars + lowerChars + specialChars
    var randPasswordArray = Array(passwordLength)
    randPasswordArray[0] = numberChars
    randPasswordArray[1] = upperChars
    randPasswordArray[2] = lowerChars
    randPasswordArray[3] = specialChars
    randPasswordArray = randPasswordArray.fill(allChars, 4)
    return shuffleArray(
      randPasswordArray.map(function (x) {
        return x[Math.floor(Math.random() * x.length)]
      })
    ).join("")
  }

  function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1))
      var temp = array[i]
      array[i] = array[j]
      array[j] = temp
    }
    return array
  }

  function resetPasswordIfUserExists(Username) {
    let resetPassParams = {
      Password: generatePassword(8),
      Permanent: false,
      Username: Username,
      UserPoolId: `${window._env_.pool_id}`,
    }
    congnitoServiceInstance().then((cognitoidentityserviceprovider) => {
      cognitoidentityserviceprovider
        .adminSetUserPassword(resetPassParams)
        .promise()
        .then((response) => {
          setSuccess(
            `Success. Password was reset for <b>${email}</b>.<br />Their temporary password is: <b>${resetPassParams.Password}</b>`
          )
        })
        .catch((err) => {
          console.log(err)
        })
    })
  }

  const userExist = (email) => {
    congnitoServiceInstance().then((cognitoidentityserviceprovider) => {
      let usersListParams = {
        UserPoolId: `${window._env_.pool_id}`,
        Filter: `email="${email}"`,
      }

      cognitoidentityserviceprovider
        .listUsers(usersListParams)
        .promise()
        .then((userData) => {
          if (userData.Users.length > 0) {
            resetPasswordIfUserExists(userData.Users[0].Username)
          } else {
            setError("Try again. This email was not found in the system.")
          }
        })
        .catch((err) => {
          console.log(err)
        })
    })
  }

  return (
    <CCard>
      <CCardHeader className="forgotpass-header">
        <h4 id="traffic" className="card-title mb-0">
          Reset Password
        </h4>
      </CCardHeader>
      <CCardBody>
        <div className="forgotPassword">
          <div className="forgotPasswordinr">
            <p>
              Enter the email address for the account you would like to reset
              the password for.
            </p>
            <div className="statusBlock">
              {error ? (
                <div className="statusDispBlock error" id="data-error">
                  <div className="statusDispBlockinr">{error}</div>
                </div>
              ) : null}
              {success ? (
                <div className="statusDispBlock success">
                  <div className="statusDispBlockinr">
                    <div dangerouslySetInnerHTML={{ __html: success }}></div>
                  </div>
                </div>
              ) : null}
            </div>
            <div>
              <input
                type="text"
                placeholder="Enter email address"
                value={email}
                onChange={(e) => handleOnChange(e.target.value)}
                id="emailAddress"
              />
              <button
                className="btn btn-primary btn-sm-4"
                type="submit"
                sm="4"
                id="button-submit"
                onClick={handleSubmit}
              >
                Submit
              </button>
            </div>
          </div>
        </div>
      </CCardBody>
    </CCard>
  )
}
export default ForgotPassword
