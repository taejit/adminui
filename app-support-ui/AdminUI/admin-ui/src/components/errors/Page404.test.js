import React from "react"
import { shallow } from "enzyme/build"
import Page404 from "./Page404"

it("mounts without crashing", () => {
  const wrapper = shallow(<Page404 />)
  wrapper.unmount()
})
