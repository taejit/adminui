import React from "react"
import { shallow } from "enzyme/build"
import Page500 from "./Page500"

it("mounts without crashing", () => {
  const wrapper = shallow(<Page500 />)
  wrapper.unmount()
})
