import React from "react"
import {
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CCardHeader,
  CCard,
  CCardBody,
} from "@coreui/react"
import "./Profile.css"
import About from "./About"
import ChangePassword from "./ChangePassword"

const Profile = () => {
  return (
    <CCard>
      <CCardHeader className="profile-header">
        <h4 id="traffic" className="card-title mb-0">
          Profile
        </h4>
      </CCardHeader>
      <CCardBody>
        <CTabs activeTab="about">
          <CNav variant="tabs">
            <CNavItem>
              <CNavLink data-tab="about">About</CNavLink>
            </CNavItem>
            <CNavItem>
              <CNavLink data-tab="changepassword">Change Password</CNavLink>
            </CNavItem>
          </CNav>
          <CTabContent className="align-tabs">
            <CTabPane data-tab="about">
              <About />
            </CTabPane>
            <CTabPane data-tab="changepassword">
              <ChangePassword />
            </CTabPane>
          </CTabContent>
        </CTabs>
      </CCardBody>
    </CCard>
  )
}

export default Profile
