import {
  CognitoUserPool,
  CognitoAccessToken,
  CognitoIdToken,
  CognitoUserSession,
  CognitoUser,
  CognitoRefreshToken,
} from "amazon-cognito-identity-js"
import { settings } from "../../Auth/OidcAuth"

const GetCognitoUserObject = () => {
  const sessionStoreKey = `oidc.user:${settings.authority}:${settings.client_id}`
  const sessionData = JSON.parse(window.sessionStorage.getItem(sessionStoreKey))
  const poolData = {
    UserPoolId: `${window._env_.pool_id}`,
    ClientId: `${window._env_.client_id}`,
  }
  const userPool = new CognitoUserPool(poolData)
  const userData = {
    Pool: userPool,
    Username: sessionData.profile["cognito:username"],
  }

  const { access_token, refresh_token, id_token } = sessionData
  const cognitoUser = new CognitoUser(userData)
  cognitoUser.setSignInUserSession(
    new CognitoUserSession({
      AccessToken: new CognitoAccessToken({ AccessToken: access_token }),
      IdToken: new CognitoIdToken({ IdToken: id_token }),
      RefreshToken: new CognitoRefreshToken({ RefreshToken: refresh_token }),
    })
  )
  return cognitoUser
}
export default GetCognitoUserObject
