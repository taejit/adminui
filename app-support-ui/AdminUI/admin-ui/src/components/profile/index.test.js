import { render } from "@testing-library/react"
import Profile from "."

jest.mock("../../Auth/OidcAuth", () => require("../../__mock__/OidcAuth.mock"))

jest.mock("./ChangePassword", () => {
  return {
    __esModule: true,
    default: () => {
      return <div></div>
    },
  }
})
jest.mock("./About", () => {
  return {
    __esModule: true,
    default: () => {
      return <div></div>
    },
  }
})
describe("Profile page loading", () => {
  it("Page loading", () => {
    const { queryAllByText } = render(<Profile />)
    expect(queryAllByText("Change Password")).toHaveLength(1)
  })
})
