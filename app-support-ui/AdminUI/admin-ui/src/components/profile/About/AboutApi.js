import { CognitoUserAttribute } from "amazon-cognito-identity-js"
import GetCognitoUserObject from "../GetCognitoUserObject"
import { settings } from "../../../Auth/OidcAuth"

const sessionStoreKey = `oidc.user:${settings.authority}:${settings.client_id}`
const GetSessionData = () => {
  return JSON.parse(window.sessionStorage.getItem(sessionStoreKey))
}

const SetSessionData = (name, phone) => {
  const current = GetSessionData()
  current.profile.name = name
  current.profile.phone_number = phone
  window.sessionStorage.setItem(sessionStoreKey, JSON.stringify(current))
}

const UpdateProfile = (name, phone, callback) => {
  const nameAttr = {
    Name: "name",
    Value: name,
  }
  const phoneAttr = {
    Name: "phone_number",
    Value: phone,
  }
  const attributeList = [
    new CognitoUserAttribute(nameAttr),
    new CognitoUserAttribute(phoneAttr),
  ]

  const cognitoUser = GetCognitoUserObject()
  cognitoUser.updateAttributes(attributeList, callback)
}

export { GetSessionData, SetSessionData, UpdateProfile }
