import React from "react"
import { render, fireEvent, waitFor } from "@testing-library/react"
import About from "."
import * as AboutApi from "./AboutApi"

jest.mock("../../../Auth/OidcAuth", () =>
  require("../../../__mock__/OidcAuth.mock")
)

describe("Profile component", () => {
  let wrapper
  beforeEach(async () => {
    AboutApi.GetSessionData = jest.fn().mockReturnValue({
      profile: {
        name: "abc",
        email: "abc@def",
        phone_number: "+11234567890",
      },
    })
    await waitFor(() => {
      wrapper = render(<About />)
    })
  })
  afterEach(() => {
    wrapper = null
  })
  it("Display logged in profile details", async () => {
    const { container } = wrapper
    expect(container.querySelector("input[name='name']").value).toEqual("abc")
    expect(container.querySelector("input[name='phoneNumber']").value).toEqual(
      "(123) 456-7890"
    )
    expect(container.querySelector("input[name='emailAddress']").value).toEqual(
      "abc@def"
    )
  })

  it("Email address should not be editable", async () => {
    const { container } = wrapper
    const emailAddressInput = container.querySelector(
      "input[name='emailAddress']"
    )
    await waitFor(() => {
      fireEvent.change(emailAddressInput, {
        target: {
          value: "will.not@update.com",
          name: "emailAdddress",
        },
      })
    })
    expect(emailAddressInput.value).not.toEqual("will.not@update.com")
  })

  it("Name is required validation", async () => {
    const { container, queryAllByText } = wrapper
    const nameInput = container.querySelector("input[name='name']")
    expect(nameInput.value).toEqual("abc")
    await waitFor(() => {
      fireEvent.change(nameInput, {
        target: {
          name: "name",
          value: "",
        },
      })
      fireEvent.blur(nameInput, {
        target: {
          name: "name",
          value: "",
        },
      })
    })
    expect(nameInput.value).toEqual("")
    expect(queryAllByText("Required")).toHaveLength(1)
  })

  it("Phone number required and format validation", async () => {
    const { container, queryAllByText } = wrapper
    const phoneInput = container.querySelector("input[name='phoneNumber']")
    expect(phoneInput.value).toEqual("(123) 456-7890")
    await waitFor(() => {
      fireEvent.change(phoneInput, {
        target: {
          value: "",
          name: "phoneNumber",
        },
      })
      fireEvent.blur(phoneInput, {
        target: {
          value: "",
          name: "phoneNumber",
        },
      })
    })
    expect(phoneInput.value).toEqual("")
    expect(queryAllByText("Required")).toHaveLength(1)
  })

  it("Submit form should update in session storage also", async () => {
    AboutApi.SetSessionData = jest.fn()
    AboutApi.UpdateProfile = jest
      .fn()
      .mockImplementation((name, phone, callback) => {
        callback(null, "SUCCESS")
      })
    const { getByText } = wrapper
    const updateBtn = getByText("Update")
    await waitFor(() => {
      fireEvent.click(updateBtn)
    })
    expect(AboutApi.UpdateProfile).toHaveBeenCalled()
  })
})
