import React, { useEffect, useState } from "react"
import {
  CContainer,
  CRow,
  CCol,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CButton,
} from "@coreui/react"
import { useFormik } from "formik"
import * as Yup from "yup"
import FormatPhoneNumber from "../../../utils/FormatPhoneNumber"
import { GetSessionData, SetSessionData, UpdateProfile } from "./AboutApi"
import "./About.css"

const About = () => {
  const [success, setSuccess] = useState(false)
  const [error, setError] = useState(false)
  const { profile } = GetSessionData()
  const phoneNum = profile.phone_number.substr(2)
  const formik = useFormik({
    initialValues: {
      name: profile.name,
      emailAddress: profile.email,
      phoneNumber: phoneNum,
    },
    validateOnChange: true,
    validationSchema: Yup.object({
      name: Yup.string().required("Required"),
      phoneNumber: Yup.string()
        .matches(
          /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/,
          "Phone number is not valid."
        )
        .required("Required"),
    }),
    onSubmit: (values) => {
      const phoneNumber = `+1${values.phoneNumber.replace(/[^\d]/g, "")}`
      UpdateProfile(values.name, phoneNumber, (err, result) => {
        setError(!!err)
        setSuccess(!!result)
        SetSessionData(values.name, phoneNumber)
        formik.setSubmitting(false)
      })
    },
  })

  const { setFieldValue } = formik
  useEffect(() => {
    setFieldValue(
      "phoneNumber",
      formik.values.phoneNumber
        ? FormatPhoneNumber(formik.values.phoneNumber)
        : ""
    )
  }, [setFieldValue, formik.values.phoneNumber])

  return (
    <CContainer>
      <CForm onSubmit={formik.handleSubmit}>
        <CRow>
          <CCol sm="4" className="about-form">
            <CFormGroup>
              <CLabel htmlFor="name" className="label-font required">
                Name
              </CLabel>
              <CInput
                type="text"
                id="name"
                name="name"
                placeholder="Enter Name"
                {...formik.getFieldProps("name")}
              />
              {formik.touched.name && formik.errors.name && (
                <div className="text-danger">{formik.errors.name}</div>
              )}
            </CFormGroup>
            <CFormGroup>
              <CLabel htmlFor="emailAddress" className="label-font required">
                Email Address
              </CLabel>
              <CInput
                type="text"
                id="emailAddress"
                name="emailAddress"
                disabled={true}
                {...formik.getFieldProps("emailAddress")}
              />
            </CFormGroup>
            <CFormGroup>
              <CLabel htmlFor="phoneNumber" className="label-font required">
                Phone Number
              </CLabel>
              <CInput
                type="text"
                id="phoneNumber"
                name="phoneNumber"
                placeholder="Enter Phone Number"
                {...formik.getFieldProps("phoneNumber")}
              />
              {formik.touched.phoneNumber && formik.errors.phoneNumber && (
                <div className="text-danger">{formik.errors.phoneNumber}</div>
              )}
            </CFormGroup>
            {error && (
              <div className="text-danger">
                Unable to update profile details. Please try again.
              </div>
            )}
            {success && (
              <div className="text-success">Profile updated successfully.</div>
            )}
            <CButton
              sm="4"
              color="primary"
              size="sm-4"
              type="submit"
              className="m-2 update-about"
              disabled={formik.isSubmitting}
            >
              Update
            </CButton>
          </CCol>
        </CRow>
      </CForm>
    </CContainer>
  )
}

export default About
