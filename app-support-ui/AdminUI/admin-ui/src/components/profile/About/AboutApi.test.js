import * as CognitoIdentityJs from "amazon-cognito-identity-js"
import { GetSessionData, SetSessionData, UpdateProfile } from "./AboutApi"

jest.mock("../../../Auth/OidcAuth", () =>
  require("../../../__mock__/OidcAuth.mock")
)
jest.mock("../GetCognitoUserObject", () => {
  return {
    __esModule: true,
    default: () => {
      return { updateAttributes: jest.fn() }
    },
  }
})

describe("Profile api test", () => {
  it("Get the profile details from session storage", () => {
    jest.spyOn(window.sessionStorage.__proto__, "getItem")
    window.localStorage.__proto__.getItem = jest.fn().mockReturnValue(
      JSON.stringify({
        profile: {
          name: "abc",
          emailAddress: "abc@def",
          phoneNumber: "1234567890",
        },
      })
    )
    GetSessionData()
    expect(sessionStorage.getItem).toHaveBeenCalled()
  })

  it("Set new profile data into session storage", () => {
    window.sessionStorage.__proto__.getItem = jest.fn().mockReturnValue(
      JSON.stringify({
        profile: {
          name: "abc",
          emailAddress: "abc@def",
          phoneNumber: "1234567890",
        },
      })
    )
    jest.spyOn(window.sessionStorage.__proto__, "setItem")
    window.sessionStorage.__proto__.setItem = jest.fn()
    SetSessionData()
    expect(sessionStorage.setItem).toHaveBeenCalled()
  })

  it("Invoke update attributes", () => {
    jest.spyOn(CognitoIdentityJs, "CognitoUserAttribute")
    UpdateProfile("some", "phone", jest.fn())
    expect(CognitoIdentityJs.CognitoUserAttribute).toHaveBeenCalledTimes(2)
  })
})
