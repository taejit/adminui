import ChangePasswordApi from "./ChangePasswordApi"

jest.mock("../../../Auth/OidcAuth", () =>
  require("../../../__mock__/OidcAuth.mock")
)
jest.mock("amazon-cognito-identity-js", () => ({
  ...jest.requireActual("amazon-cognito-identity-js"),
  CognitoUser: function () {
    this.changePassword = (param1, param2, callback) => {
      callback("SUCCESS")
    }
    this.setSignInUserSession = jest.fn()
  },
}))

describe("Change password api test", () => {
  it("Api success callback", () => {
    global.window = Object.create(window)
    Object.defineProperty(window, "sessionStorage", {
      value: {
        getItem: jest.fn().mockImplementation(() =>
          JSON.stringify({
            access_token: "",
            refresh_token: "",
            id_token: "",
            profile: { "cognito:username": "username" },
          })
        ),
      },
      writable: true,
    })
    const mock = jest.fn()
    ChangePasswordApi("old", "new", mock)
    expect(mock).toHaveBeenCalled()
  })
})
