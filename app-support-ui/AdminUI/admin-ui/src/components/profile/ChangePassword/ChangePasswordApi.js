import GetCognitoUserObject from "../GetCognitoUserObject"

const ChangePasswordApi = (currentPassword, newPassword, callback) => {
  const cognitoUser = GetCognitoUserObject()
  cognitoUser.changePassword(currentPassword, newPassword, callback)
}

export default ChangePasswordApi
