import React, { useState, useEffect } from "react"
import {
  CContainer,
  CRow,
  CCol,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CButton,
  CPopover,
} from "@coreui/react"
import { useFormik } from "formik"
import * as Yup from "yup"
import ChangePasswordApi from "./ChangePasswordApi"
import "./ChangePassword.css"

const ChangePassword = () => {
  const [success, setSuccess] = useState(false)
  const [error, setError] = useState(false)
  const [pwdValidationErrs, setPwdValidationErrs] = useState([])

  // the below schema is repeated in useEffect also, because could not resolve warning for missing dependency
  // when adding this variable in dependency array form gets stuck and nothing happens
  let newPasswordValidationSchema = Yup.string()
    .required("Required")
    .min(6, "min")
    .max(99, "max")
    .matches(RegExp("(.*[a-z].*)"), "lowercase")
    .matches(RegExp("(.*[A-Z].*)"), "uppercase")
    .matches(RegExp("(.*\\d.*)"), "number")
    /* eslint-disable no-useless-escape */
    .matches(/^(.*[=+\-^$*.\[\]{}()?"!@#%&/\\,><':;|_~`].*)$/, "special")

  const formik = useFormik({
    initialValues: {
      currentPassword: "",
      newPassword: "",
      retypePassword: "",
    },
    validateOnChange: true,
    validationSchema: Yup.object({
      currentPassword: Yup.string().required("Required"),
      newPassword: newPasswordValidationSchema,
      retypePassword: Yup.string()
        .required("Required")
        .test("test-name", "Re-type password did not match.", (value) => {
          return value === formik.values.newPassword
        }),
    }),
    onSubmit: (values) => {
      ChangePasswordApi(
        values.currentPassword,
        values.newPassword,
        (err, result) => {
          const updated = result === "SUCCESS"
          setError(!updated)
          setSuccess(updated)
          formik.resetForm()
          formik.setSubmitting(false)
        }
      )
    },
  })

  const getErrorText = (errorType) => {
    /*eslint-disable default-case */
    switch (errorType) {
      case "min":
        return "Be at least 6 characters"
      case "max":
        return "Not be more that 99 characters"
      case "lowercase":
        return "Have at least one lower case character"
      case "uppercase":
        return "Have at least one capital letter"
      case "number":
        return "Have at least one number"
      case "special":
        return "Have at least one special character"
    }
  }

  useEffect(() => {
    async function validate() {
      try {
        await Yup.object()
          .shape({
            newPassword: Yup.string()
              .required("Required")
              .min(8, "min")
              .max(99, "max")
              .matches(RegExp("(.*[a-z].*)"), "lowercase")
              .matches(RegExp("(.*[A-Z].*)"), "uppercase")
              .matches(RegExp("(.*\\d.*)"), "number")
              /* eslint-disable no-useless-escape */
              .matches(
                /^(.*[=+\-^$*.\[\]{}()?"!@#%&/\\,><':;|_~`].*)$/,
                "special"
              ),
          })
          .validate(
            { newPassword: formik.values.newPassword },
            { abortEarly: false }
          )
        // this will execute when all validation passes
        setPwdValidationErrs([])
      } catch (err) {
        // parse error object
        setPwdValidationErrs(err.errors)
      }
    }
    validate()
  }, [formik.values.newPassword])

  const getNewPasswordValidationError = () => {
    const criteria = [
      "min",
      "max",
      "lowercase",
      "uppercase",
      "number",
      "special",
    ]
    const results = criteria.map((x) => {
      return (
        <div
          key={x}
          className={
            pwdValidationErrs.includes(x) ? "text-danger" : "text-success"
          }
        >
          {getErrorText(x)}
        </div>
      )
    })
    return results
  }

  return (
    <>
      <CContainer>
        <CRow>
          <CCol sm="4" className="form-alignment">
            <CForm onSubmit={formik.handleSubmit}>
              <CFormGroup>
                <CLabel
                  htmlFor="currentPassword"
                  className="label-font required"
                >
                  Current Password
                </CLabel>
                <CInput
                  type="password"
                  id="currentPassword"
                  name="currentPassword"
                  placeholder="Enter Current Password"
                  {...formik.getFieldProps("currentPassword")}
                />
                {formik.touched.currentPassword &&
                  formik.errors.currentPassword && (
                    <div className="text-danger">
                      {formik.errors.currentPassword}
                    </div>
                  )}
              </CFormGroup>
              <CFormGroup>
                <CLabel htmlFor="newPassword" className="label-font required">
                  New Password
                </CLabel>
                <CPopover
                  header="Password Guidelines"
                  content={getNewPasswordValidationError()}
                  placement="right"
                  trigger="focus"
                >
                  <CInput
                    type="password"
                    id="newPassword"
                    name="newPassword"
                    placeholder="Enter New Password"
                    {...formik.getFieldProps("newPassword")}
                  />
                </CPopover>
                {formik.touched.newPassword && formik.errors.newPassword && (
                  <div className="text-danger">Required</div>
                )}
              </CFormGroup>
              <CFormGroup>
                <CLabel
                  htmlFor="retypePassword"
                  className="label-font required"
                >
                  Re-enter your new password
                </CLabel>
                <CInput
                  type="password"
                  id="retypePassword"
                  name="retypePassword"
                  placeholder="Re-enter your New Password"
                  {...formik.getFieldProps("retypePassword")}
                />
                {formik.touched.retypePassword &&
                  formik.errors.retypePassword && (
                    <div className="text-danger">
                      {formik.errors.retypePassword}
                    </div>
                  )}
              </CFormGroup>
              {error && (
                <div className="text-danger">
                  Unable to change your password. Please try again.
                </div>
              )}
              {success && (
                <div className="text-success">
                  Password changed successfully.
                </div>
              )}

              <CButton
                sm="4"
                color="primary"
                size="sm-4"
                type="submit"
                className="m-2 update-password"
                disabled={formik.isSubmitting}
              >
                Update
              </CButton>
            </CForm>
          </CCol>
        </CRow>
      </CContainer>
    </>
  )
}

export default ChangePassword
