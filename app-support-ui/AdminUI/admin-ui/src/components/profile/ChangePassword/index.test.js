import React from "react"
import { render, fireEvent, waitFor } from "@testing-library/react"
import ChangePassword from "."
import ChangePasswordApi from "./ChangePasswordApi"

jest.mock("../../../Auth/OidcAuth", () =>
  require("../../../__mock__/OidcAuth.mock")
)
jest.mock("./ChangePasswordApi", () => {
  return {
    __esModule: true,
    default: jest.fn((cur, newp, callback) => callback(null, "SUCCESS")),
  }
})
describe("Change password component test", () => {
  it("Form field validation for current password", async () => {
    const { container, queryAllByText } = render(<ChangePassword />)
    const curPwdInput = container.querySelector("#currentPassword")
    await waitFor(() => {
      fireEvent.change(curPwdInput, {
        target: {
          value: "",
          name: "currentPassword",
        },
      })
      fireEvent.blur(curPwdInput, {
        target: {
          value: "",
          name: "currentPassword",
        },
      })
    })
    expect(queryAllByText("Required")).toHaveLength(1)
    await waitFor(() => {
      fireEvent.change(curPwdInput, {
        target: {
          value: "some",
          name: "currentPassword",
        },
      })
    })
    expect(queryAllByText("Required")).toHaveLength(0)
  })

  it("Form field validation for re-type password", async () => {
    const { container, queryAllByText } = render(<ChangePassword />)
    const retypePwd = container.querySelector("#retypePassword")
    await waitFor(() => {
      fireEvent.change(retypePwd, {
        target: {
          value: "rety",
          name: "retypePassword",
        },
      })
    })
    expect(queryAllByText("Re-type password did not match.")).toHaveLength(0)
    await waitFor(() => {
      fireEvent.blur(retypePwd, {
        target: {
          name: "retypePassword",
        },
      })
    })
    expect(queryAllByText("Re-type password did not match.")).toHaveLength(1)
  })

  it("Form field validation for new password", async () => {
    const { container, queryAllByText } = render(<ChangePassword />)
    const newPwdInput = container.querySelector("#newPassword")
    await waitFor(() => {
      fireEvent.focus(newPwdInput, {
        target: {
          value: "",
          name: "newPassword",
        },
      })
    })
    expect(queryAllByText("Be at least 6 characters")).toHaveLength(1)
    await waitFor(() => {
      fireEvent.change(newPwdInput, {
        target: {
          value: "some",
          name: "newPassword",
        },
      })
    })
    expect(queryAllByText("Required")).toHaveLength(0)
  })

  it("Submit form validation", async () => {
    const { container, getByText } = render(<ChangePassword />)
    const updateBtn = getByText("Update")
    await waitFor(() => {
      fireEvent.click(updateBtn)
    })
    expect(ChangePasswordApi).not.toHaveBeenCalled()
    const curPwdInput = container.querySelector("#currentPassword")
    const newPwdInput = container.querySelector("#newPassword")
    const retypePwd = container.querySelector("#retypePassword")
    await waitFor(() => {
      fireEvent.change(curPwdInput, {
        target: {
          value: "some",
          name: "currentPassword",
        },
      })
      fireEvent.change(newPwdInput, {
        target: {
          value: "Ab@1234",
          name: "newPassword",
        },
      })
      fireEvent.change(retypePwd, {
        target: {
          value: "Ab@1234",
          name: "retypePassword",
        },
      })
    })
    await waitFor(() => {
      fireEvent.click(updateBtn)
    })
    expect(ChangePasswordApi).toHaveBeenCalled()
  })
})
