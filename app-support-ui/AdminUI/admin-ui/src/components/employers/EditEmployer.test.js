import React from "react"
import { shallow, mount } from "enzyme/build"
import EditEmployer from "./EditEmployer"
import EmployerApiServices from "../../services/EmployerApiServices"
import axios from "axios"
import { waitFor, fireEvent, render } from "@testing-library/react"

import TestRenderer from "react-test-renderer"

describe("EditEmployer />", () => {
  it("mounts without crashing", () => {
    const wrapper = shallow(
      <EditEmployer match={{ params: { id: "123456789" } }} />
    )
  })

  it("call normalizeInput in onchange of input tag", () => {
    const wrapper = shallow(
      <EditEmployer match={{ params: { id: "123456789" } }} />
    )
    wrapper.find("#phone").simulate("change", {
      target: { value: "1234567890" },
    })
    expect(wrapper.find("#phone").props().value).toBe("(123) 456-7890")
  })

  it("test update employer method", async () => {
    const historyMock = { push: jest.fn() }
    // pass historyMock as prop to component as you are accessing in `props.history` => shallow(<EditEmployer history={historyMock} />)
    const pushSpy = jest.spyOn(historyMock, "push")
    try {
      EmployerApiServices.updateEmployer = jest
        .fn()
        .mockImplementation(() => Promise.resolve({ status: 200 }))
      // then perform the onSubmit
      expect(pushSpy).toBeTruthy()
    } catch (error) {
      return error
    }
  })
  it("testing submit in Edit Form", () => {
    const requestObjMock = {
      uuid: "9c378a39-7db6-490d-9e5d-4b9e4ee2cbee",
      name: "testname",
      employerId: "ng8blI",
      contact: {
        name: "testname2",
        phone: "1234567890",
        email: "test@test.com",
      },
      identifierType: "email",
      active: true,
    }

    const tree = TestRenderer.create(
      <EditEmployer match={{ params: { id: "123456789" } }} />
    )
    const wrapperEmpName = tree.root.findByProps({ id: "editEmpName" }).props
    const wrapperEmpId = tree.root.findByProps({ id: "editEmployerId" }).props
    const wrapperContactName = tree.root.findByProps({ id: "contact-name" })
      .props
    const wrapperEmail = tree.root.findByProps({ id: "editEmail" }).props
    const wrapperPhone = tree.root.findByProps({ id: "phone" }).props
    const wrapperUniqueIdentifier = tree.root.findByProps({
      id: "editUniqueIdentifier",
    }).props
    const wrapperSubmit = tree.root.findByProps({ id: "editForm" }).props

    wrapperEmpName.onChange({
      target: { name: "editEmpName", value: "testname" },
    })
    wrapperEmpId.onChange({
      target: { name: "editEmployerId", value: "ng8blI" },
    })
    wrapperContactName.onChange({
      target: { name: "contactName", value: "testContactName" },
    })
    wrapperEmail.onChange({
      target: { name: "editEmail", value: "email@email.com" },
    })
    wrapperPhone.onChange({
      target: { name: "phone", value: "1234567890" },
    })
    wrapperUniqueIdentifier.onChange({
      target: { name: "editUniqueIdentifier", value: "employerId" },
    })
    wrapperSubmit.onSubmit()
  })

  it("call changeStatus in onchange of switch toggle", () => {
    const setState = jest.fn()
    const useStateSpy = jest.spyOn(React, "useState")
    const mockOject = {
      uuid: "9c378a39-7db6-490d-9e5d-4b9e4ee2cbee",
      name: "testname",
      employerId: "ng8blI",
      contact: {
        name: "testname2",
        phone: "1234567890",
        email: "test@test.com",
      },
      identifierType: "email",
      active: true,
    }
    useStateSpy.mockImplementation(() => [mockOject, setState])
    const { container } = render(
      <EditEmployer match={{ params: { id: "123456789" } }} />
    )
    const checkboxBtn = container.querySelector("#isActive")
    waitFor(() => {
      fireEvent.click(checkboxBtn)
    })
  })
})
