import React, { useState } from "react"
import Icon from "@coreui/icons-react"
import { useHistory } from "react-router-dom"
import { toast } from "react-toastify"
import {
  CCardBody,
  CCol,
  CContainer,
  CTabs,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
} from "@coreui/react"
import "./employer.css"
import employerApiService from "src/services/EmployerApiServices"
import FormatPhoneNumber from "src/utils/FormatPhoneNumber"
import moment from "moment"

const STATUS_TYPES = {
  COMPLETED: "Success",
  STARTED: "Started",
  STARTING: "In Progress",
  FAILED: "Failed",
}

const EmployerData = () => {
  const history = useHistory()
  const employerDetails = history.location.state
  const loadingFileUploadStat = employerDetails?.latestUploadedFile
    ? employerDetails?.latestUploadedFile
    : []

  const [employerData, setEmployerData] = useState({ ...employerDetails })
  const [uploadFileData, setUploadFileData] = useState(loadingFileUploadStat)

  const handleChange = async (event, uuid) => {
    try {
      const { files } = event.target
      if (
        files[0] &&
        files[0].name &&
        files[0].name.split(".").pop().toLowerCase() !== "csv"
      ) {
        toast.error("Please upload only csv files.", { autoClose: 3000 })
        return
      }
      const formData = new FormData()
      formData.append("data", files[0])

      const requestObj = {
        fileName: files[0].name,
      }

      const presignedUrlData = await employerApiService.getPresignedUrl(
        requestObj,
        uuid
      )

      if (presignedUrlData.data.presignedUrl) {
        const uploadResponse = await employerApiService.uploadFileThroughPresignedUrl(
          presignedUrlData.data.presignedUrl,
          files[0]
        )
        if (uploadResponse.status === 200) {
          getUploadStatus(employerDetails.uuid)
        } else {
          toast.error(
            "Unable to process file upload. Please try afain later.",
            {
              autoClose: 3000,
            }
          )
        }
      }
    } catch (err) {
      toast.error("Unable to process file upload. Please try afain later.", {
        autoClose: 3000,
      })
    }
  }

  const getUploadStatus = async (uuid) => {
    const uploadstate_U = await employerApiService.getUploadStatusFromApi(uuid)
    if (uploadstate_U && uploadstate_U.data) {
      setUploadFileData([uploadstate_U.data])
    } else {
      setUploadFileData([])
    }
  }

  const downloadFile = (downloadUrl) => {
    const anchor = document.createElement("a")
    anchor.style.display = "none"
    document.body.appendChild(anchor)
    anchor.href = downloadUrl
    anchor.click()
    document.body.removeChild(anchor)
  }

  const downloadCsvForFailedUpload = async (uuid) => {
    const uploadstate = await employerApiService.getUploadStatusFromApi(uuid)
    const { data } = uploadstate
    downloadFile(data.url)
  }

  const employerDataIdebtifier = (data) => {
    if (data === "companyEmail") {
      return "Company Email"
    } else if (data === "employeeId") {
      return "Employee ID"
    } else {
      return "None"
    }
  }

  const showTimeStamp = (data) => {
    if (data === "FAILED" || data === "COMPLETED") {
      return moment(new Date(uploadFileData[0].uploadCompletedAt)).format(
        "DD/MM/YYYY h:mmA"
      )
    } else if (data === "STARTED" || data === "STARTING") {
      return moment().format("DD/MM/YYYY h:mmA")
    } else {
      return ""
    }
  }

  if (!employerData) {
    return ""
  }

  return (
    <div>
      <CContainer
        fluid
        style={{
          clear: "both",
          float: "left",
          paddingLeft: 0,
          paddingRight: 0,
        }}
      >
        <CCardBody>
          <CCol
            sm="7"
            style={{ float: "left", paddingLeft: 0, paddingRight: 0 }}
          >
            <div className="employerTitleLeft">
              <h1 className="title pl-0 mb-3 employerTitle">
                {employerData?.name}
              </h1>
            </div>

            <div className="singleRow">
              <b>Employee ID</b>: {employerData?.employerId}
            </div>
            <div className="singleRow">
              <b>Unique Employee Identifier</b>:{" "}
              {employerDataIdebtifier(employerData.identifierType)}
            </div>
            <div className="singleRow">
              <b>Eligibility File uploaded</b>:{" "}
              {uploadFileData && uploadFileData.length > 0 ? (
                <React.Fragment>
                  <span style={{ marginRight: "10px" }}>
                    {showTimeStamp(uploadFileData[0].status)}
                  </span>
                  <span
                    className={`badge rounded-pill text-white
                    ${
                      uploadFileData[0].status === "COMPLETED"
                        ? "bg-success"
                        : uploadFileData[0].status === "FAILED"
                        ? "bg-danger"
                        : "bg-primary"
                    }`}
                  >
                    {STATUS_TYPES[uploadFileData[0].status]}
                  </span>
                  {(uploadFileData[0].status === "STARTED" ||
                    uploadFileData[0].status === "STARTING") && (
                    <button
                      className="button-reload"
                      onClick={() => getUploadStatus(employerDetails.uuid)}
                    >
                      <Icon
                        data-toggle="tooltip"
                        data-placement="Top"
                        title="Refresh file upload status"
                        name="cil-reload"
                        className="svg mr-3"
                        size="xl"
                      />
                    </button>
                  )}
                  {uploadFileData[0].status === "FAILED" && (
                    <button
                      data-toggle="tooltip"
                      data-placement="Top"
                      title="Download failed employee list report"
                      className="button-failed"
                      id="button-failed"
                      onClick={() => {
                        downloadCsvForFailedUpload(employerDetails.uuid)
                      }}
                    >
                      <Icon
                        className="icon-failed"
                        name="cil-warning"
                        size="xl"
                      />
                    </button>
                  )}
                </React.Fragment>
              ) : (
                "None"
              )}
            </div>
            <div className="singleRow">
              <b>Status</b>:{" "}
              {employerData.isActive
                ? employerData.isActive == true && (
                    <span className="badge rounded-pill bg-primary-active text-white">
                      Active
                    </span>
                  )
                : employerData.isActive == false && (
                    <span className="badge rounded-pill bg-dark-Inactive text-white">
                      Inactive
                    </span>
                  )}
            </div>
          </CCol>
          <CCol
            sm="5"
            style={{ float: "left", paddingLeft: 0, paddingRight: 0 }}
          >
            <div className="employerTitleRight">
              <label className="fileLabel">
                <input
                  type="file"
                  id="file"
                  accept=".csv"
                  aria-label="File browser example"
                  style={{ display: "none" }}
                  onClick={(event) => {
                    event.target.value = null
                  }}
                  onChange={(event) => {
                    handleChange(event, employerData?.uuid)
                  }}
                />
                <span className="file-custom">
                  <div className="btn upload-button ml-4 float-right mb-0 mt-0 text-center">
                    {"Upload File"}
                  </div>
                </span>
              </label>
              <button
                onClick={() =>
                  history.push(`/employers/edit/${employerData?.uuid}`)
                }
                className="btn upload-button ml-4 float-right mb-0 mt-0 text-center edit-employer"
                variant="primary"
              >
                {"Edit"}
              </button>
            </div>
          </CCol>
        </CCardBody>
      </CContainer>
      <CContainer
        fluid
        style={{
          clear: "both",
          marginTop: 30,
          float: "left",
          paddingLeft: 0,
          paddingRight: 0,
        }}
      >
        <CCardBody>
          <CCol sm="12" style={{ paddingLeft: 0, paddingRight: 0 }}>
            <CTabs activeTab="contactdetails">
              <CNav variant="tabs">
                <CNavItem>
                  <CNavLink data-tab="contactdetails">Contact Details</CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="employees">Employees</CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink data-tab="history">History</CNavLink>
                </CNavItem>
              </CNav>
              <CTabContent className="align-tabs">
                <CTabPane data-tab="contactdetails">
                  <div className="singleTab_block">
                    <div className="singleTab_blockinr">
                      <div className="singleTab_block_body">
                        <div className="singleRow">
                          <b>Contact Name:</b> {employerData?.contactName}
                        </div>
                        <div className="singleRow">
                          <b>Contact Email: </b>
                          {employerData?.contactEmail}
                        </div>
                        <div className="singleRow">
                          <b>Contact Phone:</b>{" "}
                          {employerData.contactPhone
                            ? FormatPhoneNumber(employerData.contactPhone)
                            : ""}
                        </div>
                      </div>
                    </div>
                  </div>
                </CTabPane>
                <CTabPane data-tab="employees">
                  <div className="singleTab_block">Employees</div>
                </CTabPane>
                <CTabPane data-tab="history">
                  <div className="singleTab_block">History</div>
                </CTabPane>
              </CTabContent>
            </CTabs>
          </CCol>
        </CCardBody>
      </CContainer>
    </div>
  )
}
export default EmployerData
