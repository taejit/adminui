import React from "react"
import { configure, shallow, mount } from "enzyme/build"
import AddEmployer from "./AddEmployer"
import employerApiServices from "../../services/EmployerApiServices"
import axios from "axios"
import renderer from "react-test-renderer"

describe("<AddEmployer />", () => {
  it("mounts without crashing", () => {
    const wrapper = shallow(<AddEmployer />)
    wrapper.unmount()
  })

  it("call normalizeInput in onchange of input tag", () => {
    const wrapper = shallow(<AddEmployer />)
    wrapper.find("#phone").simulate("change", {
      target: { value: "1234567890" },
    })
    expect(wrapper.find("#phone").props().value).toBe("(123) 456-7890")
  })

  it("call Api in onSubmit", async () => {
    const historyMock = { push: jest.fn() }
    // pass historyMock as prop to component as you are accessing in `props.history` => shallow(<AddEmployer history={historyMock} />)
    const pushSpy = jest.spyOn(historyMock, "push")
    employerApiServices.createEmployer = jest
      .fn()
      .mockImplementation(() => Promise.resolve({ status: 201 }))
    // then perform the onSubmit
    expect(pushSpy).toBeTruthy()
  })
  it("Expects to have 1 text field in the form", async () => {
    const requestObjMock = {
      name: "testname",
      contact: {
        name: "testname2",
        phone: "1234567890",
        email: "test@test.com",
      },
      identifierType: "email",
    }
    const response = {
      status: 201,
    }
    //axios.get.mockResolvedValue(response)
    axios.get = jest.fn().mockResolvedValue(requestObjMock)

    const tree = renderer.create(<AddEmployer />)

    const wrapperEmpName = tree.root.findByProps({ id: "employer-name" }).props
    const wrapperContactName = tree.root.findByProps({ id: "contact-name" })
      .props
    const wrapperEmail = tree.root.findByProps({ id: "email" }).props
    const wrapperPhone = tree.root.findByProps({ id: "phone" }).props
    const wrapperUniqueIdentifier = tree.root.findByProps({
      id: "unique-identifier",
    }).props
    const wrapperSubmit = tree.root.findByProps({ id: "addForm" }).props

    await renderer.act(async () => {
      await wrapperEmpName.onChange({
        target: { name: "employerName", value: "testname" },
      })
      await wrapperContactName.onChange({
        target: { name: "contactName", value: "testContactName" },
      })
      await wrapperEmail.onChange({
        target: { name: "email", value: "email@email.com" },
      })
      await wrapperPhone.onChange({
        target: { name: "phone", value: "1234567890" },
      })
      await wrapperUniqueIdentifier.onChange({
        target: { name: "uniqueIdentifier", value: "employerId" },
      })
      await wrapperSubmit.onSubmit()
    })
  })
})
