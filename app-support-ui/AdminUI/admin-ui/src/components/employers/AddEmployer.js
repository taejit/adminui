import React, { useState } from "react"
import {
  CCol,
  CRow,
  CInput,
  CLabel,
  CForm,
  CSelect,
  CButton,
  CFormGroup,
  CAlert,
  CCard,
  CCardHeader,
  CCardBody,
} from "@coreui/react"
import { useFormik } from "formik"
import * as Yup from "yup"
import "./employer.css"
import FormatPhoneNumber from "src/utils/FormatPhoneNumber"
import employerApiService from "src/services/EmployerApiServices"

const AddEmployer = (props) => {
  const [inputField, setInputField] = useState({
    employerName: "",
    contactName: "",
    email: "",
    phone: "",
    uniqueIdentifier: "",
  })

  const [phone, setPhone] = useState("")
  const [error, setError] = useState("")

  const normalizeInput = (value) => {
    let inputfield = inputField
    inputfield.phone = value
    setInputField(inputfield)
    formik.setFieldValue("phone", inputfield.phone, true)
    let result = FormatPhoneNumber(value)
    setPhone(result)
  }

  const phoneRegExp = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/
  const formik = useFormik({
    initialValues: {
      employerName: "",
      contactName: "",
      email: "",
      phone: "",
      uniqueIdentifier: "",
    },
    validationSchema: Yup.object({
      employerName: Yup.string().required("Required"),
      contactName: Yup.string().required("Required"),
      email: Yup.string().email("Invalid email address").required("Required"),
      phone: Yup.string()
        .matches(phoneRegExp, "Phone number is not valid")
        .required("Required"),
      uniqueIdentifier: Yup.string().required("Required"),
    }),
    onSubmit: async (values) => {
      const requestObj = {
        name: values.employerName,
        contact: {
          name: values.contactName,
          phone: values.phone.replace(/[^\d]/g, ""),
          email: values.email,
        },
        identifierType: values.uniqueIdentifier,
      }

      const response = await employerApiService.createEmployer(requestObj)
      if (response && response.status === 201) {
        setError(null)
        props.history.push({
          pathname: "/employers",
          state: "Changes Saved",
        })
      } else {
        setError("Check your form fields once and submit data again")
      }
    },
  })
  return (
    <>
      <CCard>
        <CCardHeader className="emp-header">
          <h4 id="traffic" className="card-title mb-0">
            Add New Employer
          </h4>
        </CCardHeader>
        <CCardBody>
          {error && (
            <CRow>
              <CCol>
                <div className="mt-2">
                  <CAlert color="danger" closeButton>
                    {error}
                  </CAlert>
                </div>
              </CCol>
            </CRow>
          )}
          <CForm id="addForm" onSubmit={formik.handleSubmit}>
            <CRow>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel
                    htmlFor="nf-employerName"
                    className="label-font required"
                  >
                    Employer Name
                  </CLabel>
                  <CInput
                    type="text"
                    name="employerName"
                    id="employer-name"
                    placeholder="Enter Employer Name"
                    autoComplete="Employer Name"
                    {...formik.getFieldProps("employerName")}
                  />
                  {formik.touched.employerName &&
                    formik.errors.employerName && (
                      <div className="text-danger">
                        {formik.errors.employerName}
                      </div>
                    )}
                </CFormGroup>
              </CCol>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel
                    htmlFor="nf-contactName"
                    className="label-font required"
                  >
                    Contact Name
                  </CLabel>
                  <CInput
                    type="text"
                    name="contactName"
                    id="contact-name"
                    placeholder="Enter Contact Name"
                    autoComplete="Contact Name"
                    {...formik.getFieldProps("contactName")}
                  />
                  {formik.touched.contactName && formik.errors.contactName && (
                    <div className="text-danger">
                      {formik.errors.contactName}
                    </div>
                  )}
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel htmlFor="nf-email" className="label-font required">
                    Email Address
                  </CLabel>
                  <CInput
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Enter Email Address"
                    autoComplete="Email Address"
                    {...formik.getFieldProps("email")}
                  />
                  {formik.touched.email && formik.errors.email && (
                    <div className="text-danger">{formik.errors.email}</div>
                  )}
                </CFormGroup>
              </CCol>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel htmlFor="nf-phone" className="label-font required">
                    Phone
                  </CLabel>
                  <CInput
                    type="text"
                    name="phone"
                    id="phone"
                    placeholder="(555) 555-5555"
                    autoComplete="Phone"
                    value={phone}
                    onChange={(e) => normalizeInput(e.target.value)}
                  />
                  {formik.touched.phone && formik.errors.phone && (
                    <div className="text-danger">{formik.errors.phone}</div>
                  )}
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel htmlFor="nf-uei" className="label-font required">
                    Unique Employee Identifier
                  </CLabel>
                  <CSelect
                    custom
                    name="unique-identifier"
                    id="unique-identifier"
                    {...formik.getFieldProps("uniqueIdentifier")}
                  >
                    <option value="">Select Option</option>
                    <option value="companyEmail">Company Email</option>
                    <option value="employeeId">Employee ID</option>
                  </CSelect>
                  {formik.touched.uniqueIdentifier &&
                    formik.errors.uniqueIdentifier && (
                      <div className="text-danger">
                        {formik.errors.uniqueIdentifier}
                      </div>
                    )}
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm="12" md="2">
                <CButton
                  type="submit"
                  id="saveBtn"
                  block
                  shape="pill"
                  color="primary"
                >
                  Save
                </CButton>
              </CCol>
            </CRow>
          </CForm>
        </CCardBody>
      </CCard>
    </>
  )
}

export default AddEmployer
