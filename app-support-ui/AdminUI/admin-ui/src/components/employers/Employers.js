import React, { useEffect, useState } from "react"
import Icon from "@coreui/icons-react"
import { useHistory, useLocation } from "react-router-dom"
import { toast } from "react-toastify"
import format from "date-fns/format"
import {
  CInputGroup,
  CInputGroupText,
  CInput,
  CInputGroupAppend,
  CDataTable,
  CBadge,
  CButton,
} from "@coreui/react"

import "./employer.css"
import employerApiService from "src/services/EmployerApiServices"

const Employers = () => {
  const history = useHistory()
  const [employerslist, setEmployers] = useState([])
  const [employersFilteredlist, setEmployersFilteredlist] = useState([])
  const [searchText, setSearchText] = useState("")
  const [noDataText, setNoDataText] = useState(
    "There are no employers that have been added to the system."
  )
  const fields = [
    { key: "name", label: "Employer Name", _classes: "name" },
    { key: "employerId", label: "Employer ID", _classes: "employerId" },
    { key: "contactName", label: "Contact Name", _classes: "contactName" },
    {
      key: "latestUploadedFile",
      label: "Eligibility File Uploaded",
      _classes: "latestUploadedFile",
    },
    { key: "isActive", label: "Status", _classes: "status" },
    { key: "edit", label: "Action" },
  ]
  const location = useLocation()
  useEffect(() => {
    loadEmployers()
  }, [])

  const loadEmployers = async () => {
    try {
      const employersResponse = await employerApiService.getEmployers()
      setEmployers(employersResponse.data.reverse())
      setEmployersFilteredlist(employersResponse.data.reverse())
    } catch (err) {
      toast.error("Unable to fetch the employers. Please try again", {
        autoClose: 3000,
      })
      return
    }
  }

  const searchEmployers = () => {
    setNoDataText("There are no employers that have been added to the system.")
    if (searchText.length)
      setNoDataText("There are no employers with the given search criteria.")
    const filteredList = employerslist.filter((employer) =>
      employer.name?.toLowerCase().includes(searchText.toLowerCase())
    )
    setEmployersFilteredlist(filteredList)
  }

  return (
    <div>
      <div className="page-header">
        <span className="title">Employers</span>
        <div className="search-field">
          <CInputGroup>
            <CInput
              type="text"
              name="searchTxt"
              id="searchTxt"
              autoComplete="searchTxt"
              placeholder="Search Employers"
              value={searchText}
              onChange={(e) => {
                setSearchText(e.target.value)
              }}
            />
            <CInputGroupAppend
              id="search-btn"
              onClick={() => searchEmployers()}
            >
              <CInputGroupText className="search-icon-box cursor-pointer">
                <Icon className="search-icon" name={"cilSearch"} />
              </CInputGroupText>
            </CInputGroupAppend>
          </CInputGroup>
        </div>
        <CButton
          className="float-right add-button"
          shape="pill"
          color="primary"
          onClick={() => history.push("/employers/add")}
        >
          Add New
        </CButton>
      </div>
      <>
        {location.state && (
          <div className="alert alert-success" role="alert">
            {location.state}
          </div>
        )}
      </>
      <>
        <CDataTable
          className="dataTables_wrapper"
          items={employersFilteredlist}
          fields={fields}
          hover
          sorter
          dynamic={true}
          noItemsViewSlot={<div className="noDataTxt">{noDataText}</div>}
          scopedSlots={{
            latestUploadedFile: (employer) => (
              <td>
                {employer.latestUploadedFile[0] &&
                (employer.latestUploadedFile[0].status === "FAILED" ||
                  employer.latestUploadedFile[0].status === "COMPLETED") ? (
                  <span
                    className={
                      employer.latestUploadedFile[0].status === "FAILED"
                        ? "failed"
                        : ""
                    }
                  >
                    {format(
                      new Date(
                        employer.latestUploadedFile[0].uploadCompletedAt
                      ),
                      "M/d/yyyy h:mma"
                    )}
                  </span>
                ) : null}
                {((employer.latestUploadedFile[0] &&
                  employer.latestUploadedFile[0].status === "FAILED") ||
                  employer.uploadFailed) && (
                  <Icon className="icon-failed" name="cil-warning" size="xl" />
                )}
              </td>
            ),
            isActive: (employer) => (
              <td className="isActive">
                <CBadge
                  color={
                    employer.isActive ? "badge-primary" : "badge-secondary"
                  }
                >
                  {employer.isActive ? "Active" : "Inactive"}
                </CBadge>
              </td>
            ),
            edit: (employer) => (
              <>
                <td>
                  <Icon
                    size="xl"
                    name="cilDescription"
                    className="mx-0 cursor-pointer"
                    onClick={() => {
                      history.push({
                        pathname: `/employers/view/${employer.uuid}`,
                        state: {
                          ...employer,
                        },
                      })
                    }}
                  ></Icon>
                  <Icon
                    data-toggle="tooltip"
                    data-placement="Top"
                    title="Edit Employer"
                    onClick={() =>
                      history.push(`/employers/edit/${employer.uuid}`)
                    }
                    name="cil-pencil"
                    className="edit-employer cursor-pointer ml-4"
                    size="xl"
                  />
                </td>
              </>
            ),
          }}
        />
      </>
    </div>
  )
}
export default Employers
