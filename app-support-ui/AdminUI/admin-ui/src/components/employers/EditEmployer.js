import React, { useEffect, useState } from "react"
import {
  CCol,
  CRow,
  CInput,
  CLabel,
  CForm,
  CSelect,
  CButton,
  CFormGroup,
  CAlert,
  CCard,
  CCardHeader,
  CCardBody,
  CSwitch,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
} from "@coreui/react"
import { useFormik } from "formik"
import * as Yup from "yup"
import "./employer.css"
import FormatPhoneNumber from "src/utils/FormatPhoneNumber"
import employerApiService from "src/services/EmployerApiServices"

const EditEmployer = (props) => {
  const [inputField, setInputField] = useState({
    contact: {
      name: "",
      phone: "",
      email: "",
    },
    identifierType: "",
    isActive: "",
    name: "",
    uuid: "",
  })

  const [phone, setPhone] = useState("")
  const [error, setError] = useState("")
  const [showModal, setModal] = useState(false)

  const getEmployerData = async (uuid) => {
    const response = await employerApiService.getEmployer(uuid)
    if (response) {
      setInputField(response.data)
      let phoneNumber = response.data.contact.phone
      if (phoneNumber) {
        setPhone(FormatPhoneNumber(phoneNumber))
      }
    }
  }

  useEffect(() => {
    getEmployerData(props.match.params.id)
  }, [props.match.params.id])

  const normalizeInput = (value) => {
    let inputfield = inputField
    inputfield.phone = value
    setInputField(inputfield)
    formik.setFieldValue("phone", inputfield.phone, true)
    let result = FormatPhoneNumber(value)
    setPhone(result)
  }

  const phoneRegExp = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/
  const formik = useFormik({
    initialValues: {
      editEmpName: inputField.name,
      editEmployerId: inputField.employerId,
      contactName: inputField.contact.name,
      editEmail: inputField.contact.email,
      phone: inputField.contact.phone,
      editUniqueIdentifier: inputField.identifierType,
      isActive: inputField.isActive,
    },
    enableReinitialize: true,
    validationSchema: Yup.object({
      editEmpName: Yup.string().required("Required"),
      contactName: Yup.string().required("Required"),
      editEmail: Yup.string()
        .email("Invalid email address")
        .required("Required"),
      phone: Yup.string()
        .matches(phoneRegExp, "Phone number is not valid")
        .required("Required"),
      editUniqueIdentifier: Yup.string().required("Required"),
    }),
    onSubmit: async (values) => {
      const uuid = props.match.params.id
      const requestObj = {
        name: values.editEmpName,
        contact: {
          name: values.contactName,
          phone: values.phone.replace(/[^\d]/g, ""),
          email: values.editEmail,
        },
        identifierType: values.editUniqueIdentifier,
        isActive: values.isActive,
      }
      const response = await employerApiService.updateEmployer(requestObj, uuid)
      if (response && response.status === 200) {
        setError(null)
        props.history.push({
          pathname: "/employers",
          state: "Changes Saved",
        })
      } else {
        setError("Check your form fields once and submit data again")
      }
    },
  })

  const changeStatus = (value) => {
    if (!value) {
      setModal(true)
      formik.setFieldValue("isActive", value)
    }
    formik.setFieldValue("isActive", value)
  }
  return (
    <>
      <CModal
        show={showModal}
        onClose={() => {
          setModal(!showModal)
          formik.setFieldValue("isActive", true)
        }}
        color="danger"
      >
        <CModalHeader closeButton>
          <CModalTitle>Deactivate Account</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <div className="sm-12">
            <p>
              Are you sure you want to deactivate this employer account? It
              will:
            </p>
            <ul>
              <li>Disable registration for new users from this employer</li>
              <li>Sign out and remove access for all registered employees</li>
              <li>Notify Care Coaches that these employees have lost access</li>
            </ul>
            <p className="modal-small-text">
              Care coach assignments will be retained if this employer is
              reactivated.
            </p>
          </div>
        </CModalBody>
        <CModalFooter>
          <CButton
            color="secondary"
            onClick={() => {
              setModal(false)
              formik.setFieldValue("isActive", true)
            }}
          >
            Cancel
          </CButton>{" "}
          <CButton
            color="danger"
            onClick={() => {
              formik.setFieldValue("isActive", false)
              setModal(!showModal)
            }}
          >
            Continue
          </CButton>
        </CModalFooter>
      </CModal>
      <CCard>
        <CCardHeader className="emp-header">
          <h4 id="traffic" className="card-title mb-0">
            Edit Employer
          </h4>
        </CCardHeader>
        <CCardBody>
          {error && (
            <CRow>
              <CCol>
                <div className="mt-2">
                  <CAlert color="danger" closeButton>
                    {error}
                  </CAlert>
                </div>
              </CCol>
            </CRow>
          )}
          <CForm id="editForm" onSubmit={formik.handleSubmit}>
            <CRow>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel htmlFor="editEmpName" className="label-font required">
                    Employer Name
                  </CLabel>
                  <CInput
                    type="text"
                    name="editEmpName"
                    id="editEmpName"
                    placeholder="Enter Employer Name"
                    autoComplete="Employer Name"
                    {...formik.getFieldProps("editEmpName")}
                  />
                  {formik.touched.editEmpName && formik.errors.editEmpName && (
                    <div className="text-danger">
                      {formik.errors.editEmpName}
                    </div>
                  )}
                </CFormGroup>
              </CCol>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel
                    htmlFor="editEmployerId"
                    className="label-font required"
                  >
                    Employer ID
                  </CLabel>
                  <CInput
                    type="text"
                    name="editEmployerId"
                    id="editEmployerId"
                    placeholder="Enter employerId"
                    autoComplete="employerId"
                    disabled="disabled"
                    {...formik.getFieldProps("editEmployerId")}
                  />
                  {formik.touched.editEmployerId &&
                    formik.errors.editEmployerId && (
                      <div className="text-danger">
                        {formik.errors.editEmployerId}
                      </div>
                    )}
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel
                    htmlFor="nf-contactName"
                    className="label-font required"
                  >
                    Contact Name
                  </CLabel>
                  <CInput
                    type="text"
                    name="nf-contactName"
                    id="contact-name"
                    placeholder="Enter Contact Name"
                    autoComplete="Contact Name"
                    {...formik.getFieldProps("contactName")}
                  />
                  {formik.touched.contactName && formik.errors.contactName && (
                    <div className="text-danger">
                      {formik.errors.contactName}
                    </div>
                  )}
                </CFormGroup>
              </CCol>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel htmlFor="editEmail" className="label-font required">
                    Email Address
                  </CLabel>
                  <CInput
                    type="email"
                    name="editEmail"
                    id="editEmail"
                    placeholder="Enter Email Address"
                    autoComplete="Email Address"
                    {...formik.getFieldProps("editEmail")}
                  />
                  {formik.touched.editEmail && formik.errors.editEmail && (
                    <div className="text-danger">{formik.errors.editEmail}</div>
                  )}
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel htmlFor="nf-phone" className="label-font required">
                    Phone
                  </CLabel>
                  <CInput
                    type="text"
                    name="phone"
                    id="phone"
                    placeholder="(555) 555-5555"
                    autoComplete="Phone"
                    value={phone ? phone : ""}
                    onChange={(e) => normalizeInput(e.target.value)}
                  />
                  {formik.touched.phone && formik.errors.phone && (
                    <div className="text-danger">{formik.errors.phone}</div>
                  )}
                </CFormGroup>
              </CCol>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel
                    htmlFor="editUniqueIdentifier"
                    className="label-font required"
                  >
                    Unique Employee Identifier
                  </CLabel>
                  <CSelect
                    custom
                    name="editUniqueIdentifier"
                    id="editUniqueIdentifier"
                    {...formik.getFieldProps("editUniqueIdentifier")}
                  >
                    <option value="">Select Option</option>
                    <option value="companyEmail">Company Email</option>
                    <option value="employeeId">Employee ID</option>
                  </CSelect>
                  {formik.touched.editUniqueIdentifier &&
                    formik.errors.editUniqueIdentifier && (
                      <div className="text-danger">
                        {formik.errors.editUniqueIdentifier}
                      </div>
                    )}
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm="6">
                <CFormGroup>
                  <CLabel
                    htmlFor="editUniqueIdentifier"
                    className="label-font required"
                  >
                    <b>Status</b>
                  </CLabel>
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol sm="6" md="2" className="mb-4">
                <CSwitch
                  className={"mx-1"}
                  shape={"pill"}
                  color={"primary"}
                  name="isActive"
                  id="isActive"
                  checked={formik.values.isActive}
                  onChange={(e) => changeStatus(e.target.checked)}
                />
                <span className="text-secondary ml-4 status-text">
                  {formik.values.isActive ? "Active" : "Inactive"}
                </span>
                {formik.touched.isActive && formik.errors.isActive && (
                  <div className="text-danger">{formik.errors.isActive}</div>
                )}
              </CCol>
            </CRow>
            <CRow>
              <CCol sm="6" md="2">
                <CButton
                  type="submit"
                  id="saveBtn"
                  block
                  shape="pill"
                  color="primary"
                  disabled={!formik.dirty}
                >
                  Save
                </CButton>
              </CCol>
            </CRow>
          </CForm>
        </CCardBody>
      </CCard>
    </>
  )
}

export default EditEmployer
