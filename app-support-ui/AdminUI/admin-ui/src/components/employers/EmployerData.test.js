import React from "react"
import { createMemoryHistory } from "history"
import { Router } from "react-router-dom"
import { fireEvent, render, waitFor } from "@testing-library/react"
import employerApiService from "../../services/EmployerApiServices"
import EmployerData from "./EmployerData"

jest.mock("../../services/EmployerApiServices")
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./employers",
    state: "Changes Saved",
  }),
}))

describe("Employer Details", () => {
  const mockResponse = {
    data: [
      {
        contactEmail: "e4@intraedge.com",
        contactName: "e4",
        contactPhone: "5042010052",
        createdAt: "2021-06-18T05:19:22.665Z",
        employerId: "3ng5nu",
        id: 170,
        identifierType: "companyEmail",
        isActive: true,
        latestUploadedFile: [
          {
            createdAt: "2021-06-19T07:35:45.133Z",
            employerUuid: "22af15ed-a7a5-40cc-9aa7-ebf183044868",
            fileName: "datavallatestitemstop.csv",
            id: 280,
            s3ObjectName: "2c3d9e72-b9e5-4e59-9460-b61b00033554.csv",
            status: "FAILED",
            updatedAt: "2021-06-19T07:35:50.492Z",
            uploadCompletedAt: "2021-06-19T07:35:50.492Z",
            uploadStartedAt: "2021-06-19T07:35:50.492Z",
            uuid: "fd0c6b8f-2aac-4443-9251-62804e00aaa8",
          },
        ],
        name: "E4",
        updatedAt: "2021-06-18T05:19:22.665Z",
        uuid: "22af15ed-a7a5-40cc-9aa7-ebf183044868",
      },
    ],
  }

  it("Component load", async () => {
    const history = createMemoryHistory()
    history.push("/", {})
    let wrapper
    wrapper = render(
      <Router history={history}>
        <EmployerData />
      </Router>
    )
  })

  it("Edit employer button test", async () => {
    const history = createMemoryHistory()
    const state = { ...mockResponse.data[0] }
    history.push("/", state)
    let wrapper
    wrapper = render(
      <Router history={history}>
        <EmployerData />
      </Router>
    )
    const { container } = wrapper
    const editBtn = container.querySelector(".edit-employer")
    expect(editBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(editBtn)
    })
  })

  it("isActive state display", async () => {
    let mockResponseFailed = { ...mockResponse }
    mockResponseFailed.data[0].isActive = false
    const state = { ...mockResponseFailed.data[0] }
    const history = createMemoryHistory()
    history.push("/", state)
    employerApiService.getEmployers = () => Promise.resolve(mockResponseFailed)
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <Router history={history}>
          <EmployerData />
        </Router>
      )
    })
    const { queryByText } = wrapper
    const InactiveText = queryByText("Inactive")
    expect(InactiveText).toBeDefined()
  })

  it("Upload button test", async () => {
    employerApiService.getPresignedUrl = () =>
      Promise.resolve({ data: { presignedUrl: "http://somerandomurl" } })
    employerApiService.uploadFileThroughPresignedUrl = () =>
      Promise.resolve({ status: 200 })
    jest.spyOn(employerApiService, "getPresignedUrl")
    jest.spyOn(employerApiService, "uploadFileThroughPresignedUrl")

    const history = createMemoryHistory()
    const state = { ...mockResponse.data[0] }
    history.push("/", state)
    let wrapper
    wrapper = render(
      <Router history={history}>
        <EmployerData />
      </Router>
    )
    const { container } = wrapper
    const uploadBtn = container.querySelector("#file")
    expect(uploadBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.change(uploadBtn, {
        target: {
          files: [{ name: "samplefile.csv" }],
        },
      })
    })
    expect(employerApiService.getPresignedUrl).toHaveBeenCalled()
    expect(employerApiService.uploadFileThroughPresignedUrl).toHaveBeenCalled()
  })

  it("Upload button Failed test", async () => {
    employerApiService.uploadFileThroughPresignedUrl = () =>
      Promise.resolve({ status: 201 })

    jest.spyOn(employerApiService, "uploadFileThroughPresignedUrl")

    const history = createMemoryHistory()
    const state = { ...mockResponse.data[0] }
    history.push("/", state)
    let wrapper
    wrapper = render(
      <Router history={history}>
        <EmployerData />
      </Router>
    )
    const { container } = wrapper
    const uploadBtn = container.querySelector("#file")
    expect(uploadBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.change(uploadBtn, {
        target: {
          files: [{ name: "samplefile.pdf" }],
        },
      })
    })
    expect(
      employerApiService.uploadFileThroughPresignedUrl
    ).not.toHaveBeenCalled()
    await waitFor(() => {
      fireEvent.change(uploadBtn, {
        target: {
          files: [{ name: "samplefile.csv" }],
        },
      })
    })
  })

  it("Click the reload button, should show download button and update the upload status", async () => {
    let mockResponseProgress = { ...mockResponse }
    mockResponseProgress.data[0].latestUploadedFile[0].status = "STARTING"
    const history = createMemoryHistory()
    history.push("/", mockResponseProgress.data[0])
    let wrapper
    wrapper = render(
      <Router history={history}>
        <EmployerData />
      </Router>
    )
    const { container, queryByText } = wrapper
    const reloadBtn = container.querySelector(".button-reload")
    expect(queryByText("STARTING")).toEqual(null)
    expect(reloadBtn).toBeDefined()
    employerApiService.getUploadStatusFromApi = () =>
      Promise.resolve({
        data: {
          status: "FAILED",
          uploadCompletedAt: null,
          uploadStartedAt: null,
          url: "https://employer-u",
        },
      })
    await waitFor(() => {
      fireEvent.click(reloadBtn)
    })
    const failedbutton = container.querySelector(".button-failed")
    expect(failedbutton).toBeDefined()
    expect(queryByText("FAILED")).toEqual(null)
    employerApiService.getEmployers = () => Promise.resolve(mockResponse)
  })

  it("Click on download button", async () => {
    let mockResponseFailed = { ...mockResponse }
    mockResponseFailed.data[0].latestUploadedFile[0].status = "FAILED"

    jest.spyOn(employerApiService, "getUploadStatusFromApi")
    employerApiService.getUploadStatusFromApi = () =>
      Promise.resolve({
        data: {
          status: "FAILED",
          uploadCompletedAt: null,
          uploadStartedAt: null,
          url: "https://employer-u",
        },
      })
    const history = createMemoryHistory()
    history.push("/", { ...mockResponseFailed.data[0] })
    let wrapper
    wrapper = render(
      <Router history={history}>
        <EmployerData />
      </Router>
    )
    const { container } = wrapper
    const failedBtn = container.querySelector(".button-failed")
    expect(failedBtn).toBeDefined()
    jest.spyOn(document.body, "appendChild")
    await waitFor(() => {
      fireEvent.click(failedBtn)
    })
  })
})
