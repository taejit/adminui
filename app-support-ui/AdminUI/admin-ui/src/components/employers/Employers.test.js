import React from "react"
import { createMemoryHistory } from "history"
import { Router } from "react-router-dom"
import { fireEvent, render, waitFor } from "@testing-library/react"
import Employers from "./Employers"
import employerApiService from "../../services/EmployerApiServices"
import { roundToNearestMinutes } from "date-fns/fp"
import renderer from "react-test-renderer"

jest.mock("../../services/EmployerApiServices")
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useLocation: () => ({
    pathname: "./employers",
    state: "Changes Saved",
  }),
}))

describe("Employer reload button test", () => {
  const mockResponse = {
    data: [
      {
        contactEmail: "clint@lantern.com",
        contactName: "Clint",
        contactPhone: "2829292910",
        createdAt: "2021-01-08T10:56:32.854Z",
        employerId: "i3p3ol",
        id: 2,
        identifierType: "Email",
        isActive: true,
        name: "Bill",
        updatedAt: "2021-01-08T10:56:32.854Z",
        uuid: "dac4a454-269a-4381-9567-4c6da55433c7",
        latestUploadedFile: [
          {
            createdAt: "2021-01-08T13:51:30.225Z",
            employerUuid: "dac4a454-269a-4381-9567-4c6da55433c7",
            fileName: "employees.csv",
            id: 3,
            s3ObjectName: "034f1d47-9a9c-49a3-a3c6-553fc56c2891.csv",
            status: "STARTING",
            updatedAt: "2021-01-08T13:51:30.225Z",
            uploadCompletedAt: null,
            uploadStartedAt: null,
            uuid: "ba60d381-fd4d-4767-bb22-e82e2ec533b4",
          },
        ],
      },
      {
        contactEmail: "clint@lantern.com",
        contactName: "Clint",
        contactPhone: "2829292910",
        createdAt: "2021-01-08T10:56:32.854Z",
        employerId: "i3p3ol",
        id: 2,
        identifierType: "Email",
        isActive: true,
        name: "Bill",
        updatedAt: "2021-01-08T10:56:32.854Z",
        uuid: "dac4a454-269a-4381-9567-4c6da55433c7",
        latestUploadedFile: [
          {
            createdAt: "2021-01-08T13:51:30.225Z",
            employerUuid: "dac4a454-269a-4381-9567-4c6da55433c7",
            fileName: "employees.csv",
            id: 3,
            s3ObjectName: "034f1d47-9a9c-49a3-a3c6-553fc56c2891.csv",
            status: "FAILED",
            updatedAt: "2021-01-08T13:51:30.225Z",
            uploadCompletedAt: null,
            uploadStartedAt: null,
            uuid: "ba60d381-fd4d-4767-bb22-e82e2ec533b4",
          },
        ],
      },
    ],
  }

  it("Add new employer redirection", async () => {
    let wrapper
    const history = createMemoryHistory()
    history.push = jest.fn()
    await waitFor(() => {
      wrapper = render(
        <Router history={history}>
          <Employers />{" "}
        </Router>
      )
    })
    const { queryByText } = wrapper
    const addNewButton = queryByText("Add New")
    expect(addNewButton).toBeDefined()
    await waitFor(() => {
      fireEvent.click(addNewButton)
    })
    expect(history.push).toHaveBeenCalled()
  })

  it("Edit employer button test", async () => {
    employerApiService.getEmployers = () => Promise.resolve(mockResponse)
    const history = createMemoryHistory()
    history.push = jest.fn()
    let wrapper
    await waitFor(() => {
      wrapper = render(
        <Router history={history}>
          <Employers />
        </Router>
      )
    })
    const { container } = wrapper
    const editBtn = container.querySelector(".edit-employer")
    expect(editBtn).toBeDefined()
    await waitFor(() => {
      fireEvent.click(editBtn)
    })
    expect(history.push).toHaveBeenCalled()
  })

  it("isActive state display", async () => {
    let mockResponseFailed = { ...mockResponse }
    mockResponseFailed.data[0].isActive = false
    employerApiService.getEmployers = () => Promise.resolve(mockResponseFailed)
    let wrapper
    await waitFor(() => {
      wrapper = render(<Employers />)
    })
    const { queryByText } = wrapper
    const InactiveText = queryByText("Inactive")
    expect(InactiveText).toBeDefined()
  })
})

it("Search employer", async () => {
  const tree = renderer.create(<Employers />)
  const wrapperSearchTxt = tree.root.findByProps({ id: "searchTxt" }).props
  const wrapperSearchBtn = tree.root.findByProps({ id: "search-btn" }).props

  await renderer.act(async () => {
    await wrapperSearchTxt.onChange({
      target: { name: "searchTxt", value: "testname" },
    })
    await wrapperSearchBtn.onClick()
  })
})
