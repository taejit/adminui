/* eslint import/no-anonymous-default-export: [2, {"allowArray": true}] */
export default [
  {
    _tag: "CSidebarNavItem",
    name: "Employers",
    to: "/employers",
    icon: "cil-building",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Care Coaches",
    to: "/carecoaches",
    icon: "cil-wc",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Reset Password",
    to: "/forgot-password",
    icon: "cil-reload",
  },
]
