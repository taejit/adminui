import React from "react"
import { withRouter } from "react-router-dom"
import { CognitoJwtVerifier } from "aws-cognito-jwt-verifier"
import { TheContent, TheSidebar, TheFooter, TheHeader } from "./index"
import Forbidden from "./Forbidden"
import userManager from "../Auth/OidcAuth"

class TheLayout extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showLayout: false,
      showForbidden: false,
    }
    this.verify = new CognitoJwtVerifier()
  }

  setShowLayout(flag) {
    this.setState({ showLayout: flag })
  }

  checkJWTValidityAndShowComponent(access_token) {
    return new Promise(async (resolve) => {
      const { region, pool_id } = window._env_
      const result = await this.verify.checkJwt(access_token, region, pool_id)

      const claim = result ? JSON.parse(result).data : null
      const groups = claim ? claim["cognito:groups"] : null
      if (groups && groups.includes("admin")) {
        // success
        resolve()
      } else {
        // clear the session storage
        this.setState({ showForbidden: true })
      }
    })
  }

  componentDidMount() {
    userManager.getUser().then((userFound) => {
      // if there is user logged in -> showLayout
      if (
        userFound &&
        userFound.access_token &&
        userFound.refresh_token &&
        userFound.id_token
      ) {
        this.checkJWTValidityAndShowComponent(userFound.access_token).then(
          () => {
            this.setShowLayout(true)
          }
        )
      }
      // check if state in url -> then its signinredirectcb
      else if (window.location.href.indexOf("state") > -1) {
        userManager
          .signinRedirectCallback()
          .then(async (user) => {
            // user exist and validity not expired
            // check for authorization
            this.checkJWTValidityAndShowComponent(user.access_token).then(
              () => {
                window.location.href = `${window.location.origin}${window.location.pathname}`
              }
            )
          })
          .catch((err) => {})
      } else {
        userManager.signinRedirect().catch((err) => {})
      }
    })
  }

  render() {
    return (
      <>
        {this.state.showForbidden && <Forbidden />}
        {this.state.showLayout && (
          <div className="c-app c-default-layout">
            <TheSidebar />
            <div className="c-wrapper">
              <TheHeader />
              <div className="c-body">
                <TheContent />
              </div>
              <TheFooter />
            </div>
          </div>
        )}
      </>
    )
  }
}

export default withRouter(TheLayout)
