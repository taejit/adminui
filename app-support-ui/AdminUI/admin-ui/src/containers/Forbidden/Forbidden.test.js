import React from "react"
import { mount } from "enzyme/build"
import Forbidden from "."
import { expect } from "chai"

jest.mock("../../Auth/OidcAuth", () => require("../../__mock__/OidcAuth.mock"))

describe("Forbidden component", () => {
  it("Component loading", () => {
    const wrapper = mount(<Forbidden />)
    expect(wrapper.find(Forbidden)).to.have.length(1)
  })
})
