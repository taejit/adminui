import React from "react"
import Logout from "../../Auth/Logout"
import "./Forbidden.css"

const Forbidden = () => {
  return (
    <div className="forbidden">
      <h2>Forbidden</h2>
      <h6>You do not have permissions to access this portal. </h6>
      <h6>
        Please contact your admin to be assigned to an admin group, in order to
        get access to this portal.
      </h6>
      <button type="button" className="go-to-login" onClick={Logout}>
        Go to Login
      </button>
    </div>
  )
}

export default Forbidden
