import React from "react"
import { createMemoryHistory } from "history"
import { CDropdownItem } from "@coreui/react"
import { Router } from "react-router-dom"
import { mount } from "enzyme/build"
import TheHeaderDropdown from "./TheHeaderDropdown"
import Logout from "../Auth/Logout"

jest.mock("../Auth/OidcAuth", () => require("../__mock__/OidcAuth.mock"))
jest.mock("../Auth/Logout", () => {
  return {
    __esModule: true,
    default: jest.fn(),
  }
})

describe("Logout from the application", () => {
  it("Component loading", () => {
    const wrapper = mount(<TheHeaderDropdown />)
    expect(wrapper.find(TheHeaderDropdown)).toHaveLength(1)
  })

  it("Trigger the logout from the header menu", async () => {
    const wrapper = await mount(<TheHeaderDropdown />)
    // .last() because logout placed at last
    const logOutButton = wrapper.find(CDropdownItem).last()
    logOutButton.simulate("click")
    expect(Logout).toHaveBeenCalled()
  })

  it("Logout session after a period of inactivity", async () => {
    jest.useFakeTimers()
    await mount(<TheHeaderDropdown />)
    expect(Logout).not.toHaveBeenCalled()
    jest.runAllTimers()
    expect(Logout).toHaveBeenCalled()
  })

  it("Profile menu click test", async () => {
    const history = createMemoryHistory()
    history.push = jest.fn()
    const wrapper = await mount(
      <Router history={history}>
        <TheHeaderDropdown />
      </Router>
    )
    const profileButton = wrapper.find(CDropdownItem).first()
    profileButton.simulate("click")
    expect(history.push).toHaveBeenCalled()
  })
})
