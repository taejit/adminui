import React from "react"
import { shallow } from "enzyme/build"
import { expect } from "chai"
import { CognitoJwtVerifier } from "aws-cognito-jwt-verifier"
import TheFooter from "./TheFooter"
import TheLayout from "./TheLayout"
import userManager from "../Auth/OidcAuth"
import sinon from "sinon"

jest.mock("aws-cognito-jwt-verifier")
jest.mock("../Auth/OidcAuth", () => require("../__mock__/OidcAuth.mock"))

describe("Layout component test", () => {
  it("When user was already logged in, show layout", () => {
    userManager.getUser = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        access_token: "header.payload.signature",
        refresh_token: "token2",
        id_token: "token3",
      })
    )
    CognitoJwtVerifier.prototype.checkJwt = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.resolve(JSON.stringify({ data: { "cognito:groups": "admin" } }))
      )
    const spyGetUser = sinon.spy(userManager, "getUser")
    shallow(<TheLayout.WrappedComponent />)
    expect(spyGetUser.called).to.equal(true)
  })

  it("When user was not logged in, layout will not be displayed", async () => {
    userManager.getUser = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve())
    userManager.signinRedirect = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve())
    const wrapper = await shallow(<TheLayout.WrappedComponent />)
    expect(wrapper.find(TheFooter)).to.have.length(0)
  })

  it("After successful login it should have state", async () => {
    global.window = Object.create(window)
    const url = "http://dummy.com"
    Object.defineProperty(window, "location", {
      value: {
        origin: url,
        pathname: "/",
        href: `${url}/?code=absg353&state=asbdg12-dg56`,
      },
      writable: true,
    })
    userManager.getUser = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve())
    userManager.signinRedirectCallback = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.resolve({ access_token: "header.payload.signature" })
      )
    CognitoJwtVerifier.prototype.checkJwt = jest
      .fn()
      .mockImplementationOnce(() =>
        Promise.resolve(JSON.stringify({ data: { "cognito:groups": "admin" } }))
      )
    const spySignInRedirectCB = sinon.spy(userManager, "signinRedirectCallback")
    await shallow(<TheLayout.WrappedComponent />)
    expect(spySignInRedirectCB.called).to.equal(true)
  })

  it("When access token is invalid", async () => {
    global.window = Object.create(window)
    const url = "http://dummy.com"
    Object.defineProperties(
      window,
      {
        location: {
          value: {
            origin: url,
            pathname: "/",
            href: `${url}/?code=absg353&state=asbdg12-dg56`,
          },
          writable: true,
        },
      },
      {
        sessionStorage: {
          value: {
            clear: jest.fn(),
          },
        },
      }
    )
    userManager.getUser = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve())
    userManager.signinRedirectCallback = jest
      .fn()
      .mockImplementationOnce(() => Promise.resolve({}))
    const spySignInRedirectCB = sinon.spy(userManager, "signinRedirectCallback")
    await shallow(<TheLayout.WrappedComponent />)
    expect(spySignInRedirectCB.called).to.equal(true)
  })
})
