import React from "react"
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from "@coreui/react"
import CIcon from "@coreui/icons-react"
import { useHistory } from "react-router-dom"
import { useIdleTimer } from "react-idle-timer"
import Logout from "../Auth/Logout"

const TheHeaderDropdown = () => {
  const history = useHistory()
  useIdleTimer({
    timeout: 1000 * 60 * 15, // 15 minutes, supplied in millisecond
    onIdle: Logout,
    debounce: 500,
  })

  const displayProfile = () => {
    history.push("/profile")
  }

  return (
    <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">Menu</div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem onClick={displayProfile}>
          <CIcon name="cil-user" className="mfe-2" />
          Profile
        </CDropdownItem>
        <CDropdownItem divider />
        <CDropdownItem onClick={Logout}>
          <CIcon name="cil-account-logout" className="mfe-2" />
          Log Out
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default TheHeaderDropdown
