import React, { Component } from "react"
import { HashRouter, Route, Switch } from "react-router-dom"
import { ToastContainer } from "react-toastify"

import "./scss/style.scss"
import "react-toastify/dist/ReactToastify.css"

// Containers
const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)
const TheLayout = React.lazy(() => import("./containers/TheLayout"))

const Page404 = React.lazy(() => import("./components/errors/Page404"))
const Page500 = React.lazy(() => import("./components/errors/Page500"))

class App extends Component {
  render() {
    return (
      <div>
        <ToastContainer
          position="top-center"
          hideProgressBar
          newestOnTop={false}
        />
        <HashRouter>
          <React.Suspense fallback={loading}>
            <Switch>
              <Route
                path="/"
                name="Home"
                render={(props) => <TheLayout {...props} />}
              />
              <Route
                path="/employers/add"
                name="Add Employer"
                render={(props) => <TheLayout {...props} />}
              />
              <Route
                exact
                path="/404"
                name="Page 404"
                render={(props) => <Page404 {...props} />}
              />
              <Route
                exact
                path="/500"
                name="Page 500"
                render={(props) => <Page500 {...props} />}
              />
            </Switch>
          </React.Suspense>
        </HashRouter>
      </div>
    )
  }
}

export default App
