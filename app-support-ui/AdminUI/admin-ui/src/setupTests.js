import { configure } from "enzyme"
import Adapter from "enzyme-adapter-react-16"

configure({ adapter: new Adapter() })
jest.mock("axios")

window._env_ = {
  region: "us-east-1",
  pool_id: "us-east-1_b7U1KDhmh",
  authority: "https://auth-feature-1.dev.lanternapp.care",
  client_id: "3nc9v6ff8agojq8kk3csme0esq",
  redirect_uri: "http://localhost:3000/",
  logout_uri: "http://localhost:3000/",
  issuer: "https://cognito-idp.us-east-1.amazonaws.com/us-east-1_b7U1KDhmh",
  REACT_APP_EMPLOYER_API_URL: "http://localhost:3030",
  identity_pool_id: "us-east-1:arn:abcde-1ghsd-yu7jk",
}

if (global.document) {
  document.createRange = () => ({
    setStart: () => {},
    setEnd: () => {},
    commonAncestorContainer: {
      nodeName: "BODY",
      ownerDocument: document,
    },
  })
}
