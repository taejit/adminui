const settings = {
  api_uri: window._env_.REACT_APP_EMPLOYER_API_URL,
}

export { settings }
