import sinon from "sinon"
import Logout from "./Logout"
import userManager from "./OidcAuth"

jest.mock("./OidcAuth", () => require("../__mock__/OidcAuth.mock"))

describe("Logout from cognito", () => {
  it("Logout method url setting", () => {
    userManager.removeUser = jest.fn().mockImplementationOnce(() => {
      return Promise.resolve()
    })
    let assignMock = jest.fn()
    delete window.location
    window.location = { assign: assignMock }
    const spyRemoveUser = sinon.spy(userManager, "removeUser")
    Logout()
    expect(spyRemoveUser.called).toBeTruthy()
    assignMock.mockClear()
  })

  it("On Logout should clear the sessionStorage", () => {
    userManager.removeUser = jest.fn().mockImplementationOnce(() => {
      return Promise.resolve()
    })
    jest.spyOn(window.localStorage.__proto__, "clear")
    window.localStorage.__proto__.setItem = jest.fn()
    Logout()
    expect(localStorage.clear).toHaveBeenCalled()
  })
})
