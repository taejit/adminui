import { UserManager, WebStorageStateStore } from "oidc-client"

const settings = {
  authority: window._env_.authority,
  client_id: window._env_.client_id,
  redirect_uri: window._env_.redirect_uri,
  logout_uri: window._env_.logout_uri,
  response_type: "code",
  response_mode: "query",
  automaticSilentRenew: true,
  scope: "profile openid aws.cognito.signin.user.admin",
  loadUserInfo: false, // kept it false so as to https://github.com/IdentityModel/oidc-client-js/issues/645
  filterProtocolClaims: true,
  userStore: new WebStorageStateStore({ store: sessionStorage }),
  revokeAccessTokenOnSignout: true,
  metadata: {
    token_endpoint: `${window._env_.authority}/oauth2/token`,
    authorization_endpoint: `${window._env_.authority}/oauth2/authorize`,
    userinfo_endpoint: `${window._env_.authority}/oauth2/userInfo`,
    issuer: `${window._env_.issuer}`,
    jwks_uri: `${window._env_.issuer}/.well-known/jwks.json`,
  },
}

const userManager = new UserManager(settings)

export { settings }
export default userManager
