import {
  CognitoAccessToken,
  CognitoIdToken,
  CognitoUserSession,
  CognitoRefreshToken,
} from "amazon-cognito-identity-js"
import { settings } from "./OidcAuth"
import { Credentials } from "@aws-amplify/core"

class GetAWSCredentials {
  constructor() {
    if (!GetAWSCredentials.instance) {
      this.awsCredentials = null
      this.expiration = null
      GetAWSCredentials.instance = this
    }
    return GetAWSCredentials.instance
  }

  setExpirationTime(time) {
    this.expiration = time
  }

  isCredentialsExpired() {
    if (this.expiration) {
      return new Date().valueOf() > new Date(this.expiration).valueOf()
    }
    return true
  }
  async getAWSCredentials() {
    // check expiration of the current session
    if (!this.isCredentialsExpired()) {
      return Promise.resolve(this.awsCredentials)
    }

    const sessionStoreKey = `oidc.user:${settings.authority}:${settings.client_id}`
    const sessionData = JSON.parse(
      window.sessionStorage.getItem(sessionStoreKey)
    )
    if (!sessionData) {
      console.log("No user session found.")
      return
    }

    const { access_token, refresh_token, id_token } = sessionData
    if (!access_token || !refresh_token || !id_token) {
      console.log("Some or all of the session tokens are missing.")
      return
    }

    const sessionObject = new CognitoUserSession({
      AccessToken: new CognitoAccessToken({ AccessToken: access_token }),
      IdToken: new CognitoIdToken({ IdToken: id_token }),
      RefreshToken: new CognitoRefreshToken({ RefreshToken: refresh_token }),
    })

    // this will get the AWS credential
    this.awsCredentials = await Credentials.set(sessionObject, "session")
    this.setExpirationTime(this.awsCredentials.expiration)
    return this.awsCredentials
  }
}

const instance = new GetAWSCredentials()
Object.seal(instance)

export default instance
