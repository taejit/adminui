import GetAWSCredentials from "./GetAWSCredentials"

jest.mock("./OidcAuth", () => require("../__mock__/OidcAuth.mock"))

// one day advance expiration time
const addDays = function (days) {
  var date = new Date()
  date.setDate(date.getDate() + days)
  return date
}

jest.mock("@aws-amplify/core", () => ({
  ...jest.requireActual("@aws-amplify/core"),
  Credentials: {
    set: () =>
      Promise.resolve({
        access_key: "",
        secret_key: "",
        session_key: "",
        expiration: addDays(1),
      }),
  },
}))
jest.mock("amazon-cognito-identity-js", () => ({
  ...jest.requireActual("amazon-cognito-identity-js"),
  CognitoUser: function () {
    this.changePassword = (param1, param2, callback) => {
      callback("SUCCESS")
    }
    this.setSignInUserSession = jest.fn()
  },
  CognitoUserSession: function () {
    return {
      AccessToken: jest.fn(),
      IdToken: jest.fn(),
      RefreshToken: jest.fn(),
    }
  },
}))

describe("Test AWS credential retrieving class", () => {
  it("Should not create instance when no session data is found", async () => {
    global.window = Object.create(window)
    Object.defineProperty(window, "sessionStorage", {
      value: {
        getItem: jest.fn().mockImplementation(() => null),
      },
      writable: true,
    })

    const instance = await GetAWSCredentials.getAWSCredentials()
    expect(instance).toBeUndefined()
  })

  it("Create the instance when tokens are found", async () => {
    global.window = Object.create(window)
    Object.defineProperty(window, "sessionStorage", {
      value: {
        getItem: jest.fn().mockImplementation(() =>
          JSON.stringify({
            access_token: "",
            refresh_token: "",
            id_token: "",
          })
        ),
      },
      writable: true,
    })
    const instance = await GetAWSCredentials.getAWSCredentials()
    expect(instance).toBeUndefined()
  })
  it("No error while invoking and can not modify instance", async () => {
    global.window = Object.create(window)
    Object.defineProperty(window, "sessionStorage", {
      value: {
        getItem: jest.fn().mockImplementation(() =>
          JSON.stringify({
            access_token: "access",
            refresh_token: "refresh",
            id_token: "id",
          })
        ),
      },
    })
    const instance = await GetAWSCredentials.getAWSCredentials()
    expect(instance).toBeDefined()

    const instance2 = await GetAWSCredentials.getAWSCredentials()
    expect(instance).toEqual(instance2)
  })

  it("Class instance can not be modified", async () => {
    try {
      Object.defineProperty(GetAWSCredentials, "instance", { set: jest.fn() })
    } catch (err) {
      expect(err.message).toEqual(
        "Cannot define property instance, object is not extensible"
      )
    }
  })
})
