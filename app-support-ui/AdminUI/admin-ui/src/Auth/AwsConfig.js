import Amplify, { Auth } from "aws-amplify"

Amplify.configure({
  Auth: {
    region: `${window._env_.region}`,
    userPoolId: `${window._env_.pool_id}`,
    userPoolWebClientId: `${window._env_.client_id}`,
    identityPoolId: `${window._env_.identity_pool_id}`,
    authenticationFlowType: "USER_PASSWORD_AUTH",
  },
})

export default Auth.configure()
