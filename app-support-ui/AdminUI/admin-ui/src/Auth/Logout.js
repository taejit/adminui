import userManager, { settings } from "./OidcAuth"

const Logout = () => {
  localStorage.clear()
  userManager
    .removeUser()
    .then(() => {
      window.location.href = `${settings.authority}/logout?client_id=${
        settings.client_id
      }&logout_uri=${encodeURIComponent(settings.logout_uri)}`
    })
    .catch(() => {})
}

export default Logout
